using System;
using System.Collections.Generic;
using src.cbf.engine;
using src.cbfx.uidriver;

namespace src.ModuleDrivers
{
	public class PaymentDriver  : BaseWebModuleDriver
    {
        public void MakePayment(Dictionary<string, dynamic> input, Dictionary<string, dynamic> output)
        {
            if (input != null)
            {
                //1. Click on PersonalTab WebElement

                

                seleniumUIDriver.Click("MakePayment.eltPersonalTab");
                seleniumUIDriver.WaitForBrowserStability(10);

                //2. Click on Contacts WebElement
                seleniumUIDriver.Click("MakePayment.eltContacts");

                //3. Click on UserClick Link
                seleniumUIDriver.Click("MakePayment.lnkUserClick");

                //4. Click on MakePayment WebElement
                seleniumUIDriver.Click("MakePayment.eltMakePayment");

                //5. Type Input in Amount field
                seleniumUIDriver.SetValue("MakePayment.edtAmount", input["Type@Amount"]);

                //6. Click on Submit WebElement
                seleniumUIDriver.Click("MakePayment.eltSubmit");
            }
            if (output != null)
            {
                output.Add("Out Param1", "Param_username");
            }

        }

      
	
    }
}
