using System;
using System.Collections.Generic;
using src.cbf.engine;
using src.cbfx.uidriver;


namespace src.ModuleDrivers
{
    
    public class GeneralDriver : BaseWebModuleDriver
	{

        public void Login(Dictionary<string, dynamic> input, Dictionary<string, dynamic> output)
        {
            if (input != null)
            {
                //1. Navigate to Input URL            
                seleniumUIDriver.LaunchBrowser(input["Navigate"]);

                

                //2. Type Input in UserName field
                seleniumUIDriver.SetValue("Login.edtUserName", input["Type@UserName"]);

                //3. Type Input in PassWord field
                seleniumUIDriver.SetValue("Login.edtPassWord", input["Type@PassWord"]);

                //4. Click on LoginButton WebElement
                seleniumUIDriver.Click("Login.eltLoginButton");                

                //5. Verify whether PageTitle page with Input title is displayed
                if (seleniumUIDriver.CheckPage(input["VerifyPage@PageTitle"]))
                {
                    TestResultLogger.Passed("checkPage", "checkPage should pass", "checkPage passed");
                }
                else
                {
                    TestResultLogger.Failed("checkPage", "checkPage should pass", "checkPage failed");
                }
            }
            if (output != null)
            {
                output.Add("Out Param1", "Param_username");
            }
        }

        public void Logout(Dictionary<string, dynamic> input, Dictionary<string, dynamic> output)
        {
            //1. Close CloseBrowser #targettype# 
            seleniumUIDriver.CloseBrowsers();
            if (input != null)
            {
                TestResultLogger.Done("Logout", "Logout should pass", "Logout passed"); 
            }
            if (output != null)
            {
                output.Add("Out Param1", "Param_username");
            }

        }

        
        

    }
}
