﻿using System.Collections.Generic;
using src.cbf.engine;
using src.cbfx.uidriver;
using System;

namespace src.ModuleDrivers
{
    public class CommonDriver : BaseWebModuleDriver
    {

        public void Launch(Dictionary<string, dynamic> input, Dictionary<string, dynamic> output)
        {

            if (output != null)
            {
                output.Add("Out Param1", "Param_username");
                output.Add("Out Param2", "param_password");
            }

            if (input != null)
            {
                TestResultLogger.Passed("launch call", input["UserName"], input["Password"]);
            }
           
            /**RESULT.Passed("Login To Via.com", "User should be able to login to via  application", "User is logged to via application");
           
            TestResultLogger.Passed("launch call", input["userName"], input["password"]);
//            TestResultLogger.failed("launch call", input["userName"], input["password"]);
            //TestResultLogger.failed("launch call", input["userName"], input["password"]);
            //TestResultLogger.failed("launch call", input["userName"], input["password"]);
            //TestResultLogger.failed("launch call", input["userName"], input["password"]);
            //RESULT.Passed("launch call", input["userName"], input["password"]);*/
            
        }

        public void Login(Dictionary<string, dynamic> input, Dictionary<string, dynamic> output)
        {
            if (input != null)
            {
                TestResultLogger.Passed("Login call", input["userName"], input["password"]);
                TestResultLogger.Passed("Login call", input["userName"], input["password"]);
            }

            if (output != null)
            {
                output.Add("Out Param1", "Param_username");                
            }
        }
		
		

        public void Logout(Dictionary<String, dynamic> input, Dictionary<String, dynamic> output)
        {
            if (input != null)
            {
                /**viaOR OR = new viaOR();
                //seleniumUIDriver.closeBrowsers();
                //Mouse.Click(OR.UICheapAirTicketsOnlinWindow.UICheapFlightTicketsBoTitleBar.UICloseButton);
                //codedUIDriver.closeBrowsers();*/
                TestResultLogger.Passed("Logout call ", input["userName"], input["password"]);
                
            }

            if (output != null)
            {
                output.Add("Out Param1", "Param_username");
            }
        }
    }
}
