﻿using System;
using System.Collections.Generic;
using src.cbf.model;
using src.cbfx.uidriver;

namespace src.ModuleDrivers
{
    public class CompositeAppDriver : BaseWebAppDriver
    {
        public override string Glow()
        {
            return null;
        }
        public CompositeAppDriver(Dictionary<String, dynamic> param) : base(param)
        {
         
        }
        
        public override Dictionary<String, IModuleDriver> LoadModuleDrivers()
        {
            Dictionary<String, IModuleDriver> moduleDrivers = new Dictionary<String, IModuleDriver>();           
            moduleDrivers.Add("General", new GeneralDriver());            
			moduleDrivers.Add("Common", new CommonDriver());
			moduleDrivers.Add("Payment", new PaymentDriver());
            return moduleDrivers;   
        }

        public new void Recover()
        {
            base.Recover();
        }
    }
}
