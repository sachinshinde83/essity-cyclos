﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using src.cbf.harness;
using src.cbf.utils;
using System.Linq;

namespace Projects.Sample1.Lab
{
    [TestClass]
    public class StubSelenium
    {
        [Timeout(TestTimeout.Infinite)]
        [TestMethod]
        public void StubRun_Selenium()
        {
            try
            {
                Dictionary<String, dynamic> runMap = new Dictionary<String, dynamic>();
                runMap.Add("configFilePath", Directory.GetParent
                    (Directory.GetParent
                    ((new System.Diagnostics.StackTrace(true).GetFrame(0).GetFileName())).ToString())
                    + "\\Plan\\ExcelConfig.xml");
                Script.Run(runMap, "Make_Payment");
                if (LogUtils.TCFail)
                {
                    Assert.Fail("Failed");
                }
            }
            catch (FileNotFoundException e)
            {
                if (e.Message.Trim() != "Not found")
                {
                    Assert.Fail("Failed." + e.Message);
                }
            }
        }

       
    }
}
