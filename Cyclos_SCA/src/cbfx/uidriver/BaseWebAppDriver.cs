﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using Selenium;
using src.cbf.engine;
using src.cbfx.uidriver;
using src.cbf.plugin;
using src.cbf.harness;
using src.ModuleDrivers;


namespace src.cbfx.uidriver
{
    public abstract class BaseWebAppDriver : BaseAppDriver
    {
        public static WebSeleniumUIDriver seleniumUIDriver;
        /**
         * Used to initialize WebDriver
         */
        public abstract string Glow();


        public BaseWebAppDriver(Dictionary<String, dynamic> param)
        {
            if(param==null)
            {
                throw new ArgumentNullException("Param");
            }

            List<Dictionary<String, Object>> obj = (List<Dictionary<String, Object>>)param["UIDrivers"];
            Dictionary<String, Object> parameters = new Dictionary<String, Object>();

            foreach (Dictionary<String, Object> map in obj)
            {
                String plugin = (String)map["plugin"];

                if (plugin.Equals("Selenium"))
                {
                    parameters = (Dictionary<String, Object>)map["parameters"];
                    parameters.Add("plugin", plugin);
                    seleniumUIDriver = (WebSeleniumUIDriver)PluginManager.GetPlugin(plugin, parameters);
                }
            }
        }

        public new void Recover()
        {
            if (seleniumUIDriver != null)
            {
                seleniumUIDriver.CloseBrowsers();
            }
        }
    }
}
