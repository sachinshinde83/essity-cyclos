﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using src.cbfx.uidriver;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using Selenium;
using System.Management;
using System.Runtime;
using System.Collections;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using src.cbf.plugin;
using src.cbf.utils;
using src.cbf.reporting;

namespace src.cbfx.uidriver
{
    public class TableHandler
    {

        /**
         * For getting IWebElement of an element
         * 
         * @param elementName
         *            Element name for table locator - uiMap
         * @return IWebElement - IWebElement for element Name
         */

        public TableHandler(WebSeleniumUIDriver driver)
        {
            logger = new LogUtils(this);
            this.driver = driver;
        }

        private IWebElement WebElement(String elementName)
        {
            IWebElement tableElement = null;
            try
            {
                tableElement = driver.GetControl(elementName);
            }
            catch (ObjectDisposedException e)
            {
                logger.HandleError("Failed to get IWebElement ", elementName + ".Exception is " + e.Message);
                logger.HandleError("Error" + e.Message);
            }
            return tableElement;
        }

        /**
         * For getting total number of columns in table
         * 
         * @param elementName
         *            Element name for table locator - uiMap
         * @return int - Number of columns
         */

        public int GetColumnCount(String elementName)
        {
            int col_count = 0;

            try
            {
                col_count = WebElement(elementName).FindElements(By.TagName("td")).Count();
            }
            catch (ObjectDisposedException e)
            {
                logger.HandleError("Failed to get row count for ", elementName + ".Exception is " + e.Message);
                logger.HandleError("Error" + e.Message);
            }
            return col_count;
        }

        /**
         * For getting total number of rows in table
         * 
         * @param elementName
         *            Element name for table locator - uiMap
         * @return int - Number of rows
         */
        public int GetRowCount(String elementName)
        {
            int row_count = 0;
            try
            {
                row_count = WebElement(elementName).FindElements(
                        By.TagName("tr")).Count();

            }
            catch (ObjectDisposedException e)
            {
                logger.HandleError("Failed to get row count for ", elementName + ".Exception is " + e.Message);
                logger.HandleError("Error" + e.Message);
            }
            return row_count;
        }

        /**
         * For getting total number of columns for particular row in table
         * 
         * @param elementName
         *            Element name of table
         * @return int - Number of columns
         */

        public int GetColumnCountOfRow(String elementName)
        {
            IWebElement tableElement = WebElement(elementName);

            ReadOnlyCollection<IWebElement> td_collection = tableElement.FindElement(By.TagName("tr")).FindElements(By.TagName("td"));
            try
            {
                if (td_collection.Count() == 0)
                {
                    td_collection = tableElement.FindElement(By.TagName("tr"))
                            .FindElements(By.TagName("th"));
                }
            }
            catch (ObjectDisposedException e)
            {
                logger.HandleError("Failed to get column count for " + ".Exception is " + e.Message, tableElement);
                logger.HandleError("Error" + e.Message);
            }
            return td_collection.Count();
        }

        /**
         * For getting an object of the table - name matches with given text
         * 
         * @param textTosearch
         *            Name of the table you want to search
         * @return WebElement - Object of table
         */
        public IWebElement GetTableObjectByText(String textTosearch)
        {
            ReadOnlyCollection<IWebElement> tablecollection = webDr.FindElements(By.TagName("table"));
            int FinalIndex = 0;
            try
            {
                for (int i = 0; i < tablecollection.Count(); i++)
                {
                    if (tablecollection[i].Text.Contains(textTosearch))
                    {
                        FinalIndex = i;                       
                    }
                }
            }
            catch (ObjectDisposedException e)
            {
                logger.HandleError("Failed to get text ", textTosearch, "for ",
                        textTosearch, " ", e);
                logger.HandleError("Error" + e.Message);
            }
            return tablecollection[FinalIndex];
        }

        /**
         * For getting data of particular cell
         * 
         * @param tableElement
         *            Element of the table
         * @param row
         *            Row number
         * @param col
         *            Column number
         * @return String - Data of that particular cell
         */

        public String GetCellData(String elementName, int row, int col)
        {
            IWebElement tableElement = WebElement(elementName);
            ReadOnlyCollection<IWebElement> tr_Collection = tableElement.FindElements(By.TagName("tr"));
            int row_num = 0;
            String Data = null;
            try
            {
                foreach (IWebElement trElement in tr_Collection)
                {

                    if (row_num == row)
                    {
                        ReadOnlyCollection<IWebElement> td_collection = trElement.FindElements(By.TagName("td"));

                        if (td_collection.Count() == 0)
                        {
                            td_collection = trElement.FindElements(By.TagName("th"));
                        }
                        Data = td_collection[col].Text.ToString();                       
                    }

                    row_num++;
                }
            }
            catch (ObjectDisposedException e)
            {
                logger.HandleError("Failed to get cell data for ", tableElement,
                        " ", e);
                logger.HandleError("Error" + e.Message);
            }
            return Data;
        }

        /**
         * For getting data of particular cell
         * 
         * @param tableElement
         *            Element of the table
         * @param row
         *            Row number
         * @param colName
         *            Column name
         * @return String - Data of that particular cell
         */

        public String GetCellData(String elementName, int row, String colName)
        {
            IWebElement tableElement = WebElement(elementName);
            ReadOnlyCollection<IWebElement> tr_Collection = tableElement.FindElements(By.TagName("tr"));

            int row_num = 0;
            int col_num = 0;
            String Data = null;
            try
            {

                foreach (IWebElement trElement in tr_Collection)
                {
                    if (row_num == 0)
                    {
                        ReadOnlyCollection<IWebElement> td_col = trElement.FindElements(By.TagName("td"));
                        if (td_col.Count() == 0)
                        {
                            td_col = trElement.FindElements(By.TagName("th"));
                        }
                        for (int i = 0; i < td_col.Count(); i++)
                        {
                            if (td_col[i].Text.Contains(colName))
                            {
                                col_num = i;                               
                            }
                        }
                    }

                    if (row_num == row)
                    {
                        ReadOnlyCollection<IWebElement> td_collection = trElement.FindElements(By.TagName("td"));
                        Data = td_collection[col_num].Text.ToString();                       
                    }

                    row_num++;
                }
            }
            catch (ObjectDisposedException e)
            {
                logger.HandleError("Failed to get cell data for ", tableElement,
                        " ", e);
                logger.HandleError("Error" + e.Message);
            }
            return Data;

        }

        /**
         * For getting data of particular cell
         * 
         * @param tableElement
         *            Element of the table
         * @param rowName
         *            Row name
         * @param colName
         *            Column name
         * @return String - Data of that particular cell
         */
        public String GetCellData(String elementName, String rowName, String colName)
        {

            IWebElement tableElement = WebElement(elementName);
            ReadOnlyCollection<IWebElement> tr_Collection = tableElement.FindElements(By.TagName("tr"));
            int row_num = 0;
            int col_num = 0;

            String Data = null;
            try
            {
                foreach (IWebElement trElement in tr_Collection)
                {
                    if (row_num == 0)
                    {
                        ReadOnlyCollection<IWebElement> td_col = trElement.FindElements(By.TagName("td"));

                        for (int i = 0; i < td_col.Count(); i++)
                        {
                            if (td_col[i].Text.Contains(colName))
                            {
                                col_num = i;                              
                            }
                        }
                    }
                    ReadOnlyCollection<IWebElement> td_col1 = trElement.FindElements(By.TagName("td"));
                    if (td_col1[0].Text.Contains(rowName))
                    {
                        Data = td_col1[col_num].Text.ToString();                      
                    }
                    row_num++;
                }
            }
            catch (ObjectDisposedException e)
            {
                logger.HandleError("Failed to get cell data for ", tableElement,
                        " ", e);
                logger.HandleError("Error" + e.Message);
            }

            return Data;

        }

        public String GetRelativeCellData(String elementName, String searchName,
                int columnNumber)
        {
            int row_count = 0;
            String cellData = null;

            IWebElement tableElement = WebElement(elementName);
            ReadOnlyCollection<IWebElement> allRows = tableElement.FindElements(By.TagName("tr"));
            try
            {
                foreach (IWebElement row in allRows)
                {
                    int col_count = 0;
                    ReadOnlyCollection<IWebElement> cells = row.FindElements(By.TagName("td"));
                    foreach (IWebElement cell in cells)
                    {
                        if (cell.Text.Equals(searchName))
                        {
                            cellData = GetCellData(elementName, row_count,
                                    columnNumber);                        
                        }

                        col_count++;
                    }
                    row_count++;
                }

            }
            catch (ObjectDisposedException e)
            {
                logger.HandleError("getRelativeCellData " + ".Exception is " + e.Message, elementName);
                logger.HandleError("Error" + e.Message);
            }
            return cellData;
        }

        /**
         * For Searching relative cell based on some value
         * 
         * @param elementName Element name for table locator - uiMap
         * 
         * @return int - Number of rows
         */
        public String FindRelativeCellAndClick(String elementName,
                String searchName, int columnNumber, String text)
        {
            int row_count = 0;
            String cellData = null;

            IWebElement tableElement = WebElement(elementName);
            ReadOnlyCollection<IWebElement> allRows = tableElement.FindElements(By.TagName("tr"));
            try
            {
                foreach (IWebElement row in allRows)
                {
                    int col_count = 0;
                    ReadOnlyCollection<IWebElement> cells = row.FindElements(By.TagName("td"));
                    foreach (IWebElement cell in cells)
                    {
                        if (cell.Text.Equals(searchName))
                        {
                            cellData = GetCellData(elementName, row_count,
                                    columnNumber);
                            if (cellData.Equals(text))
                            {
                                ClickCell(cell);
                            }                            
                        }

                        col_count++;
                    }

                    row_count++;                    

                }

            }
            catch (ObjectDisposedException e)
            {
                logger.HandleError("getRelativeCellData " + ".Exception is " + e.Message, elementName);
                logger.HandleError("Error" + e.Message);
            }
            return cellData;
        }

        public void ClickCell(IWebElement element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            else
            {
                element.Click();
            }

        }

        /**
         * Overriding toString() method to return TableHandler format string
         */
        public String TooString()
        {
            return StringUtils.MapString(this);
        }

        readonly private LogUtils logger;
        readonly private IWebDriver webDr;        
        readonly private WebSeleniumUIDriver driver;

    }
}
