﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.Management;
using System.Linq;
using System.Runtime;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.PhantomJS;
using Selenium;
using src.cbf.plugin;
using src.cbf.utils;
using src.cbfx.uidriver;
using src.cbf.engine;
using src.cbf.reporting;
using OpenQA.Selenium.Remote;
using System.Globalization;

namespace src.cbfx.uidriver
{
    public class WebSeleniumUIDriver
    {
        private enum BrowserType
        {
            IE, CHROME, FIREFOX, PhantomJS
        };

        private String startUp, browserDriver;
        private IWebDriver webDr;
        public static ObjectMap objMap;
        readonly private Dictionary<String, dynamic> @params;
        private static Dictionary<String, dynamic> temp = new Dictionary<string, dynamic>();
        readonly private LogUtils logger;
        public List<Dictionary<string, dynamic>> uiMap;


        /**
	 * Overloaded Constructor to initialize webdriver and selenium	 *
         * 
         * * @param parameters
	 *            webDriver: object of WebDriver selenium: object of
	 *            WebDriverBackedSelenium
	 */
        public WebSeleniumUIDriver(Dictionary<String, dynamic> parameters)
        {
            if (parameters == null)
            {
                throw new ArgumentNullException("parameters");
            }
            objMap = new ObjectMap();
            if (parameters.ContainsKey("webDriver"))
            {
                webDr = (IWebDriver)parameters["webDriver"];

            }
            else
            {
                webDr = null;
            }

            @params = parameters;
            logger = new LogUtils();
        }

        public void OpenBrowser(String browser)
        {
            if(browser==null)
            {
                throw new ArgumentNullException();
            }
            @params["browser"] = browser.Trim().ToUpper(CultureInfo.InvariantCulture);
            InitializeParameters(@params);


            if (startUp.Equals(BrowserType.CHROME.ToString()) || startUp.Equals(BrowserType.IE.ToString())
                || startUp.Equals(BrowserType.FIREFOX.ToString()) || startUp.Equals(BrowserType.PhantomJS.ToString()))
            {
                switch ((BrowserType)Enum.Parse(typeof(BrowserType), startUp))
                {

                    case BrowserType.IE:

                        try
                        {

                            var options = new InternetExplorerOptions();
                            options.IntroduceInstabilityByIgnoringProtectedModeSettings = true;
                            webDr = new InternetExplorerDriver(browserDriver, options);

                        }
                        catch (ObjectDisposedException e)
                        {
                            logger.HandleError("Invalid browser driver path in user configuration" + ".Exception is " + e.Message);
                        }
                        break;

                    case BrowserType.FIREFOX:

                        Proxy prox = new Proxy();
                        foreach (KeyValuePair<String, dynamic> proxx in @params)
                        {
                            if (proxx.Key.Equals("proxy"))
                            {
                                prox.HttpProxy = "" + proxx.Value;
                                prox.SslProxy = "" + proxx.Value;
                                prox.SocksProxy = "" + proxx.Value;
                                prox.FtpProxy = "" + proxx.Value;                               
                            }

                        }
                        FirefoxProfile profile = new FirefoxProfile();
                        profile.SetProxyPreferences(prox);

                        webDr = new FirefoxDriver(profile);
                        break;

                    case BrowserType.CHROME:

                        try
                        {

                            webDr = new ChromeDriver(browserDriver);

                        }
                        catch (ObjectDisposedException e)
                        {
                            logger.HandleError("Invalid browser driver path in user configuration" + ".Exception is " + e.Message);
                        }
                        break;

                    case BrowserType.PhantomJS:


                        try
                        {

                            webDr = new PhantomJSDriver(browserDriver);

                        }
                        catch (ObjectDisposedException e)
                        {
                            logger.HandleError("Invalid browser driver path in user configuration" + ".Exception is " + e.Message);
                        }
                        break;
                    default:
                        logger.HandleError("Browser Type is not correct");
                        break;
                }


                webDr.Manage().Window.Maximize();

            }
            else
            {
                logger.HandleError("Invalid browser name");
            }
        }

        /**
         * Launches the Application in the Browser
         * 
         * @param url
         *            URL of the page to be opened.
         */
        public void LaunchBrowser(String link)
        {
            if (link == null)
            {
                throw new ArgumentNullException("link");
            }

            CloseBrowsers();
            string[] part = link.Split('|');
            OpenBrowser(part[0]);
            webDr.Navigate().GoToUrl(part[1]);

            TestResultLogger.Passed("Launch browser with URL " + link,
                "Browser should be launched successfully",
                "Browser is launched successfully");
        }

        /**
	     * Checks page title matches or not
	     * 
	     * @param pageTitle
	     *            title of page opened
	     * @return page match or not value
	     */
        public Boolean CheckPage(String pageTitle)
        {
            if (pageTitle == null)
            {
                throw new ArgumentNullException("pageTitle");
            }

            if (pageTitle.Equals(webDr.Title))
            {
                TestResultLogger.Passed("Check " + pageTitle, "Page " + pageTitle + " should match", pageTitle + " is matched");

                return true;
            }
            TestResultLogger.Failed("Check " + pageTitle, "Page " + pageTitle + " should match", pageTitle + " is not matched");
            return false;
        }


        public IWebElement AutoFind(String elementName)
        {
            IWebElement element = null;
            ReadOnlyCollection<String> availableWindows = webDr.WindowHandles;
            foreach (String win in availableWindows)
            {
                webDr.SwitchTo().Window(win);
                element = GetWebDriverLocator(objMap.GetLocator(elementName));
                if (element == null)
                {
                    List<IWebElement> listOfFrames = webDr.FindElements(By.TagName("frame")).ToList();
                    List<IWebElement> listOfIFrames = webDr.FindElements(By.TagName("iframe")).ToList();
                    listOfFrames.AddRange(listOfIFrames);
                    if (listOfIFrames.Count > 0)
                    {
                        foreach (IWebElement webelement in listOfFrames)
                        {
                            webDr.SwitchTo().Frame(webelement);
                            element = GetWebDriverLocator(objMap.GetLocator(elementName));
                            if (element == null)
                            {
                                webDr.SwitchTo().DefaultContent();
                                continue;
                            }
                        }
                    }
                    continue;
                }             
            }
            return element;
        }


        /**
	     * Identifies the locator as needed by webDriver object
	     * 
	     * @param actualLocator
	     *            to be identified
	     * @return Identified locator as Web Element
	     */

        public IWebElement GetWebDriverLocator(String actualLocator)
        {

            if (actualLocator == null)
            {
                throw new ArgumentNullException("actualLocator");
            }
            String[] elementProp = actualLocator.Split('$');
            IWebElement element = null;
            try
            {
                switch (elementProp[1])
                {
                    case "id":
                        element = webDr.FindElement(By.Id(elementProp[0]));
                        break;
                    case "name":
                        element = webDr.FindElement(By.Name(elementProp[0]));
                        break;
                    case "xpath":
                    case "relativeXpath":
                        element = webDr.FindElement(By.XPath(elementProp[0]));
                        break;
                    case "className":
                        element = webDr.FindElement(By.ClassName(elementProp[0]));
                        break;
                    case "css":
                        element = webDr.FindElement(By.CssSelector(elementProp[0]));
                        break;
                    case "tagName":
                        element = webDr.FindElement(By.TagName(elementProp[0]));
                        break;
                    case "linkText":
                        element = webDr.FindElement(By.LinkText(elementProp[0]));
                        break;
                    case "partialLinkText":
                        element = webDr.FindElement(By.PartialLinkText(elementProp[0]));

                        break;

                    default:
                        logger.HandleError("Unable to find element for the given locator ",
                                                       actualLocator);
                        break;
                }
            }
            catch (ObjectDisposedException e)
            {
                logger.HandleError("Error" + e.Message);
                return null;
            }

            return element;
        }


        public void ExecuteJavaScript(String script, String elementName)
        {
            IWebElement element = GetWebDriverLocator(objMap.GetLocator(elementName));

            try
            {

                if (element == null)
                {
                    logger.HandleError(
                            "Element properties are not provided in the uiMap file",
                            elementName);
                    return;
                }
                IJavaScriptExecutor js = (IJavaScriptExecutor)webDr;
                js.ExecuteScript(script, element);

            }
            catch (ObjectDisposedException e)
            {
                logger.HandleError("Falied to execute ", script + ".Exception is " + e.Message);
            }
        }
        /** returns value from UIMap with cell actually conatining the locator */
        public String ActualLocator(Dictionary<String, dynamic> result)
        {
            if (result == null)
            {
                throw new ArgumentNullException("result");
            }
            foreach (KeyValuePair<String, dynamic> loc in result)
            {
                if (!loc.Key.Equals("page") && !loc.Key.Equals("elementName") && loc.Value != null)
                {
                    return loc.Value;
                }
            }

            return "";


        }
        /**
	     * Sets the value to the specified UI Element
	     * 
	     * @param elementName
	     *            name of element
	     * @param value
	     *            value of element
	     */
        public void SetValue(String elementName, String value)
        {
            try
            {
                String locator = objMap.GetLocator(elementName);
                String type = GetWebDriverLocator(locator).GetAttribute("type");

                if (type.Equals("text", StringComparison.OrdinalIgnoreCase)
                        || type.Equals("textArea", StringComparison.OrdinalIgnoreCase)
                    || type.Equals("password", StringComparison.OrdinalIgnoreCase) || type.Equals("number", StringComparison.OrdinalIgnoreCase))
                {
                    try
                    {
                        if (CheckElement(locator, 10))
                        {
                            GetWebDriverLocator(locator).Clear();
                            GetWebDriverLocator(locator).SendKeys(value);
                            logger.Trace("Typed text '" + value
                                    + "' in the input element '" + elementName
                                    + "'");
                            return;
                        }
                    }
                    catch (Exception e)
                    {
                        
                    }
                }
                if (type.Equals("select-one", StringComparison.OrdinalIgnoreCase) && CheckElement(locator, 10))
                {

                    try
                    {
                        SelectElement selectOption = new SelectElement(GetWebDriverLocator(locator));
                        selectOption.SelectByText(value);
                        logger.Trace("Selected  " + value + "' in "
                                + elementName + "'");
                        return;
                    }
                    catch (ObjectDisposedException e)
                    {
                        logger.HandleError("Error" + e.Message);
                    }

                }
                if (type.Equals("multiSelectBox", StringComparison.OrdinalIgnoreCase))
                {
                    /**setMultiSelectBox(elementName, value);*/
                    SetMultiSelectBox(locator, value);
                    return;
                }
                if (type.Equals("Calendar", StringComparison.OrdinalIgnoreCase))
                {
                    SetCalendar(locator, value);
                    return;
                }

            }
            catch (ObjectDisposedException e)
            {
                logger.Trace("Error while setting value ," + value + " on element "
                        + elementName, e);

                TestResultLogger.Failed("Set " + value + " for " + elementName, value
                + " should get set for " + elementName, value
                + " is not for " + elementName);
            }

            TestResultLogger.Passed("Set " + value + " for " + elementName, value
                + " should get set for " + elementName, value
                + " is set for " + elementName);
        }






        /**
	     * Sets the value to the Calendar control
	     * 
	     * @param locator
	     *            name of locator
	     * @param value
	     *            value to be set
	     */
        public void SetCalendar(String locator, String value)
        {
            if (value == null)
            {
                throw new ArgumentNullException();
            }
            CompareInfo ci = CultureInfo.CurrentCulture.CompareInfo;
            try
            {
                if (CheckElement(locator, 5))
                {
                    GetWebDriverLocator(locator).Click();
                    webDr.FindElement(
                            By.LinkText(value.Substring(0, ci.IndexOf(value, "/", CompareOptions.None))
                                    )).Click();
                }
            }
            catch (ObjectDisposedException e)
            {
                logger.Trace("Error caused while setting value to \"" + locator
                        + "\"", e);
                logger.HandleError("Error" + e.Message);
            }
        }

        /**
	     * Selects the options from MultiSelectBox
	     * 
	     * @param multiSelectBox
	     *            name of multi select box
	     * @param optionsToBeSelected
	     *            name of option to be selected
	     */

        public void SetMultiSelectBox(String multiSelectBox,
                String optionsToBeSelected)
        {
            if(optionsToBeSelected==null)
            {
                throw new ArgumentNullException();
            }
            try
            {
                GetWebDriverLocator(multiSelectBox).Click();
                try
                {
                    IWebElement selectOption = GetWebDriverLocator(objMap
                            .GetLocator("Select All"));
                    if (selectOption.Displayed)
                    {
                        selectOption.Click();
                        selectOption.Click();
                    }
                }
                catch (SeleniumException e)
                {
                    logger.Trace("Error : Unable to find Select All checkbox for "
                            + multiSelectBox + " MultiselectBox" + ".Exception is " + e.Message);
                }

                String[] options = optionsToBeSelected.Split(new[] { ';' });
                /**for (String option : options) {*/
                foreach (String option in options)
                {
                    if (CheckElement(objMap.GetLocator(option), 3))
                    {
                        GetWebDriverLocator(objMap.GetLocator(option)).Click();
                        logger.Trace("Selected the Option :" + option);
                    }
                }
                GetWebDriverLocator(objMap.GetLocator(multiSelectBox)).Click();
            }
            catch (ObjectDisposedException e)
            {
                logger.Trace("Failed to select " + optionsToBeSelected + " from "
                        + multiSelectBox + ".Exception is " + e.Message);
                logger.HandleError("Error" + e.Message);
            }
        }


        /**
	     * Sets the value to all the locators on the specified page.
	     * 
	     * @param pageName
	     *            name of the Page
	     * @param data
	     *            value to be set
	     */
        public void SetValues(String pageName, Dictionary<String, dynamic> data)
        {            
            List<Dictionary<String, dynamic>> resultMap = objMap.GetPageLocators(pageName);
            SetValues(data, resultMap);
        }


        /**
	     * Sets the given value to the list of UI elements on the specified page
	     * 
	     * @param data
	     *            to be used to set the values
	     * @param uiElements
	     *            list of selected UI elements
	     */
        private void SetValues(Dictionary<String, dynamic> data, List<Dictionary<String, dynamic>> uiElements)
        {
            String sElement = "";
            /**for (Map uiElement : uiElements) {*/
            foreach (Dictionary<String, dynamic> uiElement in uiElements)
            {
                sElement = ((String)uiElement["page"]) + "." + ((String)uiElement["elementName"]);
                if (data.ContainsKey(sElement))
                {
                    SetValue(sElement, (String)data[sElement]);
                }
            }
        }



        /**
	     * Sets the value to the list of selected locators in the specified page
	     * 
	     * @param pageName
	     *            name of page
	     * @param data
	     *            to be used to set the values
	     * @param selectedElements
	     *            list of selected elements
	     */
        public void SetSelectedValues(String pageName, Dictionary<String, dynamic> data,
                List<String> selectedElements)
        {
            
            List<Dictionary<String, dynamic>> resultMap = objMap.GetUiMap(pageName);
            SetValues(data, objMap.GetSelectedMappingElements(resultMap, selectedElements));
        }



        /**
	     * Gets the value of the specified element
	     * 
	     * @param elementName
	     *            whose value has to be obtained
	     */
        public String GetValue(String elementName)
        {
            Dictionary<String, dynamic> result = null;
            try
            {
                result = objMap.GetElementMap(elementName);
                if (result == null)
                {
                    logger.HandleError(
                            "Element properties are not provided in the uiMap file",
                            elementName);
                    return null;
                }

                String locator = objMap.GetLocator(elementName);

                String type = GetWebDriverLocator(locator).GetAttribute("type");

                /**String temp = getwebDriverLocator(locator).GetAttribute("value");*/

                if (type == null)
                {
                    return GetWebDriverLocator(locator).Text;
                }

                if (type.Equals("text", StringComparison.OrdinalIgnoreCase)
                  || type.Equals("textArea", StringComparison.OrdinalIgnoreCase)
                  || type.Equals("text", StringComparison.OrdinalIgnoreCase)
                  || type.Equals("password", StringComparison.OrdinalIgnoreCase))
                {

                    if (GetWebDriverLocator(locator).Text.Equals(""))
                    {
                        return GetWebDriverLocator(locator).GetAttribute("value");
                    }
                    return GetWebDriverLocator(locator).Text;

                }


                if (type.Equals("select-one", StringComparison.OrdinalIgnoreCase)
                        || type.Equals("select", StringComparison.OrdinalIgnoreCase))
                {
                    SelectElement selectOption = new SelectElement(GetWebDriverLocator(locator));
                    return selectOption.SelectedOption.Text;


                }
                if (type.Equals("checkBox", StringComparison.OrdinalIgnoreCase)
                        || type.Equals("radio", StringComparison.OrdinalIgnoreCase))
                {
                    string v = GetWebDriverLocator(locator).Selected ? "yes" : "No";
                    return v;
                }
                else
                {
                    return GetWebDriverLocator(locator).Text;
                }

            }
            catch (ObjectDisposedException e)
            {
                logger.Trace("Error while getting value of element " + elementName,
                        e);
                logger.HandleError("Error" + e.Message);
            }

            return null;
        }
        /**
	     * Clears the value of specified element
	     * 
	     * @param elementName
	     *            name of element
	     */
        public Boolean Clear(String elementName)
        {
            try
            {
                GetWebDriverLocator(objMap.GetLocator(elementName)).Clear();

                return true;
            }
            catch (ObjectDisposedException e)
            {
                logger.HandleError("Failed to clear  " + elementName + ".Exception is " + e.Message);
                return false;
            }

        }


        /**
	     * Navigates to the Menu
	     * 
	     * @param menuList
	     *            list of menu items
	     */
        public void NavigateMenu(String menuList)
        {
            if(menuList==null)
            {
                throw new ArgumentNullException();
            }
            String[] aMenu;
            aMenu = menuList.Split(new[] { ',' });
            try
            {
                for (int i = 0; i < aMenu.Length; i++)
                {
                    if (!(aMenu[i].Equals("")))
                    {
                        GetWebDriverLocator(objMap.GetLocator(aMenu[i])).Click();
                        logger.Trace("Clicked on menu: "
                                + objMap.GetLocator(aMenu[i]) + ".");
                        WaitForBrowserStability(1000);
                    }
                }
            }
            catch (ObjectDisposedException e)
            {
                if (menuList.Contains(","))
                {
                    menuList = menuList.Replace(",", "-->");
                }
                logger.HandleError("Failed while Navigating to  " + menuList + ".Exception is " + e.Message);
            }
        }



        /**
	     * Checks the presence of element till timeout
	     * 
	     * @param locator
	     *            of the element
	     * @param timeInSec
	     *            time limit
	     * @return boolean result
	     */
        private Boolean CheckElement(String locator, int timeInSec)
        {

            /** IWebElement webElement = getwebDriverLocator(locator); */
            IWebElement webElement;
            try
            {
                webElement = GetWebDriverLocator(objMap.GetLocator(locator));
            }
            catch (Exception)
            {
                webElement = GetWebDriverLocator(locator);

            }
            Boolean result = false;
            try
            {
                for (int second = 0; ; second++)
                {

                    if (second >= timeInSec * 2)
                    {
                        /** logger.trace("TimeOut waiting for " + locator + " "
                                + (second / 2) + " Seconds");*/
                        logger.Trace("Error: Caused while Verifying the Presence of Element \" "
                        + locator + " \"");
                        break;
                    }
                    try
                    {
                        if (webElement.Displayed)
                        {
                            result = true;
                            break;
                        }
                    }
                    catch (ObjectDisposedException e)
                    {
                        logger.HandleError("Error" + e.Message);
                    }
                    /**waitForElement.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(locator));*/
                }
            }
            catch (ObjectDisposedException e)
            {
                logger.HandleError("Error" + e.Message);
                return result;

                /**  logger.trace("Error: Caused while Verifying the Presence of Element \" "
                         + locator + " \"");*/
            }

            return result;
        }


        /**
	     * Checks the presence of element till timeout
	     * 
	     * @param elementName
	     *            name of the element
	     * @param timeInSec
	     *            time limit
	     * 
	     */
        public Boolean CheckElementPresent(String elementName, int timeInSec)
        {
            return CheckElement(objMap.GetLocator(elementName), timeInSec);
        }




        /**
     * Check for the presence of the specified text on page till timeout
     * 
     * @param text
     *            text to be verified
     * @param timeInSec
     *            time-limit(in seconds)
     * @return boolean Result
     */
        public Boolean CheckTextOnPage(String text, int timeInSec)
        {
            Boolean result = false;
            WebDriverWait waitForPage = new WebDriverWait(webDr, TimeSpan.FromMilliseconds(500));

            try
            {
                for (int second = 0; ; second++)
                {
                    if (second >= timeInSec * 10)
                    {
                        logger.Trace("TimeOut for " + second);
                        break;
                    }

                    if (webDr.FindElement(By.TagName("body")).Text.Contains(text))
                    {
                        result = true;
                        TestResultLogger.Passed("checkText", text + " should be present on page", "Text is present on the page");
                        break;
                    }

                    waitForPage.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(By.TagName("body")));

                }
            }
            catch (ObjectDisposedException e)
            {
                TestResultLogger.Passed("checkText", text + " should be present on page", "Text is not present on the page");
                logger.HandleError("Error: Caused while Verifying the Presence of Text \" " + text + " \"", e);
            }
            return result;
        }


        /**
         * Clicks on the specified element
         * 
         * @param element
         *            to be clicked
         */

        public void Click(String element)
        {
            try
            {
                /** checkElementPresent(element, 60);*/
                GetWebDriverLocator(objMap.GetLocator(element)).Click();
            }
            catch (ObjectDisposedException e)
            {
                logger.HandleError("Failed to click on the element ", element, " ",
                        e);
            }

        }


        /**
         * Mouse overs on the specified menu and clicks on the sub menu.
         * 
         * @param menu
         *            of the elements
         * @param subMenu
         *            menu to be clicked
         */

        public void MouseOverAndClick(String menu, String subMenu)
        {
            try
            {
                IMouse mouse = MouseOver(menu);
                ILocatable hoverSubItem = (ILocatable)GetWebDriverLocator(objMap.GetLocator(subMenu));
                mouse = ((IHasInputDevices)webDr).Mouse;
                mouse.Click(hoverSubItem.Coordinates);
            }
            catch (ObjectDisposedException e)
            {
                logger.HandleError("Falied to click ", subMenu, " on", menu, " ", e);
            }
        }


        /**
         * Mouse overs on the specified element
         * 
         * @param elementName
         *            element name
         */

        public IMouse MouseOver(String elementName)
        {
            try
            {
                /**
                * WebElement link = webDr.findElement(By.xpath(objMap
                * .getLocator(elementName)));
                */
                IWebElement link = GetWebDriverLocator(objMap
                        .GetLocator(elementName));
                ILocatable hoverItem = (ILocatable)link;
                IMouse mouse = ((IHasInputDevices)webDr).Mouse;
                mouse.MouseMove(hoverItem.Coordinates);
                return mouse;
            }
            catch (ObjectDisposedException e)
            {
                logger.HandleError("Falied to hover on ", elementName + ".Exception is " + e.Message);
                return null;
            }

        }


        /**
         * Performs the drag and drop operation
         * 
         * @param fromLocator
         *            Contains the locator of source element
         * @param toLocator
         *            Contains the locator of destination element
         */
        public void DragAndDrop(String fromLocator, String toLocator)
        {
            try
            {
                IWebElement from = webDr.FindElement(By.XPath(objMap.GetLocator(fromLocator)));
                IWebElement to = webDr.FindElement(By.XPath(objMap.GetLocator(toLocator)));
                Actions builder = new Actions(webDr);
                IAction dragAndDrop = builder.ClickAndHold(from).MoveToElement(to).Release(to).Build();
                dragAndDrop.Perform();
            }
            catch (ObjectDisposedException e)
            {
                logger.HandleError("Failed to drag drop elements ", fromLocator,
                        " , ", toLocator, " ", e);
            }
        }


        /**
         * Switch to another window
         * 
         * @param title
         *            Contains title of the new window
         * @return true or false
         */
        public Boolean SwitchToWindow()
        {
            ReadOnlyCollection<String> availableWindows = webDr.WindowHandles;
            /** dynamic availableWindows = webDr.WindowHandles;*/
            if (availableWindows.Count > 1)
            {

                try
                {
                    /**for (String windowId : availableWindows) { */
                    foreach (String windowId in availableWindows)
                    /**foreach (dynamic windowId in availableWindows) */
                    {
                        webDr.SwitchTo().Window(windowId);
                        /** if (webDr.SwitchTo().Window(windowId).Title.Equals(title))
                        return true; */
                    }
                }
                catch (ObjectDisposedException e)
                {
                    logger.HandleError("No child window is available to switch ", e);
                }
            }

            return false;
        }


        /**
         * Uploads a file
         * 
         * @param filePath
         *            Contains path of the file which is to be uploaded
         * @param browse
         *            Contains locator of the browse button
         * @param upload
         *            locator of the upload button
         */
        public void FileUpload(String filePath, String browse, String upload)
        {

            IWebElement webElement = GetWebDriverLocator(browse);
            try
            {
                webElement.SendKeys(filePath);
                if (upload != null)
                {
                    Click("upload");
                }
            }
            catch (ObjectDisposedException e)
            {
                logger.HandleError("Invalid File Path: ", filePath, e);
            }

        }


        /**
	     * Invokes enter/tab/F5 key
	     * 
	     * @param keyEvent
	     *            key to be invoked
	     * 
	     */

        public void SendKey(String keyEvent)
        {
            if (keyEvent == null)
            {
                throw new ArgumentNullException();
            }
            try
            {
                if (keyEvent.Equals("enter", StringComparison.OrdinalIgnoreCase))
                {
                    try
                    {

                        Actions action = new Actions(webDr);
                        action.SendKeys(OpenQA.Selenium.Keys.Enter).Build().Perform();
                        WaitForBrowserStability(1000);
                        logger.Trace("Key event i.e. '" + keyEvent + "' is performed '");
                        return;
                    }
                    catch (ObjectDisposedException e)
                    {
                        logger.HandleError("Error" + e.Message); 
                    }
                }
                if (keyEvent.Equals("tab", StringComparison.OrdinalIgnoreCase))
                {
                    try
                    {
                        Actions action = new Actions(webDr);
                        action.SendKeys(OpenQA.Selenium.Keys.Tab).Build().Perform();
                        WaitForBrowserStability(5000);
                        logger.Trace("Key event i.e. '" + keyEvent + "' is performed '");
                        return;
                    }
                    catch (NullReferenceException e)
                    {
                        logger.HandleError("Error" + e.Message);
                    }
                }
                if (keyEvent.Equals("F5", StringComparison.OrdinalIgnoreCase))
                {
                    try
                    {
                        Actions action = new Actions(webDr);
                        action.SendKeys(OpenQA.Selenium.Keys.F5).Build().Perform();
                        WaitForBrowserStability(1000);
                        logger.Trace("Key event i.e. '" + keyEvent + "' is performed '");
                        return;
                    }
                    catch (ObjectDisposedException e)
                    {
                        logger.HandleError("Error" + e.Message);
                    }
                }
                logger.Trace("Error caused while clicking on '" + keyEvent);
            }
            catch (ObjectDisposedException e)
            {
                logger.HandleError("Error" + e.Message);
            }
        }


        /**
	     * Implements keyboard shortcuts
	     * 
	     * @param keyEvent
	     *            Give the shortcut you want to use. Example:
	     *            CONTROL_T,CONTROL_V etc.
	     */
        /**
         public void sendKeys(String keyEvent)
         {
             try
             {
                 string c;
                 //char c;
                 Actions action = new Actions(webDr);

                 try
                 {
                     String[] temp = keyEvent.Split(new char[] { '_' });
                     for (int i = 0; i < temp.Length; i++)
                     {
                         if (temp[i].Equals("CONTROL", StringComparison.InvariantCultureIgnoreCase))
                         {
                             action.SendKeys(OpenQA.Selenium.Keys.Control).Build().Perform();
                         }
                         else if (temp[i].Equals("ALT", StringComparison.InvariantCultureIgnoreCase))
                         {
                             action.SendKeys(OpenQA.Selenium.Keys.Alt).Build().Perform();
                         }
                         else if (temp[i].Equals("SHIFT", StringComparison.InvariantCultureIgnoreCase))
                         {
                             action.SendKeys(OpenQA.Selenium.Keys.Shift).Build().Perform();
                         }
                         else
                         {

                             //key.keyPress(Char.ToUpper(c));

                         }
                     }
                     //key.keyRelease(Character.toUpperCase(c));
                     action.SendKeys(OpenQA.Selenium.Keys.Shift).Build().Perform();
                     action.SendKeys(OpenQA.Selenium.Keys.Alt).Build().Perform();
                     action.SendKeys(OpenQA.Selenium.Keys.Control).Build().Perform();

                 }
                 catch (Exception e)
                 {
                     logger.trace("Error caused while clicking on '" + keyEvent);
                 }
             }
             catch (Exception e)
             { }
         }*/

        public void SendKeys(String keyEvent)
        {
            if (keyEvent == null)
            {
                throw new ArgumentNullException();
            }
            try
            {
                /** string c;
               char c; */
                Actions action = new Actions(webDr);

                try
                {
                    String[] tempe = keyEvent.Split(new[] { '_' });
                    for (int i = 0; i < tempe.Length; i++)
                    {
                        if (tempe[i].Equals("CONTROL", StringComparison.OrdinalIgnoreCase))
                        {
                            action.SendKeys(OpenQA.Selenium.Keys.Control).Build().Perform();
                        }
                        else if (tempe[i].Equals("ALT", StringComparison.OrdinalIgnoreCase))
                        {
                            action.SendKeys(OpenQA.Selenium.Keys.Alt).Build().Perform();
                        }
                        else if (tempe[i].Equals("SHIFT", StringComparison.OrdinalIgnoreCase))
                        {
                            action.SendKeys(OpenQA.Selenium.Keys.Shift).Build().Perform();
                        }
                        else
                        {
                            action.SendKeys(tempe[i]);
                            /**key.keyPress(Char.ToUpper(c)); */

                        }
                    }
                    /** key.keyRelease(Character.toUpperCase(c)); */
                    action.SendKeys(OpenQA.Selenium.Keys.Shift).Build().Perform();
                    action.SendKeys(OpenQA.Selenium.Keys.Alt).Build().Perform();
                    action.SendKeys(OpenQA.Selenium.Keys.Control).Build().Perform();

                }
                catch (ObjectDisposedException e)
                {
                    logger.Trace("Error caused while clicking on '" + keyEvent + ".Exception is " + e.Message);
                }
            }
            catch (ObjectDisposedException e)
            {
                logger.HandleError("Error" + e.Message);
            }
        }

        /**
         
         * Compares actual and expected messages from the application
         * 
         * @param element
         *            for which message to be checked
         * @param expectedMsg
         *            expected message
         * @return boolean result
         */
        public Boolean VerifyMsg(String element, String expectedMsg)
        {

            String actualMsg = GetValue(element);
            if (actualMsg.Equals(expectedMsg, StringComparison.OrdinalIgnoreCase))
            {
                return true;
            }
            return false;
        }

        /**
        * Overriding toString() method to return WebUIDriver format string
        */
        public String TooString()
        {
            return StringUtils.MapString(this);
        }


        /**
     * Check the running process and kill it
     * 
     * @param serviceName
     *            Give name of the process that you want to kill
     * @return Boolean
     * @throws IOException
     */


        public void KillProcess(String serviceName)
        {

            /**foreach (System.Diagnostics.Process myProc in System.Diagnostics.Process.GetProcessesByName(serviceName))
            {
             if (myProc.ProcessName == serviceName)
            {
                myProc.Kill();
             }
             } */

            foreach (System.Diagnostics.Process myProc in System.Diagnostics.Process.GetProcesses())
            {
                if (myProc.ProcessName == serviceName)
                {
                    myProc.Kill();
                }
            }


        }

        /**
	     * Verify Tooltip's text
	     * 
	     
	     *            Give element for which tooltip is visible
	     
	     *            expected tooltip text
	     */

        public void VerifyTooltip(String elementName, String expected)
        {
            try
            {
                String locator = objMap.GetLocator(elementName);

                IWebElement element = GetWebDriverLocator(locator);
                String str = null;
                try
                {
                    if (!string.IsNullOrEmpty(element.GetAttribute("title").ToString()) || !string.IsNullOrWhiteSpace(element.GetAttribute("title").ToString()))
                    {

                        str = element.GetAttribute("title");
                        Console.WriteLine("Tooltip text:" + str);
                        if (str.Contains(expected))
                        {
                            TestResultLogger.Passed("verify tooltip: " + str + "for " + element, "Tooltip's text should match with " + str + "for " + element, "Tooltip's" +
                                " text matches with " + str + "for " + element);

                        }
                        else
                        {
                            TestResultLogger.Failed("verify tooltip: " + str, "Tooltip's text should match with " + str, "Tooltip's text doesn't match with " + str);

                        }
                    }
                    else
                    {
                        TestResultLogger.Failed("verify tooltip: " + str + "for " + element, str + " not Visible", "Tooltip" + str + " is not visible for " + element);

                    }
                }
                catch (ObjectDisposedException e)
                {
                    logger.HandleError("Error: verifyTooltip on element " + elementName + " ", e.Message);
                    /** throw e; */
                }
            }
            catch (ObjectDisposedException e)
            {
                logger.HandleError("Error: verifyTooltip on element " + elementName + " ", e.Message);
            }
        }


        /**
	     * Verifies text in alert and clicks on either OK or cancel.
	     * 
	     * @param text
	     *            expected text
	     * @param button
	     *            "OK" or "Cancel" button
	     */

        public void VerifyTextinAlert(String text, String button)
        {
            if (button == null)
            {
                throw new ArgumentNullException();
            }
            IAlert alt = webDr.SwitchTo().Alert();
            String alerttext = alt.Text;
            if (alerttext.Equals(text))
            {
                TestResultLogger.Passed("verify " + text + " ", text + " ", text + " is present");
                /** passed("verify " + text + " in alert", text+ " should present in alert", text+ " is present in  alert"); */
            }
            else
            {
                TestResultLogger.Failed("verify " + text + " in alert", text + " should present in alert", text + " is not present in  alert");
                /** failed("verify " + text + " in alert", text+ " should present in alert", text+ " is not present in  alert"); */
            }
            if (button.Equals("yes", StringComparison.OrdinalIgnoreCase) || button.Equals("ok", StringComparison.OrdinalIgnoreCase))
            {
                alt.Accept();
            }
            else
            {
                alt.Dismiss();
            }

        }
        /**
	 * For right clicking on any element
	 * 
	 * @param elementName
	 *            name of element
	 */

        public void RightClick(String elementName, int option)
        {
            Actions action = new Actions(webDr);
            Actions act = null;
            for (int i = 0; i < option; i++)
            {
                act = action.ContextClick(GetControl(elementName)).SendKeys(OpenQA.Selenium.Keys.ArrowDown);
                act.Build().Perform();

            }
            act.SendKeys(OpenQA.Selenium.Keys.Enter).Build().Perform();

        }

        /**
 * Identifies the locator as needed by webDriver object
 * 
 * @param elementName
 *            Name of the element whose locator is required
 * @return Identified locator as Web Element
 */

        public IWebElement GetControl(String elementName)
        {
            String actualLocator = (String)objMap.GetElementMap(elementName)["locator"];
            IWebElement element = null;
            Int32 index = 1;

            By[] byCollection = { By.Id(actualLocator),
                By.Name(actualLocator), By.XPath(actualLocator),
                By.ClassName(actualLocator), By.CssSelector(actualLocator),
                By.TagName(actualLocator), By.LinkText(actualLocator),
                By.PartialLinkText(actualLocator) };


            foreach (By by in byCollection)
            {
                try
                {
                    element = webDr.FindElement(by);
                    if (!element.Equals(null))
                    {
                        break;
                    }
                }
                catch (ObjectDisposedException e)
                {
                    if (index == byCollection.Length)
                    {
                        logger.HandleError("Unable to find element ", elementName,
                                e);
                    }
                    else
                    {
                        index++;
                        continue;
                    }
                }
            }
            return element;
        }


        /**
	 * Compares actual and expected text from the application
	 * 
	 * @param elementName
	 *            element for which text is to be checked
	 * @param expectedText
	 *            expected text
	 * @return boolean result
	 */
        public Boolean VerifyText(String elementName, String expectedText)
        {
            String actualText = GetValue(elementName);
            if (actualText.Equals(expectedText, StringComparison.OrdinalIgnoreCase))
            {
                return true;
            }
            return false;
        }

        /**
            * Clicks on OK or Cancel button on alert box
            * 
            * @param button
            *            "OK" or "Cancel" button
            */
        public void HandleAlert(String button)
        {
            if (button == null)
            {
                throw new ArgumentNullException();
            }
            try
            {
                IAlert alt = webDr.SwitchTo().Alert();

                if (button.Equals("yes", StringComparison.OrdinalIgnoreCase) || button.Equals("ok", StringComparison.OrdinalIgnoreCase))
                {
                    alt.Accept();
                }
                else
                {
                    alt.Dismiss();
                }
            }
            catch (UnhandledAlertException e)
            {
                logger.HandleError("Failed to handle alert with ", button,
                        " button" + ".Exception is " + e.Message);
            }
        }


        /**
            * Sets the text in the alert box
            * 
            * @param value
            *            text to be set in alert box
            */

        public void TextinAlert()
        {

            IAlert alt = webDr.SwitchTo().Alert();
            /**sendKeys(value);
           alt.SendKeys(value); */
            alt.Accept();
        }


        /**
        * Gets the text from the alert box
        * 
        * @return String
        */

        public String AlertText()
        {

            IAlert alt = webDr.SwitchTo().Alert();
            return alt.Text;
        }

        /**
            * Verify Alert
            * 
            * @param TimeOutinSeconds
            *            Give max time limit
            * @return boolean
            */

        public Boolean IsAlertPresent(int TimeOutinSeconds)
        {

            for (int i = 0; i < TimeOutinSeconds; i++)
            {
                try
                {
                    Thread.Sleep(500);
                    webDr.SwitchTo().Alert();
                    return true;
                }
                catch (ThreadAbortException e)
                {
                    logger.HandleError("Error" + e.Message);
                }
            }
            return false;
        }


        /**
	     * For highlighting an element
	     * 
	     * @param elementName
	     *            Locator
	     */

        public void DrawHighlight(String elementName)
        {
            try
            {
                IWebElement element = GetWebDriverLocator(objMap.GetLocator(elementName));
                if (startUp.Equals(BrowserType.FIREFOX.ToString(), StringComparison.OrdinalIgnoreCase)
                        || startUp.Equals(BrowserType.CHROME.ToString(), StringComparison.OrdinalIgnoreCase))
                {

                    for (int i = 0; i < 2; i++)
                    {
                        IJavaScriptExecutor js = (IJavaScriptExecutor)webDr;
                        js.ExecuteScript(
                                "arguments[0].setAttribute('style', arguments[1]);",
                                element, "color: red; border: 5px solid red;");
                        Thread.Sleep(500);
                        js.ExecuteScript(
                                "arguments[0].setAttribute('style', arguments[1]);",
                                element, "");
                    }
                }

                if (startUp.Equals(BrowserType.IE.ToString(), StringComparison.OrdinalIgnoreCase))
                {
                    IJavaScriptExecutor js = ((IJavaScriptExecutor)webDr);

                    for (int i = 0; i < 2; i++)
                    {
                        js.ExecuteScript("arguments[0].style.border='4px solid red'",
                                element);
                        js.ExecuteScript("arguments[0].style.backgroundColor = '"
                                + "rgb(0,200,0)" + "'", element);
                        /**js.ExecuteScript(bgcolor, element, js);*/
                        Thread.Sleep(500);
                        js.ExecuteScript("arguments[0].style.border='none'", element);
                    }
                }
            }
            catch (ThreadInterruptedException e)
            {
                logger.HandleError("Error" + e.Message);
            }

        }


        /**
	     * Retrieve drop down options
	     * 
	     * @param elementName
	     *            name of element
	     * 
	     */
        public List<String> GetDropDownOptions(String elementName)
        {
            List<String> optionsStr = new List<String>();
            IWebElement element = GetWebDriverLocator(objMap.GetLocator(elementName));
            ReadOnlyCollection<IWebElement> options = element.FindElements(By.TagName("option"));
            try
            {
                /**for (WebElement option : options) { */
                foreach (IWebElement option in options)
                {
                    optionsStr.Add(option.Text);

                }

            }
            catch (ObjectDisposedException e)
            {
                logger.HandleError("Failed to get dropdown list for: ",
                        elementName, " ", e);
            }

            return optionsStr;
        }



        /**
         * checks the presence of specified options in the dropdown
         * 
         * @param elementName
         *            name of element
         * @param optionsStr
         *            options to be checked
         * 
         * 
         */
        public Boolean CheckDropDownOptions(String elementName, String optionsStr)
        {
            if (optionsStr == null)
            {
                throw new ArgumentNullException();
            }
            List<Object> flag = new List<Object>();
            List<String> dropDownOptions = GetDropDownOptions(elementName);
            List<String> dropDownOptionsLowerCase = new List<String>();
            /**for (String temp : dropDownOptions) { */
            foreach (String temp in dropDownOptions)
            {
                dropDownOptionsLowerCase.Add(temp.Trim().ToLower(CultureInfo.CurrentCulture));
            }
            String[] dropDownOptionsActList = optionsStr.Split(new[] { ',' });
            try
            {
                for (int i = 0; i < dropDownOptionsActList.Length; i++)
                {
                    Console.WriteLine(dropDownOptionsLowerCase.ToString());
                    Console.WriteLine(i + "---"
                            + dropDownOptionsActList[i].Trim().ToLower(CultureInfo.InvariantCulture));
                    try
                    {
                        bool val = dropDownOptionsLowerCase
                                  .Contains(dropDownOptionsActList[i].Trim()
                                          .ToLower(CultureInfo.InvariantCulture)) ? true : false;

                        flag.Add(val);
                    }
                    catch (NullReferenceException e)
                    {
                        logger.HandleError("Failed to verify option: ", optionsStr, "for ",
                        elementName, " ", e);
                    }
                }

            }
            catch (NullReferenceException e)
            {
                logger.HandleError("Failed to verify option: ", optionsStr, "for ",
                        elementName, " ", e);
            }
            bool value = flag.Contains(true) || false;
            return value;
        }


        /**
         * checks whether the element is readonly or not
         * 
         * @param elementName
         *            name of element
         */
        public Boolean IsReadonly(String elementName)
        {
            IWebElement element = GetWebDriverLocator(objMap.GetLocator(elementName));
            String valueReadonly = element.GetAttribute("readonly");
            if (valueReadonly == null)
            {
                valueReadonly = "";
            }
            String valueDisabled = element.GetAttribute("disabled");
            if (valueDisabled == null)
            {
                valueDisabled = "";
            }
            Boolean result = false;
            try
            {
                if (valueReadonly.Equals("true") || valueDisabled.Equals("true"))
                {
                    result = true;
                }
            }
            catch (NullReferenceException e)
            {
                logger.HandleError("Failed to get 'readOnly' attribute for ",
                        elementName, " ", e);
            }
            return result;
        }


        /**
            * checks whether the element is checked or not
            * 
            * @param elementName
            *            name of element
            */
        public Boolean IsChecked(String elementName)
        {
            IWebElement element = GetWebDriverLocator(objMap.GetLocator(elementName));
            String valueChecked = element.GetAttribute("checked");
            Boolean result = false;
            try
            {
                if (valueChecked.Equals("true"))
                {

                    result = true;
                }
            }
            catch (NullReferenceException e)
            {
                logger.HandleError("Failed to get 'checked' attribute for ",
                        elementName, " ", e);
            }
            return result;
        }

        /**
            * Sets the attribute value
            * 
            * @param elementName
            *            name of element
            * 
            * @param attribute
            *            attribute to be set
            * @param value
            *            value of the attribute
            */
        public void SetAttribute(String elementName, String attribute, String value)
        {
            try
            {
                /**
               IWebElement element = webDr.FindElement(By.XPath(objMap
                       .getLocator(elementName)));
               IJavaScriptExecutor js = (IJavaScriptExecutor)webDr;
               js.ExecuteScript(
                       "arguments[0].setAttribute(arguments[1], arguments[2])",
                       element, attribute, value);*/
                IWebElement element = GetWebDriverLocator(objMap.GetLocator(elementName));
                IJavaScriptExecutor js = (IJavaScriptExecutor)webDr;
                js.ExecuteScript(
                        "arguments[0].setAttribute(arguments[1], arguments[2])",
                        element, attribute, value);


            }
            catch (ObjectDisposedException e)
            {
                logger.HandleError("Failed to set ", value, " for attribute ",
                        attribute, " for the element ", elementName, " ", e);
            }
        }



        /**
        * Gets the attribute value
        * 
        * @param elementName
        *            name of element
        * 
        * @param attribute
        *            attribute name
        * */
        public String GetAttribute(String elementName, String attribute)
        {
            IWebElement element = GetWebDriverLocator(objMap.GetLocator(elementName));
            try
            {
                String value = element.GetAttribute(attribute);
                return value;
            }
            catch (ObjectDisposedException e)
            {
                logger.HandleError("Failed to get value for attribute ", attribute,
                        " for the element ", elementName, " ", e);
                return null;
            }
        }

        /**
            * Executes Javascript
            * 
            * @param script
            *            script to be executed
            **/
        public void ExecuteJavaScript(String script)
        {
            try
            {
                IJavaScriptExecutor js = (IJavaScriptExecutor)webDr;
                js.ExecuteScript(script);

            }
            catch (ObjectDisposedException e)
            {
                logger.HandleError("Falied to execute ", script + ".Exception is " + e.Message);
            }
        }


        /**
         * checks whether the element is displayed or not
         * 
         * @param element
         *            name of element
         **/
        public Boolean IsDisplayed(String element)
        {
            Boolean val = GetWebDriverLocator(objMap.GetLocator(element)).Displayed;
            return val;
        }

        /**
         * For Double clicking on any element
         * 
         * @param elementName
         *            name of element
         */

        public void Doubleclick(String elementName)
        {
            Actions axn = new Actions(webDr);
            try
            {
                axn.DoubleClick(GetWebDriverLocator(objMap.GetLocator(elementName))).Build().Perform();
            }
            catch (ObjectDisposedException e)
            {
                logger.HandleError("Failed to double click on " + elementName + ".Exception is " + e.Message);
            }
        }



        /**
	     * Shut downs the webdriver
	     */
        public void CloseBrowsers()
        {
            if (webDr != null)
            {
                webDr.Quit();
            }

            /**TestResultLogger.passed("Close browser", "Browser should be closed successfully",
              "Browser is closed successfully"); */
        }

        /**
         * Takes screenshot
         */
        public void TakeScreenShot(String filePath)
        {

            try
            {
                Screenshot ss = ((ITakesScreenshot)webDr).GetScreenshot();
                ss.SaveAsFile(@filePath, System.Drawing.Imaging.ImageFormat.Png);

            }
            catch (FileNotFoundException e)
            {
                logger.HandleError("Failed to take screenshots" + ".Exception is " + e.Message);
            }

        }

        /**
         * Clear cookies
         */
        public void ClearCookies()
        {
            try
            {
                webDr.Manage().Cookies.DeleteAllCookies();
            }
            catch (ObjectDisposedException e)
            {

                logger.HandleError("Failed to clear cookies" + ".Exception is " + e.Message);
            }
        }

        /**
         * Check the running process and kill it
         * 
         * @param serviceName
         *            Give name of the process that you want to kill
         * @return Boolean
         */

        public void IsProcessRunningAndKill(String serviceName)
        {
            foreach (var process in Process.GetProcessesByName(serviceName))
            {
                process.Kill();
            }
        }

        /**
         * Checks whether the page is loaded or not
         * 
         * @param maxTimeInSec
         *            time to wait(In seconds)
         * @return boolean result
         */
        public Boolean WaitForBrowserStability(int maxWait)
        {
            Boolean bResult = false;
            /** int maxWait = Int32.Parse(maxTimeInSec); */
            int secsWaited = 0;

            try
            {
                do
                {
                    Thread.Sleep(100);
                    secsWaited++;
                    if (IsBrowserLoaded())
                    {
                        bResult = true;
                        break;
                    }
                } while (secsWaited < (maxWait * 10));
                Thread.Sleep(100);
            }
            catch (NullReferenceException e)
            {
                logger.Trace("Exception caught while waiting for the page to load " + ".Exception is " + e.Message);
                bResult = false;
            }
            return bResult;
        }

        /**
         * Checks if body of the page is loaded or not
         * 
         * @return Boolean Result
         */
        public static double CurrentMilli()
        {
            DateTime Jan1970 = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan javaSpan = DateTime.UtcNow - Jan1970;
            return javaSpan.TotalMilliseconds;
        }



        public Boolean IsBrowserLoaded()
        {
            Boolean isBrowserLoaded = false;
            try
            {
                double timeOut = 5000;
                double end = CurrentMilli() + timeOut;

                IJavaScriptExecutor js = (IJavaScriptExecutor)webDr;
                while (CurrentMilli() < end)
                {
                    try
                    {
                        if (js.ExecuteScript("return document.readyState").Equals("complete"))
                        {
                            isBrowserLoaded = true;
                            break;
                        }

                    }
                    catch (NullReferenceException e)
                    {
                        logger.HandleError("Failed to check for the browser to load", e);
                    }

                }

            }
            catch (NullReferenceException e)
            {
                logger.HandleError("Failed to check for the browser to load", e);
            }
            return isBrowserLoaded;
        }





        /**
         * Checks the attribute value
         * 
         * @param elementName
         *            name of element
         * 
         * @param attribute
         *            attribute to be set
         * @param value
         *            value of the attribute
         */
        public bool CheckAtttribute(String elementName, String attribute, String value)
        {
            if (value == null)
            {
                throw new ArgumentNullException();
            }
            String actualValue = GetAttribute(elementName, attribute);
            if (value.Equals(actualValue))
            {
                return true;
            }

            return false;
        }

        /**
           * Checks the read only attribute or enable attribute for the input field
           * 
           * @param elementName
           *            Name of the element
           */

        public Boolean CheckEditable(String elementName)
        {

            String element = "";
            /** String value = ""; */
            Boolean value = false;

            try
            {

                element = objMap.GetLocator(elementName);
                if (element == null)
                {
                    logger.Error("Element properties are not provided in the uiMap file", elementName);
                    /** return null; */
                    return false;
                }
                if (element != null)
                {
                    IWebElement locator = GetWebDriverLocator(element);
                    bool result = false;
                    if (locator.Enabled)
                    {

                        String result1 = locator.GetAttribute("readonly");

                        if (result1 == null)
                        {
                            result1 = "false";
                        }

                        if (result1.Equals("true", StringComparison.OrdinalIgnoreCase))
                        {

                            result = (locator.GetAttribute("readonly").Equals("true", StringComparison.OrdinalIgnoreCase)) || (locator.GetAttribute("readonly")
                                            .Equals("", StringComparison.OrdinalIgnoreCase))
                                    || (locator.GetAttribute("readonly")
                                            .Equals("readonly", StringComparison.OrdinalIgnoreCase));

                            if (result)
                            {
                                logger.Trace("Field is enabled and readonly for the element'"
                                        + elementName + "'");

                                value = false;
                            }
                            if (locator.GetAttribute("readonly")
                                  .Equals("false", StringComparison.OrdinalIgnoreCase))
                            {

                                logger.Trace("Field is enabled'" + elementName
                                        + "'");
                                value = true;
                            }
                            else
                            {

                                logger.Trace("Field is enabled and readonly for the element'"
                                        + elementName + "'");
                                value = false;
                            }
                        }
                        else
                        {
                            /** logger.trace("Field is enabled for the element \" "
                                   + elementName + " \""); */

                            value = true;
                            /**
                           logger.trace("Field is enabled for the element '" + elementName
                                   + "' in the input element '" + elementName
                                   + "'");*/

                        }
                    }
                }
            }
            catch (ObjectDisposedException e)
            {
                logger.Error("Failed while checking the edibility of" + elementName + ".Exception is " + e.Message);
            }

            return value;

        }

        /**
	 * Checks the presence of element
	 * 
	 * @param elementName
	 *            name of the element
	 * @return Boolean Result
	 */
        public bool CheckVisible(String elementName)
        {

            bool result = false;
            try
            {
                if (GetControl(elementName).Displayed)
                {
                    result = true;
                }
            }

            catch (NullReferenceException e)
            {
                logger.Trace("Error: Caused while Verifying the Presence of Element \" "
                        + elementName + " \"" + ".Exception is " + e.Message);
            }

            return result;
        }

        public void ClickFromData(String sValue)
        {
            if (sValue == null)
            {
                throw new ArgumentNullException();
            }
            /** Map result = null; */
            string element = "";
            String[] parts = null;
            String valueForClick = null;
            if (sValue.Contains(","))
            {
                parts = sValue.Split(',');
                for (int i = 0; i < parts.Length; i++)
                {
                    valueForClick = parts[i];
                    try
                    {
                        element = objMap.GetLocator(valueForClick);
                        IWebElement webElement = GetWebDriverLocator(element);
                        webElement.Click();
                        Thread.Sleep(1000);
                    }
                    catch (ObjectDisposedException e)
                    {
                        logger.HandleError("Error" + e.Message);
                    }
                }

            }

        }

        /**
	     * Reads locators and returns List of it
	     * 
	     * @return List of locators
	     */
        public List<Dictionary<string, dynamic>> ReadLocators()
        {
            List<Dictionary<string, dynamic>> locators = new List<Dictionary<string, dynamic>>();

            try
            {
                /**	ExcelAccess.accessSheet(ResourcePaths.singleton.getSuiteResource("Plan/AppDriver/OR", "uiMap.xls"),"locators",
                           new ExcelAccess.RowArrayBuilder(locators));*/
                if (File.Exists(System.Reflection.Assembly.GetExecutingAssembly().Location + "Plan//AppDriver//OR//uiMap.xls"))
                {
                    ExcelAccess.AccessLocatorSheet(System.Reflection.Assembly.GetExecutingAssembly().Location + "Plan/AppDriver/OR/uiMap.xls", new ExcelAccess.RowArrayBuilder(locators));
                }
                else
                {
                    ExcelAccess.AccessLocatorSheet(System.Reflection.Assembly.GetExecutingAssembly().Location + "Plan/AppDriver/OR/uiMap.xlsx", new ExcelAccess.RowArrayBuilder(locators));
                }


            }
            catch (FileNotFoundException e)
            {
                /** TODO Auto-generated catch block */
                Console.WriteLine(e.StackTrace);
            }

            return locators;

        }


        public String GetLocator(String sElementName)
        {
            if (sElementName == null)
            {
                throw new ArgumentNullException();
            }
            Dictionary<String, dynamic> result = null;
            String sLocator = null, sParentLocator = null, parentTemp = null;
            try
            {
                uiMap = ReadLocators();
                foreach (Dictionary<String, dynamic> map in uiMap)
                {
                    String temporary = (String)map["elementName"];
                    if (sElementName.Equals(temporary))
                    {
                        result = map;
                    }
                }
                parentTemp = (String)result["parentName"];
                if (parentTemp != null && !parentTemp.Equals(""))
                {

                    foreach (Dictionary<String, dynamic> map1 in uiMap)
                    {
                        String elementName = (String)map1["elementName"];
                        String Locator = (String)map1["locator"];
                        if ((parentTemp.Equals(elementName)) && (Locator != null))
                        {
                            sParentLocator = Locator;
                        }
                    }
                    sLocator = sParentLocator + (String)result["locator"];
                }
                else
                {
                    sLocator = (String)result["locator"];
                }
            }

            catch (NullReferenceException e)
            {
                logger.Trace("Error: Trying to retrieve the Locator of Unknown Element \" " + sElementName + " \"", e);
            }
            return sLocator;
        }

        public void CloseAlert(String button)
        {
            if (button == null)
            {
                throw new ArgumentNullException();
            }
            IAlert alert = webDr.SwitchTo().Alert();
            try
            {
                if (button.Equals("yes", StringComparison.OrdinalIgnoreCase) || button.Equals("ok", StringComparison.OrdinalIgnoreCase))
                {
                    alert.Accept();

                }
                else
                {
                    alert.Dismiss();
                }
            }
            catch (AccessViolationException e)
            {
                logger.HandleError("Error while verifying button name:" + button, e);
            }
        }



        /**
         * Check whether text is present on the page
         * 
         * @param text
         *            Give name of the text you want to search
         * 
         * @return Boolean
         */
        public bool IsTextPresent(String text)
        {
            IList<IWebElement> foundElements = webDr.FindElements(By.XPath("//*[contains(text(), '" + text + "')]"));

            return foundElements.Count > 0;

        }


        /**
         * Sets the focus to given frame
         * 
         * @param frame
         *            Locator
         * 
         */
        public void SetFrame(String frame)
        {
            try
            {
                if (CheckVisible(frame))
                {
                    IWebElement locator = GetControl(frame);
                    webDr.SwitchTo().Frame(locator);
                    /** webDr.SwitchTo().Frame(frame); */
                    /** logger.trace("Navigated to frame with element name" + frame); */
                }
            }
            catch (NoSuchFrameException e)
            {
                TestResultLogger.Warning("Set Frame: " + frame, "Frame " + frame + " should be set", "No such frame found");
                /**
               TestResultLogger.passed("Set frame :  " + frame, "Frame"
                       + frame + "should be set", "Frame"
                       + frame + "is set");*/
                logger.Trace("Unable to locate frame with element name " + frame + ".Exception is " + e.Message);
            }
            catch (FormatException e)
            {
                TestResultLogger.Failed("Set frame :  " + frame, "Frame"
                        + frame + "should be set", "Frame"
                        + frame + "is set");
                logger.Trace("Unable to navigate to frame with element name "
                        + frame + ".Exception is " + e.Message);
            }

        }

        /**
         * Return the focus to the parent frame
         * 
         */
        public void ResetFrame()
        {
            try
            {
                /**webDr.SwitchTo().ParentFrame(); */
                webDr.SwitchTo().DefaultContent();
                logger.Trace("Navigated back to webpage from frame");

            }
            catch (FormatException e)
            {
                TestResultLogger.Failed("Reset frame", "Frame should get reset", "Frame is not reset");
                logger.Trace("Unable to navigate back to main webpage from frame" + e.StackTrace);
            }

        }

        /**
	 * Choose appropriate property before acting on control
	 * 
	 * @param elementName
	 *            name of the element
	 * @param timeOutValue
	 *            time out value in seconds
	 * @return Boolean Result
	 */
        public bool WaitForObject(String elementName, int timeOutValue)
        {

            /**IWebElement webElement = getControl(elementName); */

            String element = "";

            bool result = false;
            try
            {
                element = objMap.GetLocator(elementName);
                if (element != null)
                {
                    for (int sec = 0; sec < timeOutValue; sec++)
                    {
                        IWebElement webElement = GetWebDriverLocator(element);
                        Thread.Sleep(1000);
                        if (webElement.Displayed)
                        {
                            result = true;
                        }

                    }
                }
            }

            catch (NotSupportedException e)
            {
                logger.Trace("Error: Caused while waiting for \" " + elementName
                       + " \"" + ".Exception is " + e.Message);
            }

            return result;
        }


        public void DragAndDrop(String fromLocator, String toLocator, String fromRowNum)
        {
            IWebElement from;
            try
            {
                String result = objMap.GetLocator(fromLocator);
                if (result.Contains("$"))
                {
                    from = webDr.FindElement(By.XPath(result.Replace("$", fromRowNum)));
                }
                else
                {
                    from = webDr.FindElement(By.XPath(result));
                }

                IWebElement to = webDr.FindElement(By.Id(objMap.GetLocator(toLocator)));
                Actions builder = new Actions(webDr);
                IAction dragAndDrop = builder.ClickAndHold(from).MoveToElement(to).Release(to).Build();
                dragAndDrop.Perform();
            }
            catch (NotSupportedException e)
            {
                logger.HandleError("Failed to drag drop elements ", fromLocator,
                        " , ", toLocator, " ", e);
            }
        }


        /**
         * Initialize parameters
         * 
         * @param params
         * 
         */
        private void InitializeParameters(Dictionary<String, dynamic> @paramas)
        {
            if (!@paramas.ContainsKey("browser"))
            {
                foreach (KeyValuePair<String, dynamic> temploc in temp)
                {
                    if (temploc.Key != "parameters")
                    {
                        if (temploc.Key == "browser")
                        {
                            @paramas.Add(temploc.Key, temploc.Value);
                        }
                        if (temploc.Key == "browserdriverfolder")
                        {
                            @paramas.Add(temploc.Key, temploc.Value);
                        }
                        if (temploc.Key == "plugin")
                        {
                            @paramas.Add(temploc.Key, temploc.Value);
                        }
                        if (temploc.Key == "proxy")
                        {
                            @paramas.Add(temploc.Key, temploc.Value);
                        }
                    }
                }

            }
            startUp = @paramas["browser"].ToString();
            browserDriver = @paramas["browserdriverfolder"].ToString();
            /**browserDriver = Directory.GetParent(Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).ToString()) + "\\Resources\\SeleniumDrivers";
           browserDriver = "..\\..\\..\\Resources\\SeleniumDrivers";
           property = "webdriver." + startUp.ToLower() + ".driver"; */
        }

        public WebSeleniumUIDriver()
        {
        }


    }
}
