﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using src.cbf.engine;
using src.cbf.model;
using src.cbf.reporting;
using src.cbf.harness;
using src.cbf.utils;

namespace src.cbfx.uidriver
{
    public class BaseWebModuleDriver : BaseModuleDriver
    {
        readonly LogUtils logger;
        public override void Glow()
        {
            logger.HandleError("TEmporary Solution");
        }
        public WebSeleniumUIDriver seleniumUIDriver;
        public TableHandler selTblHndlr;
        /**
         * Constructor to initialize TestResultLogger
         */
        public BaseWebModuleDriver()
        {
            seleniumUIDriver = BaseWebAppDriver.seleniumUIDriver;
            selTblHndlr = new TableHandler(seleniumUIDriver);
        }
    }
}
