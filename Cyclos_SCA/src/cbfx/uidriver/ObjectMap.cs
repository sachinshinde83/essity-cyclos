﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using src.cbf.utils;
using src.cbf.harness;
using System.Text.RegularExpressions;
using System.IO;
using System.Globalization;

namespace src.cbfx.uidriver
{
    public class ObjectMap
    {
        public List<Dictionary<String, dynamic>> uiMap;

        public ObjectMap()
        {

            /** TODO Auto-generated constructor stub */
            logger = new LogUtils(this);
            uiMap = ReadLocators();
        }

        /**
         * Reads locators and returns List of it
         * 
         * @return List of locators
         */
        
	public List<Dictionary<String, dynamic>> ReadLocators()
        {
            List<Dictionary<String, dynamic>> locators = new List<Dictionary<String, dynamic>>();
            try
            {

                String filepath = ResourcePaths.GetInstance().GetSuiteResource(
                        "Plan/AppDriver/OR", "uiMap.xls");
                if (new FileInfo(filepath).Exists)
                {
                    ExcelAccess.AccessLocatorSheet(filepath, new ExcelAccess.RowArrayBuilder(locators));
                }
                else
                {
                    ExcelAccess.AccessLocatorSheet(ResourcePaths.GetInstance()
                            .GetSuiteResource("Plan/AppDriver/OR", "uiMap.xlsx"), new ExcelAccess.RowArrayBuilder(locators));
                }
            }
            catch (ObjectDisposedException e)
            {
                logger.HandleError("Error while reading the locators", e);
                logger.HandleError("Error" + e.Message);
            }

            return locators;

        }

        /**
         * Returns the Locator for the given ElementName
         * 
         * @param sElementName
         *            name of element
         * @return Locator String
         */
        public String GetLocator(String sElementName)
        {
            Dictionary<String, dynamic> result = null;
            String sLocator = null;             
            try
            {
                result = GetElementMap(sElementName);
                sLocator = ActualLocator(result)+"$"+GetLocatorType(result);              
               
            }
            catch (ObjectDisposedException e)
            {
                logger.Trace("Error: Trying to retrieve the Locator of Unknown Element \" " + sElementName + " \"", e);
                logger.HandleError("Error" + e.Message);
            }
            return sLocator;
        }

        /**
         * Creates a sub-set of Mapping Element List only for Specified elements
         * 
         * @param columnMaps
         *            from Mapping file
         * @param selectedElements
         *            of selected elementsgetu
         * @return sub-set of Mapping Elements
         */
        public List<Dictionary<String, dynamic>> GetSelectedMappingElements(List<Dictionary<String, dynamic>> columnMaps, List<String> selectedElements)
        {
            if(selectedElements==null || columnMaps==null)
            {
                throw new ArgumentNullException();
            }
            List<Dictionary<String, dynamic>> reDefinedMaps = new List<Dictionary<String, dynamic>>();
            try
            {
                foreach (String element in selectedElements)
                {
                    for (int i = 0; i < columnMaps.Count; i++)
                    {
                        Dictionary<String, dynamic> temp = columnMaps[i];
                        if ((((String)temp["page"]) + "." + ((String)temp["elementName"])).Contains(element))
                        {
                            reDefinedMaps.Add(temp);
                        }
                    }
                }
            }
            catch (ObjectDisposedException e)
            {
                logger.Trace(
                        "Error : Caused while creating sub-set of Mapping Elements" + ".Exception is " + e.Message,
                        columnMaps, selectedElements);
                logger.HandleError("Error" + e.Message);
            }
            return reDefinedMaps;
        }

        /**
         * Gets the list of locators that belongs to the specified page
         * 
         * @param sPageName
         *            name of Pagee
         * @return List of page locators
         */
        public List<Dictionary<String, dynamic>> GetPageLocators(String sPageName)
        {
            if(sPageName==null)
            {
                throw new ArgumentNullException("sPageName");
            }

            Dictionary<String, dynamic> result;
            List<Dictionary<String, dynamic>> resultMap = new List<Dictionary<String, dynamic>>();

            String sElement = "";
            CompareInfo ci = CultureInfo.CurrentCulture.CompareInfo;
            try
            {
                foreach (Dictionary<String, dynamic  > map in uiMap)
                {
                    sElement = (String)map["elementName"];
                    if (sElement.Contains("."))
                    {
                        String temp = sElement.Substring(0, ci.IndexOf(sElement,".", CompareOptions.None));
                        if (sPageName.Contains(temp))
                        {
                            result = map;
                            resultMap.Add(result);
                        }
                    }
                }
            }
            catch (ObjectDisposedException e)
            {
                logger.Trace("Error: Caused while searching the locators of page \" " + sPageName + " \"", e);
                logger.HandleError("Error" + e.Message);
            }
            return resultMap;
        }

        /**
         * Retrieves the Element Map of the specified UI element
         * 
         * @param sElementName
         *            name of Element
         */
        public Dictionary<String, dynamic> GetElementMap(String sElementName)
        {
            Dictionary<String, dynamic> locatorMap = null;
            finalUiMap = GetUiMap(sElementName);
           
            try
            {
           

            foreach (Dictionary<String, dynamic> map in finalUiMap)
                {
                    String sTemp =(String)map["elementName"];
                    if (sElementName.Split('.')[1].Equals(sTemp))
                    {
                        locatorMap = map;                        
                    }
                }
                if (locatorMap == null)
                {
                    logger.Trace("Error : Unknown Element \"" + sElementName + "\"");

                }
            }
            catch (ObjectDisposedException e)
            {
                logger.Trace("Error : Caused while trying to retrieve the map for Unknown Element \""
                        + sElementName + "\"" + ".Exception is " + e.Message);
                logger.HandleError("Error" + e.Message);
            }
            return locatorMap;
        }


        public List<Dictionary<String, dynamic>> GetUiMap(String ElementName) 
        {
            if(ElementName==null)
            {
                throw new ArgumentNullException("ElementName");
            }

            String pName = ElementName.Contains(".") ? ElementName.Split('.')[0] : ElementName;
               
            List<Dictionary<String, dynamic>> finaleUiMap = new List<Dictionary<string, dynamic>>();
            foreach (Dictionary<String, dynamic> tempMap in uiMap)
            {
                if (pName.Equals(tempMap["page"]))
                {
                    finaleUiMap.Add(tempMap);
                }

            }
            return finaleUiMap;
    }

       public String ActualLocator(Dictionary<String, dynamic> result)
        {
            if(result==null)
            {
                throw new ArgumentNullException("result");
            }
            foreach (KeyValuePair<String, dynamic> loc in result)
            {
                if (!loc.Key.Equals("page") && !loc.Key.Equals("elementName")&&loc.Value != null && loc.Value != "")
                {                

                        return loc.Value;
                    
                }
            }

            return "";


        }

        public String GetLocatorType(Dictionary<String, dynamic> result)
        {
            if (result == null)
            {
                throw new ArgumentNullException("result");
            }
            foreach (KeyValuePair<String, dynamic> loc in result)
            {
                if (!loc.Key.Equals("page") && !loc.Key.Equals("elementName")&&loc.Value != null && loc.Value != "")
                {                  

                        return loc.Key;
                   
                }
            }

            return "";


        }
        
        /**
         * Gets the dynamic locators for the specified page
         * 
         * @param sPageName
         *            Name whose elements has to be filtered
         * @return List of UI Element map
         */
        public List<Dictionary<String, dynamic>> GetDynamicLocators(String sPageName)
        {
            if (sPageName == null)
            {
                throw new ArgumentNullException("sPageName");
            }

            List<Dictionary<String, dynamic>> resultMap = new List<Dictionary<String, dynamic>>();
            Dictionary<String, dynamic> result;
            try
            {
                foreach (Dictionary<String, dynamic> map in uiMap)
                {
                    String temp = (String)map["page"];
                    if (sPageName.Contains(temp)
                            && ((String)map["elementName"]).StartsWith("_"))
                    {
                        result = map;
                        resultMap.Add(result);
                    }
                }
            }
            catch (ObjectDisposedException e)
            {
                logger.Trace("Error: Caused while searching the dynamic locators of page \" "
                        + sPageName + " \"" + ".Exception is " + e.Message);
                logger.HandleError("Error"+e.Message);
            }
            return resultMap;
        }

        /**
         * Overriding toString() method and returns ObjectMap format string
         */
        public String TooString()
        {
            return StringUtils.MapString(this, uiMap);
        }

        
        List<Dictionary<String, dynamic>> finalUiMap;
        readonly LogUtils logger;

    }
}
