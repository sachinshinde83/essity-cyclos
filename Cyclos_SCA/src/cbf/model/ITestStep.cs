﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace src.cbf.model
{
/**
 * 
 * Defines the structure of the TestStep
 * 
 */
public interface ITestStep {
	/**
	 * Returns TestStep name
	 * 
	 * @return name of TestStep
	 */
	String StepName();

	/**
	 * Returns moduleCode of TestStep
	 * 
	 * @return TestStep moduleCode
	 */
	String ModuleCode();

	/**
	 * Returns componentCode of TestStep
	 * 
	 * @return TestStep componentCode
	 */
	String ComponentCode();

	/**
	 * Returns component parameters in form of DataRow object
	 * 
	 * @return object of DataRow
	 */
    Dictionary<String, dynamic> ComponentParameters();

	/**
	 * Returns value of OutputValidation field
	 * 
	 * @return OutputValidation value
	 */
	String ComponentOutputValidation();

	/**
	 * Returns value of failTestIfUnexpected field
	 * 
	 * @return failTestIfUnexpected value
	 */
	bool FailTestIfUnexpected();

	/**
	 * Returns value of abortTestIfUnexpected field
	 * 
	 * @return abortTestIfUnexpected value
	 */
	bool AbortTestIfUnexpected();
    /**change */
    String StepId();
}
}
