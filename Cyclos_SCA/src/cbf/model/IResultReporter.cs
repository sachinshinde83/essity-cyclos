﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using src.cbf.engine;

namespace src.cbf.model
{
    

/**
 * 
 * Extends the reporter with open and close methods Opens report Logs execution
 * events with details Closes the report
 * 
 */
    public interface IResultReporter : TestResultTracker.Observer 
    {
	/**
	 * Opens the report file and updates the headers as required
	 * 
	 * @param headers
	 *            run details to be updated in report
	 * 
	 */
    void Open(Dictionary<String, object> headers);

	/**
	 * Closes the report file
	 */
	void Close();
}
}
