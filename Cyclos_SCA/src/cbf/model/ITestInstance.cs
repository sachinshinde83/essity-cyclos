﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace src.cbf.model
{
    public interface ITestInstance
    {
        /**
	 * Deserializes test file, resolves parameters and returns TestCase object.
	 * 
	 * @return TestCase object in an engine acceptable format
	 */
	ITestCase TestCase();

	/**
	 * Returns iterations specified for the testcase
	 * 
	 * @return iterations
	 */
	ITestIteration[] Iterations();

	/**
	 * Returns description of TestInstance
	 * 
	 * @return TestInstance's description
	 */
	String Description();

	/**
	 * Returns name of TestInstance
	 * 
	 * @return TestInstance's name
	 */
	String InstanceName();
	
	/**
	 * Returns path of folder
	 * 
	 * @return TestInstance's folder path
	 */
	String FolderPath();

    }
}
