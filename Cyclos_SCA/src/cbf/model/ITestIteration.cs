﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Collections.Generic;


namespace src.cbf.model

{
public interface ITestIteration {

	/**
	 * Returns number of steps present in the current iteration
	 * 
	 * @return number of steps
	 */
	int StepCount();

	/**
	 * Returns step with the given index from step array
	 * 
	 * @param stepIx
	 *            index of step
	 * @return TestStep object with step and component details
	 */
	ITestStep Step(int stepIx);

	/**
	 * Returns iter parameters read from the iteration sheet
	 * 
	 * @return Map of parameters 
	 */
    Dictionary<String, dynamic> Parameters();
}
}
