﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace src.cbf.model
{
/**
 * 
 * Initializes report logger and  
 * contains a method recover for implementing
 * recovery steps in case of abrupt interruption in execution.
 */
public interface IAppDriver {

	/**
	 * Initializes TestResultLogger
	 * 
	 * @param resultLogger
	 *            TestResultLogger object with methods like passed, failed, error etc available for reporting runtime results
	 */
	void Initialize();

	/**
	 * Invokes specific function related to provided component code for execution
	 * 
	 * @param moduleCode
	 *            module code for the input component
	 * @param componentCode
	 *            code for the component under execution
	 * @param input
	 *            DataRow of input parameters
	 * @param output
	 *            empty DataRow passed to capture any runtime output during execution of component
	 */
    void Perform(String moduleCode, String componentCode, Dictionary<string, dynamic> input, Dictionary<string, dynamic> output);

	/**
	 * Recovers from abrupt execution interruption
	 */
	void Recover();

    }
}
