﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using src.cbf.engine;
using src.cbf.utils;


namespace src.cbf.engine
{
    public class TestResultTracker : TestResult
    {
        /**
         * 
         * Logs execution events with details
         * 
         */
         /** TestResult.ResultType resultobj = new TestResult.ResultType(); */
        private readonly LogUtils logger;
        TestResult leafResult;

        public new String ToString()
        {
            return StringUtils.MapString(this, reporters);
        }
        /**
         * Constructor to initialize Reporter's list
         * 
         * @param oReporter
         *            List of Reporters
         */
        public TestResultTracker(List<Observer> oReporter)
        {
            reporters = oReporter;
            logger = new LogUtils(this);
        }
        public TestResultTracker()
        {
        }

        private readonly List<Observer> reporters = new List<Observer>();

        public interface Observer
        {
            /**
             * Starts Reporter
             * 
             * @param result
             *            actual e
             */
            void Start(TestResult result);


            /**
             * Logs details in report
             * 
             * @param result
             *            execution result
             * @param rsType
             *            result type of the current executed entity
             * @param details
             *            result details of the current executed entity
             */
            void Log(TestResult result, TestResult.ResultType rsType, Object details);

            /**
             * Finishes the Reporter
             * 
             * @param result
             *            execution result
             * @param rsType
             *            result type of the current executed entity
             * @param details
             *            result details of the current executed entity
             */
            void Finish(TestResult result, TestResult.ResultType rsType, Object details);
        };


        /**
         * Adds input reporter to selected report list
         * 
         * @param reporter
         *            object of Reporter like excel, html etc
         */
        public void AddReporter(Observer reporter)
        {
            try
            {
                reporters.Add(reporter);
            }
            catch (NullReferenceException nre)
            {
                logger.Trace(nre.Message);
                logger.HandleError("Can not add reporter ", reporter, nre);
            }
            catch (Exception e)
            {
                logger.Trace(e.Message);
                logger.HandleError("Can not add reporter ", reporter, e); 
                throw;
            }
        }

        /**
         * 
         * Tracks logger
         * 
         */
        public interface Trackable
        {
             /** results have to be added/set to this result object */

            /**
             * Tracks execution results taking TestResult object
             * 
             * @param result
             *            object of testResult
             */

           
            void RunIteration(TestResult result);
            void RunIteration(TestResult result,ParameterManager manager);
            void RunTestStep(TestResult result);
            void Runcomponent(TestResult result);
            void RunTestCase(TestResult result);

        };

        /**
         * Tracks logger and returns TestResult object
         * 
         * @param trackable
         *            object of Trackable
         * @param entityType
         *            type of entity
         * @param entityName
         *            name of entity
         * @param entity
         *            object having entity details
         * @return result
         */

      

        public TestResult Track(Trackable trackable, TestResult.EntityType entityType, String entityName, Object entity)
        
        {
            TestResult result = Start(entityType, entityName, entity);

            logger.Detail("START : " + Thread.CurrentThread.Name);
            if (entityType == TestResult.EntityType.Iteration && trackable!=null)
            {
                try
                {
                    trackable.RunIteration(result);
                }
                catch (NullReferenceException nre)
                {
                    logger.Trace("NullReferenceException");
                    result.Add(TestResult.ResultType.ERROR, new Dictionary<Object, Object> { { "exception", nre } });
                }
                catch (ObjectDisposedException e)
                {
                    logger.Trace("ObjectDisposedException Exception");
                    result.Add(TestResult.ResultType.ERROR, new Dictionary<Object, Object> { { "exception", e } });
                }
            }
            else if (entityType == TestResult.EntityType.TestStep && trackable != null)
            {
                try
                {
                    trackable.RunTestStep(result);
                }
                catch (NullReferenceException nre)
                {
                    logger.Trace("NullReferenceException");
                    result.Add(TestResult.ResultType.ERROR, new Dictionary<Object, Object> { { "exception", nre } });
                }
                catch (ObjectDisposedException e)
                {
                    logger.Trace("ObjectDisposedException Exception");
                    result.Add(TestResult.ResultType.ERROR, new Dictionary<Object, Object> { { "exception", e } });
                }
            }
            else if (entityType == TestResult.EntityType.TestCase && trackable != null)
            {
                try
                {
                    trackable.RunTestCase(result);
                }
                catch (NullReferenceException nre)
                {
                    logger.Trace("NullReferenceException");
                    result.Add(TestResult.ResultType.ERROR, new Dictionary<Object, Object> { { "exception", nre } });
                }
                catch (ObjectDisposedException e)
                {
                    logger.Trace("ObjectDisposedException Exception");
                    result.Add(TestResult.ResultType.ERROR, new Dictionary<Object, Object> { { "exception", e } });
                }
            }
            else if (entityType == TestResult.EntityType.Component && trackable != null)
            {
                try
                {
                    trackable.Runcomponent(result);
                }
                catch (NullReferenceException nre)
                {
                    logger.Trace("NullReferenceException");
                    result.Add(TestResult.ResultType.ERROR, new Dictionary<Object, Object> { { "exception", nre } });
                }
                catch (ObjectDisposedException e)
                {
                    logger.Trace("ObjectDisposedException Exception");
                    result.Add(TestResult.ResultType.ERROR, new Dictionary<Object, Object> { { "exception", e } });
                }
            }
            else
            {
                logger.Trace("Else empty");
            }
            logger.Detail("FINISH : " + Thread.CurrentThread.Name + " STATUS : " + result.finalRsType);

            Finish();

            return result;
        }
        
        




        public TestResult Track(Trackable trackable, TestResult.EntityType entityType, String entityName, Object entity,ParameterManager manager)
        {
            TestResult result = Start(entityType, entityName, entity, manager);

            logger.Detail("START : " + Thread.CurrentThread.Name);
            if (entityType == TestResult.EntityType.Iteration && trackable != null)
            {
                try
                {
                    trackable.RunIteration(result, manager);
                }
                catch (NullReferenceException nre)
                {
                    logger.Trace("NullReferenceException");
                    result.Add(TestResult.ResultType.ERROR, new Dictionary<Object, Object> { { "exception", nre } });
                }
                catch (ObjectDisposedException e)
                {
                    logger.Trace("ObjectDisposedException Exception");
                    result.Add(TestResult.ResultType.ERROR, new Dictionary<Object, Object> { { "exception", e } });
                }
            }
            else if (entityType == TestResult.EntityType.TestStep && trackable != null)
            {
                try
                {
                    trackable.RunTestStep(result);
                }
                catch (NullReferenceException nre)
                {
                    logger.Trace("NullReferenceException");
                    result.Add(TestResult.ResultType.ERROR, new Dictionary<Object, Object> { { "exception", nre } });
                }
                catch (ObjectDisposedException e)
                {
                    logger.Trace("ObjectDisposedException Exception");
                    result.Add(TestResult.ResultType.ERROR, new Dictionary<Object, Object> { { "exception", e } });
                }
            }
            else if (entityType == TestResult.EntityType.TestCase && trackable != null)
            {
                try
                {
                    trackable.RunTestCase(result);
                }
                catch (NullReferenceException nre)
                {
                    logger.Trace("NullReferenceException");
                    result.Add(TestResult.ResultType.ERROR, new Dictionary<Object, Object> { { "exception", nre } });
                }
                catch (ObjectDisposedException e)
                {
                    logger.Trace("ObjectDisposedException Exception");
                    result.Add(TestResult.ResultType.ERROR, new Dictionary<Object, Object> { { "exception", e } });
                }
            }
            else if (entityType == TestResult.EntityType.Component && trackable != null)
            {
                try
                {
                    trackable.Runcomponent(result);
                }
                catch (NullReferenceException nre)
                {
                    logger.Trace("NullReferenceException");
                    result.Add(TestResult.ResultType.ERROR, new Dictionary<Object, Object> { { "exception", nre } });
                }
                catch (ObjectDisposedException e)
                {
                    logger.Trace("ObjectDisposedException Exception");
                    result.Add(TestResult.ResultType.ERROR, new Dictionary<Object, Object> { { "exception", e } });
                }
            }
            else
            {
                logger.Trace("Else empty");
            }
            logger.Detail("FINISH : " + Thread.CurrentThread.Name + " STATUS : " + result.finalRsType);

            Finish();

            return result;
        }
         /** TestResult leafResult = null; */
        /**
         * Logs ResultType and TestResult details
         * 
         * @param rsType
         *            result type of the current executed entity
         * @param details
         *            Object containing TestResult details
         */
        public void Log(TestResult.ResultType rsType, Object details)
        {
            if (leafResult.entityType == TestResult.EntityType.TestCase)
            {
                leafResult.entityDetails = details;
                return;  /** consume this event */
            }
            Dictionary<dynamic, dynamic> d = (Dictionary<dynamic, dynamic>)details;
            leafResult.Add(rsType, d);
            logger.Trace("Report Event: type:" + rsType.ToString() + ":details:"
                    + ToString(details).Replace(",", ";"));
            foreach (Observer reporter in reporters)
            {
                reporter.Log(leafResult, rsType, d);
            }
        }



        /************* Functions ********************/
        /** change 20-09-2016 */

        
        private TestResult Start(TestResult.EntityType entityType, String entityName, Object entityDetails)
        {
            TestResult result = new TestResult(leafResult, entityType, entityName, entityDetails);
            result.startTime = DateTime.Now;
            leafResult = result;
            foreach (Observer reporter in reporters)
            {
                reporter.Start(result);
            }
            return result;
        }
        
        private TestResult Start(TestResult.EntityType entityType, String entityName, Object entityDetails,ParameterManager manager)
        {
            TestResult result = new TestResult(leafResult, entityType, entityName, entityDetails, manager);
            result.startTime = DateTime.Now;
            leafResult = result;
            foreach (Observer reporter in reporters)
            {
                reporter.Start(result);
            }
            return result;
        }

        private void Finish()
        {
            TestResult result = leafResult;
            if (result == null)
            {
                logger.HandleError("No result when details called ");
            }
            result.finishTime = DateTime.Now;
            leafResult = result.parent;
            if (leafResult != null)
            {
                leafResult.Add(result.msRsType, result);
            }
            foreach (Observer reporter in reporters)
            {
                reporter.Finish(result, result.msRsType, result.finalRsType);
            }

        }

        private static String ToString(Object o)
        {
            return StringUtils.ToString(o);
        }

         /** private static Dictionary<Object, Object> toMap(Object[] o)
        {
            return Utils.toMap(o);
        } */


    }
}
