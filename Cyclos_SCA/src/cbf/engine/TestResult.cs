﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using src.cbf.utils;

namespace src.cbf.engine
{
    public class TestResult
    {


        public TestResult parent;
        public int index;
        public int value;
        public int childCount;
        public Dictionary<ResultType, Int32> rsTypeCounts = new Dictionary<ResultType, Int32>();
        public ResultType msRsType = ResultType.DONE;
        public Object msDetails;
        public ResultType finalRsType = ResultType.DONE;
        public DateTime startTime, finishTime;

        public Dictionary<String, dynamic> miscInfo = new Dictionary<String, dynamic>();

        public ParameterManager paramManager;
        public EntityType entityType;
        public String entityName;
        public Object entityDetails;

        private readonly LogUtils logger;

        public TestResult()
        {
            logger = new LogUtils(this);
        }

        public enum EntityType
        {
            RUN, TESTSUITE, TestCase, Iteration, TestStep, Component, SubStep
        }





        public enum ResultType
        {
            PASSED = 1, FAILED = 3, WARNING = 2, ERROR = 4, DONE = 0
        }




        /**
   * Constructor to initialize TestResult, EntityType and entityDetails
   * objects
   * 
   * @param parent
   *            parent object for the current entity
   * @param entityType
   *            entityType of the current entity like TESTCASE, TESTSTEP etc
   * @param entityName
   *            name of entity
   * @param entityDetails
   *            entity details object
   */
       /**  Change */
        public TestResult(TestResult parent, EntityType entityType, String entityName, Object entityDetails)
        {
            this.entityType = entityType;
            this.entityName = entityName;
            this.entityDetails = entityDetails;

            this.parent = parent;
            this.index = (parent == null ? -1 : this.parent.childCount++);
        }

        public TestResult(TestResult parent, EntityType entityType, String entityName, Object entityDetails,ParameterManager manager)
        {
            this.entityType = entityType;
            this.entityName = entityName;
            this.entityDetails = entityDetails;
            this.paramManager = manager;
            this.parent = parent;
            this.index = (parent == null ? -1 : this.parent.childCount++);
        }
        /**
      * Updates TestResult object for msRsType, msDetails etc with the current
      * execution check details
      * 
      * @param rsType
      *            result type of the current check like PASSED, ERROR etc
      * @param checkDetails
      *            check details while execution of component
      */
       
        /**
      * Updates TestResult object for msRsType, msDetails etc with the current
      * execution child results
      * 
      * @param rsType
      *            result type of the current child node under execution like
      *            PASSED, ERROR etc
      * @param childResult
      *            current child object
      */
        /**public void Add(ResultType rsType, TestResult childResult)
        //{
        //    Add(rsType, (Object)childResult);
        //} */

        public void Add(ResultType rsType, Object details)
        {
            int rsTypeCount = 0;
          /**
             * boolean rsT=rsTypeCounts.isEmpty(); if(rsT==false){
             * System.out.println("In if->"+rsTypeCounts);
             * System.out.println(rsType); rsTypeCount = rsTypeCounts.get(rsType); }
             */
            try
            {
                rsTypeCount = rsTypeCounts.ContainsKey(rsType) ? rsTypeCounts[rsType] : 0;

            }
            catch (IndexOutOfRangeException ior)
            {
                logger.Trace(ior.Message);
            }

            if (rsTypeCounts.ContainsKey(rsType))
            {
                rsTypeCounts[rsType] = rsTypeCount + 1;
            }
            else
            {
                rsTypeCounts.Add(rsType, rsTypeCount + 1);
            }

            value = rsTypeCounts[rsType];
            if (msRsType.Getlevel() <= rsType.Getlevel())
            {
                msRsType = rsType;
                msDetails = details;

            }
            finalRsType = rsType;
        }

        public TestResult ParentResult(EntityType type)
        {
            for (TestResult rs = this; rs != null; rs = rs.parent)
            {
                if (rs.entityType == type)
                {
                    return rs;
                }
            }
            return null;
        }

        /**
      * Overriding toString() method to return TestResult string
      */
        public String ToString()
        {
            return StringUtils.MapString(this);
        }


    }

   
    static class ResultTypeExtensions
    {


        public static String name;

        /**
         * Returns ResultType name
         */


        public static int level;

               /**
         * Returns true/false depending on TestCase passed or failed
         * 
         * @return true/false
         * 
         */


        public static Boolean IsPassed(this TestResult.ResultType msRsType)
        {
            /** PASSED = 1, FAILED = 3, WARNING = 2, ERROR = 4, DONE = 0 */

            if (msRsType == TestResult.ResultType.DONE)
            {
                level = 0;
            }
            else if (msRsType == TestResult.ResultType.PASSED)
            {
                level = 1;
            }
            else if (msRsType == TestResult.ResultType.WARNING)
            {
                level = 2;
            }
            else if (msRsType == TestResult.ResultType.FAILED)
            {
                level = 3;
            }
            else if (msRsType == TestResult.ResultType.ERROR)
            {
                level = 4;
            }
            else
            {
                level = 5;
            }

            return level <= (int)TestResult.ResultType.WARNING;

        }

        public static int Getlevel(this TestResult.ResultType msRsType)
        {
            return (int)msRsType;
        }

    }

}
