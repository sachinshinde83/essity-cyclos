﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using src.cbf.utils;
using src.cbf.harness;
using System.Text.RegularExpressions;
using src.cbf.model;
namespace src.cbf.engine
    
{
    public class ParameterManager
        
    {
        private readonly LogUtils logger;
        private readonly Dictionary<dynamic, dynamic> entities = new Dictionary<dynamic, dynamic>();
        private readonly Dictionary<String, Object> nestedParams = new Dictionary<String, Object>();
        /** private String refv2="";
        private List<dynamic>=new List<dynamic
       Object dop=Activator.CreateInstance(typeof(cbf.utils.Substitutor.Substitution));  
        
           cbf.utils.Substitutor.Substitution */
        static Dictionary<String, TestResult> results = new Dictionary<String, TestResult>();
        public ParameterManager()
        {
          
            logger = new LogUtils(this);
        }
        
      /** public String get(String refer)
         {
         if(nestedParams.ContainsKey("CONFIG");
             
           return getFromResults(refer,"UNIQUE");
            
         }*/
     
     
        public void Start()
        {
            logger.Trace("Testresult start");
        }
        public void Log()
        {
            logger.Trace("Logger");
        }
        public void Finish()
        {
            logger.Trace("Testresult finish");
        }

        public Dictionary<String, dynamic> Resolve(Dictionary<String, dynamic> input)
        {
            Dictionary<String, dynamic> resolved = new Dictionary<String, dynamic>();
            if (input != null)
            {
                foreach (String key in input.Keys)
                {
                    Object value=null;
                    try
                    {
                        value = new Substitutor(nestedParams).Substitute(input[key]);
                    }
                    catch (AllParamsException e)
                    {
                        /**value = new ParameterManager().MakeParameterAccess(GetTestResult(e.stepID));*/
                        logger.Trace(e);
                    }
                    resolved.Add(key, value);
                }
            }
            return resolved;
        }

        public TestResult GetTestResult(String stepId)
        {

            return results[stepId];
        }
        public void PutTestResult(TestResult result)
        {

            if (result.entityType == src.cbf.engine.TestResult.EntityType.TestStep || result.entityType == src.cbf.engine.TestResult.EntityType.SubStep)
            {
                results.Add((String)result.miscInfo["StepId"], result);
            }

        }
        public Object GetEntity(src.cbf.engine.TestResult.EntityType enType)
        {
            return entities[enType];
        }
             
       public void Initialise()
        {
            nestedParams.Add(ConstantVar.CONFIG, Harness.GCONFIG.FetchAllProp());
            nestedParams.Add(ConstantVar.UNIQUE, new Substitute());
            nestedParams.Add(ConstantVar.INPUT, new Substitute());
            nestedParams.Add(ConstantVar.OUTPUT, new Substitute());
        }
   

       private Dictionary<String, String> Parse(String refer) 
       {
           Regex reg = new Regex("[A-Z0-9]\\.?\\d*#?\\w+(\\s?\\w+)+", RegexOptions.IgnoreCase);
           Match matcher = reg.Match(refer);
           List<String> matches = new List<String>();
           while (matcher.Success)
           {
               matches.Add(matcher.Value);
               matcher = matcher.NextMatch();
           }
         String paramcode;
         try
         {
             /** paramcode = "" + Regex.Split(matches[matches.Count - 2], "#")[1]; */
             paramcode = Regex.Split(matches[matches.Count - 1], "#")[1];
         }
         catch (ArgumentException)
         {
             logger.Trace("Specific Exception");
             paramcode = matches[(matches.Count - 1)];
         }

		String stepId = "";
        StringBuilder sb = new StringBuilder();
        StringBuilder sbnew = new StringBuilder();

		for (int ix = matches.Count-1; ix >1 ; ix--) 
        {
			if (stepId != "")
            {
                sb.Append(stepId).Append(".");			
			}
            stepId = sb.ToString();        
            sbnew.Append(stepId).Append(Regex.Split(matches[matches.Count - 2], "#")[0]);
            stepId = sbnew.ToString();
		}


		Dictionary<String, String> parseRes = new Dictionary<String, String>();
		parseRes.Add(ConstantVar.STEPID, stepId);
		parseRes.Add(ConstantVar.PARAMCODE, paramcode);
		return parseRes;

	}


        private String GetFromResults(String reference, String type) 
        {
            Dictionary<String, String> details = Parse(reference);
		    if (details["ParamCode"].Contains("All")) 
            {
			    throw new AllParamsException(details["StepId"]);
		    }
		    TestResult result = GetTestResult(details["StepId"]);
            Dictionary<String,dynamic> values = result.miscInfo[type];
            return values[details["ParamCode"]];
		   /** DataRow values = (DataRow) result.miscInfo[type];
            return (String)values.getObject(details["ParamCode"]);*/
            
	    }


        public class AllParamsException :Exception 
        {
		    public AllParamsException(String stepId) 
            {
			    stepID = stepId;
		    }
		public  String stepID;
	    }

        public class ParamAccess : ParameterAccess
        {
            private readonly LogUtils logger;
            private readonly TestResult result;
            public ParamAccess(TestResult result)
            {
                logger = new LogUtils(this);
                this.result = result;
            }


            public Boolean IsPassed()
            {

                return result.finalRsType.IsPassed();  /** FIXME: */
            }

            public ParamAccess Parameters(ITestStep step)
            {
                TestResult rs = null;
                if (step != null)
                {
                    rs = new ParameterManager().GetTestResult(step.StepName());
                }
                 /**if (rs == null)
                    return null; */
                return new ParamAccess(rs);
            }
            public Dictionary<String,dynamic> IterationParameters()
            {
                TestResult rs = result.ParentResult(src.cbf.engine.TestResult.EntityType.Iteration);
                if (string.Equals(rs,null))
                {
                    logger.HandleError("Bug: cannot find iteration object");
                }

                return ((ITestIteration)rs).Parameters(); /** <-- CHECKME */
            }

            public ITestStep Parent()
            {
                TestResult p = result.ParentResult(src.cbf.engine.TestResult.EntityType.TestStep);
                 /**if (p == null)
                    return null; */
                return (ITestStep)p;
            }

            public ITestIteration Iteration()
            {
                TestResult rs = result.ParentResult(src.cbf.engine.TestResult.EntityType.Iteration);
                if (string.Equals(rs, null))
                {
                    logger.HandleError("Bug: cannot find iteration object");
                }
                return (ITestIteration)rs;
            }
            public ITestCase Testcase()
            {
                TestResult rs = result.ParentResult(src.cbf.engine.TestResult.EntityType.TestCase);
                if (string.Equals(rs, null))
                {
                    logger.HandleError("Bug: cannot find testcase object");
                }
                return (ITestCase)rs;
            }

            public Boolean IsPassing()
            {
                TestResult rs = result.ParentResult(src.cbf.engine.TestResult.EntityType.Iteration);
                return rs.finalRsType.IsPassed();  /** so far, passing */
            }

            public Object Configuration(String key)
            {
                return Harness.GCONFIG.Get(key);
            }

            public int Count()
            {
                return result.childCount;
            }

            public Dictionary<String,dynamic> Input(int ix) 
            {
			    return ((TestResult) result.miscInfo[""+ix]).miscInfo["input"];
		    }

            public Dictionary<String, dynamic> Output(int ix) 
            {
			    return  ((TestResult) result.miscInfo[""+ix]).miscInfo["output"];
		    }

            public Dictionary<String, dynamic> Input() 
            {
			    return  result.miscInfo["input"];
		    }


            public Dictionary<String, dynamic> Output()
            {
			    return  result.miscInfo["output"];
		    }
        }
        public ParamAccess MakeParameterAccess(TestResult result)
        {
            return new ParamAccess(result);
        }

        public class Substitute:Substitution
        {
            ParameterManager manager;
            public Substitute() { }
            public Substitute(ParameterManager manager)
            {
                this.manager = manager;
            }
            public String Get(String refv)

            {
                /** if(refv.Contains("OUTPUT")) */
                manager = new ParameterManager();
                    return manager.GetFromResults(refv, "OUTPUT");
                /** if(refv.Contains("INPUT"))
                    return manager.getFromResults(refv, "INPUT");
                return "";*/
            }
            public String GetUniqUtil(String refv)
            {
                 /** dynamic obj = Activator.CreateInstance(typeof(UniqueUtils)); */
                dynamic obj = new UniqueUtils();
                return obj.uniqueString(refv);
            }
        }
    }
    
   
}
