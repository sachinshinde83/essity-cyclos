﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using src.cbf.model;
using System.Data;
using System.Reflection;

namespace src.cbf.engine
{
    abstract public class BaseModuleDriver : IModuleDriver
    {
        /** private LogUtils logger = new LogUtils(this); */
        public static String test;
        public abstract void Glow();
        /**
         * Performs mapping with DataRow
         * 
         * @param componentCode
         *            takes component code value
         * @param input
         *            DataRow of input parameters
         * @param output
         *            empty DataRow passed to capture any runtime output during
         *            execution of component
         */
        public void Perform(String componentCode, Dictionary<string, dynamic> input, Dictionary<string, dynamic> output)
        {
            Eval(componentCode, input, output);
        }

        private void Eval(String componentCode, Dictionary<string, dynamic> input, Dictionary<string, dynamic> output)
        {
            MethodInfo m = FindMethod(componentCode);
            InvokeMethod(m,input, output);

        }

        private MethodInfo FindMethod(String componentCode)
        {
            Type t = this.GetType();
            MethodInfo finalMethodInfo = null;
            
            foreach (MethodInfo m in t.GetMethods())
            {
                if (m.Name.Equals(componentCode,StringComparison.OrdinalIgnoreCase))
                {
                    ParameterInfo[] pType = m.GetParameters();
                    if (pType.Length == 2)
                    {
                                    
                        finalMethodInfo = m;
                        return finalMethodInfo;
                    }
                }
            }
            return finalMethodInfo;
        }

        private void InvokeMethod(MethodInfo m, Dictionary<string, dynamic> input, Dictionary<string, dynamic> output)
        {
            object classInstance = Activator.CreateInstance(this.GetType(), null);
            object[] parametersArray = new object[] { input, output };
             /** m.Invoke(m, (new object[] { input,output})); */
            m.Invoke(classInstance, parametersArray);
        }
    }
}
