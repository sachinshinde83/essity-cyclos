﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using src.cbf.harness;
using src.cbf.utils;

namespace src.cbf.engine
{






/**
 * 
 * Implements built-in components
 * 
 */
class BuiltinComponentsDriver : BaseModuleDriver 
{
    private readonly ParameterAccess paramsAccess;
    readonly LogUtils logger;
        public override void Glow()
        {
            logger.HandleError("TEmporary Solution");
        }
        /**
         * Constructor to initialize ParameterAccess object
         * 
         * @param paramsAccess
         *            object of ParameterAccess
         */
        public BuiltinComponentsDriver() {
        paramsAccess = new ParameterAccess();
    }
    public BuiltinComponentsDriver(ParameterAccess paramsAccess)
    {
        logger = new LogUtils(this);
        this.paramsAccess = paramsAccess;
    }

    /**
     * 
     * Saves output parameters for further use
     * 
     */
    public void Save(Dictionary<String, dynamic> input)
    {
        if (!input.ContainsKey("inputName"))
        {
            input.Add("inputName", null);
        }
        if (!input.ContainsKey("outputName"))
        {
            input.Add("outputName", null);
        }
        paramsAccess.CopyRecentParameters(input["inputName"],input["outputName"]);
    }
    
    /**
     * 
     * Provides sleep time slabs
     * 
     */
    public void Sleep(Dictionary<String,dynamic> input)
    {
        String level = input["level"];
        if (level.Equals(""))
        {
            logger.HandleError("Parameter level must be specified", level);
        }
       /**  SleepUtils.TimeSlab sleepLevel; */
        Object sleepLevel=null;
        switch (level.ElementAt(0))
        {
            case 'Y':
                sleepLevel =src.cbf.utils.SleepUtils.TimeSlab.YIELD;
                break;
            case 'L':
                sleepLevel = src.cbf.utils.SleepUtils.TimeSlab.LOW;
                break;
            case 'M':
                sleepLevel = src.cbf.utils.SleepUtils.TimeSlab.MEDIUM;
                break;
            case 'H':
                sleepLevel = src.cbf.utils.SleepUtils.TimeSlab.HIGH;
                break;
            default:
               logger.HandleError("Invalid level : ", sleepLevel);
               break;

        }
       
        SleepUtils.Sleep((SleepUtils.TimeSlab)sleepLevel);
         
    }

    public void Fail(Dictionary<String, dynamic> input, Dictionary<String, dynamic> output)
    {
        logger.HandleError("Failed with parameters ", input, output);
    }

    /**
     * Returns BuiltinComponentsDriver format string
     */
    public String ToString()
    {
        return StringUtils.MapString(this, paramsAccess);
    }

}

}
