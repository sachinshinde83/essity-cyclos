﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

/**
 * Executes TestCase i.e, steps and returns TestResult object to tracker for reporting
 * 
 */
using src.cbf.model;
using src.cbf.harness;
using src.cbf.utils;
using System;
using System.Dynamic;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using src.cbf.engine;
using Microsoft.CSharp.RuntimeBinder;
 /** using src.cbf.testAccess; */

namespace src.cbf.engine
{
    public class TestCaseRunner : TestResultTracker
    {
        readonly TestResultTracker oResultsTracker;
        public IAppDriver oAppDriver;
        private static ParameterAccess paramsAccess;
        private static LogUtils logger;        
        private readonly ParameterManager parameterManager;
        private static BuiltinComponentsDriver biComponentsDriver;
       /**  public static Configuration GCONFIG; */
        /**
         * Constructor to initialize AppDriver, TestResultTracker and
         * TestResultLogger objects
         * 
         * @param oAppDriver
         *            AppDriver object necessary for execution
         * @param oResultTracker
         *            TestResultTracker object for tracking execution results
         */

        
        public TestCaseRunner(IAppDriver oAppDriver,
                TestResultTracker oResultTracker)
        {
            logger = new LogUtils(this);
            this.oAppDriver = oAppDriver;
            this.oResultsTracker = oResultTracker;
        }
        public TestCaseRunner(IAppDriver oAppDriver,
             TestResultTracker oResultTracker, ParameterManager parameterManager)
        {
            logger = new LogUtils(this);
            this.oAppDriver = oAppDriver;
            this.oResultsTracker = oResultTracker;
            this.parameterManager = parameterManager;
        }
       

         /** public TestCaseRunner()
        {

        } */

        public static TestCaseRunner TTRunner;

        public static TestResult SetRunDetails(IAppDriver oAppDriver, TestResultTracker oResultTracker, 
            ITcMaker tcMaker, string TCName, ParameterManager paramterManger)
        {

            TTRunner = new TestCaseRunner(oAppDriver, oResultTracker, paramterManger);
            return TTRunner.RunTestCase(tcMaker, TCName,paramterManger);
        }

         /** change 20-09-2016 */
          /**public static TestResult setRunDetails(AppDriver oAppDriver, TestResultTracker oResultTracker, TestCaseRunner.TCMaker tcMaker, string TCName)
        {

            TTRunner = new TestCaseRunner(oAppDriver, oResultTracker);
            return TTRunner.runTestCase(tcMaker, TCName);
        }*/

        /**
         * Deserializes the test file, resolves references and makes executable TestCase object
         */

        /**    
             * Executes TestCase
             * 
             * @param tcMaker
             *            TCMaker object
             * @param name
             *            TestCase name
             * @return call to track function with trackable object, entityType, entityName and entity object
        */  


        public TestResult RunTestCase(ITcMaker tcMaker, String name)
        {
            Track trobj = new Track(tcMaker, name, oResultsTracker);

            return oResultsTracker.Track((Trackable)trobj, TestResult.EntityType.TestCase, name, tcMaker);

        }
        public TestResult RunTestCase(ITcMaker tcMaker, String name,ParameterManager manager)
        {
            Track trobj = new Track(tcMaker, name, oResultsTracker, manager);
            return oResultsTracker.Track((Trackable)trobj, TestResult.EntityType.TestCase, name, tcMaker,manager);
             /** Change
            return oResultsTracker.track((Trackable)trobj, TestResult.EntityType.TestCase, name, tcMaker); */

        }

        
        public static TestCaseRunner TCrunner;
         /**
        * Run test case for the given iteration
        */
        public TestResult RunIteration(ITestIteration iteration,ParameterManager manager)
        {
            /**change Track trobj = new Track(iteration) */
            Track trobj = new Track(iteration, manager);
             /**dynamic trobj =new ExpandoObject (); */
             /** trobj.Trackable(); */
             /** new Track(iteration); */
             /** Changereturn oResultsTracker.track((Trackable)trobj, TestResult.EntityType.Iteration, "#IterIx#")*/
            return oResultsTracker.Track((Trackable)trobj, TestResult.EntityType.Iteration, "#IterIx#", iteration,manager);

        }
       
        public TestResult RunIteration(ITestIteration iteration)
        {
             /**change Track trobj = new Track(iteration) */
            Track trobj = new Track(iteration);
             /**dynamic trobj =new ExpandoObject ();
            trobj.Trackable();
            new Track(iteration);
            Changereturn oResultsTracker.track((Trackable)trobj, TestResult.EntityType.Iteration, "#IterIx#") */
            return oResultsTracker.Track((Trackable)trobj, TestResult.EntityType.Iteration, "#IterIx#", iteration);

        }

        /**
        * Executes TestStep
        * 
        * @param oTestStep
        *            TestStep object
        * @return call to track function with trackable object, entityType, entityName and entity object
        */

     /**    public TestResult runStep(TestStep oTestStep,ParameterManager paramManager)
        {
            //Track trobj = new Track(oTestStep);
            Dictionary<string, dynamic> input = oTestStep.componentParameters();
            //Dictionary<string, string> output = null;
            //TestResult componentRs = runComponent(oTestStep, input, output);
            Track trobj = new Track(oTestStep);
            return oResultsTracker.track((Trackable)trobj, TestResult.EntityType.TestStep, oTestStep.stepName(), oTestStep);
        }*/

 /** Change */

        public TestResult RunStep(ITestStep oTestStep,ParameterManager manager)
        {
             /** Track trobj = new Track(oTestStep); */
             /** Dictionary<string, string> output = null; */
              /** TestResult componentRs = runComponent(oTestStep, input, output); */
            Track trobj = new Track(oTestStep, manager);
            return oResultsTracker.Track((Trackable)trobj, TestResult.EntityType.TestStep, oTestStep.StepName(), oTestStep, manager);
        }

       
        public TestResult RunStep(ITestStep oTestStep)
        {
             /** Track trobj = new Track(oTestStep); */
              /** Dictionary<string, string> output = null;
              /** TestResult componentRs = runComponent(oTestStep, input, output); */
            Track trobj = new Track(oTestStep);
            return oResultsTracker.Track((Trackable)trobj, TestResult.EntityType.TestStep, oTestStep.StepName(), oTestStep);
        }


        public TestResult RunComponent(ITestStep oTestStep, Dictionary<string, dynamic> input, Dictionary<string, dynamic> output)
        {
            String moduleCode = null;
            String componentCode = null;
              /** TestResultTracker oResultsTracker; */
            if (oTestStep != null)
            {
                moduleCode = oTestStep.ModuleCode();
                componentCode = oTestStep.ComponentCode();
            }
            Track trobj = new Track(oTestStep, input, output, moduleCode, componentCode);
           
            return oResultsTracker.Track((Trackable)trobj, (TestResult.EntityType)TestResult.EntityType.Component, moduleCode + "-" + componentCode, oTestStep);
              /** performComponent(moduleCode, componentCode, input, output); */
        }






        /**
         * Executes component functionality
         * 
         * @param oTestStep
         *            TestStep with step details and component details like module code, component code, parameters etc
         * @param input
         *            DataRow of input parameters
         * @param output
         *            empty DataRow passed to capture any runtime output during execution of component
         * @return call to track function with trackable object, entityType, entityName and entity object
         */

        private void PerformComponent(String moduleCode, String componentCode, Dictionary<string, dynamic> input, Dictionary<string, dynamic> output)
        {
              /** TODO: Support for built-in _FW module */
            logger.Trace("performComponent()");
            logger.Trace("appdriver.perform( " + moduleCode + "_" + componentCode + "_" + StringUtils.ToString(input).Replace(",", ";") + " ) ");
            if (moduleCode.Equals("_FW"))
            {
                biComponentsDriver.Perform(componentCode, input, output);
            }
            else
            {
                oAppDriver.Perform(moduleCode, componentCode, input, output);
            }


        }

        /**
         * Returns TestCaseRunner format string
         */
        public new String ToString()
        {
            return StringUtils.MapString(this, oAppDriver, oResultsTracker);
        }



        public class Track : Trackable
        {
            readonly TestResultTracker oResultsTracker;
              /** TestCaseRunner TCrunner; */
            readonly IAppDriver oAppDriver;
              /** TCMaker tcmkr; */
            private readonly ParameterManager parameterManager;
            
            public Track(IAppDriver oAppDriver, TestResultTracker oResultsTracker)
            {
                this.oResultsTracker = oResultsTracker;
                this.oAppDriver = oAppDriver;

            }

            public Track(IAppDriver oAppDriver, TestResultTracker oResultsTracker, ParameterManager parameterManager)
            {
                this.oResultsTracker = oResultsTracker;
                this.oAppDriver = oAppDriver;
                this.parameterManager = parameterManager;

            }

              /** change */

            public Track(ITestStep oTestStep, ParameterManager parameterManager)
            {
                this.oTestStep = oTestStep;
                this.parameterManager = parameterManager;

            }

             public Track(ITestStep oTestStep)
            {
                this.oTestStep = oTestStep;

            }

           
            public Track(ITcMaker tcMaker, String name, TestResultTracker oResultsTracker)
            {
                this.oResultsTracker = oResultsTracker;

                this.tcMaker = tcMaker;
                this.name = name;
            }


            public Track(ITcMaker tcMaker, String name, TestResultTracker oResultsTracker, ParameterManager parameterManager)
            {
                this.oResultsTracker = oResultsTracker;
                this.parameterManager = parameterManager;
                this.tcMaker = tcMaker;
                this.name = name;
            }

              /** change  */

            public Track(ITestIteration iteration, ParameterManager parameterManager)
            {
                this.iteration = iteration;
                this.parameterManager = parameterManager;

            }
           
            public Track(ITestIteration iteration)
            {
                this.iteration = iteration;

            }

            public Track(ITestStep oTestStep, Dictionary<string, dynamic> input, Dictionary<string, dynamic> output, String moduleCode, String componentCode)
            {
                this.moduleCode = moduleCode;
                this.componentCode = componentCode;
                this.oTestStep = oTestStep;
                this.input = input;
                this.output = output;
            }

            public void RunTestCase(TestResult result)
            {
                ITestCase oTestCase = null;
                try
                {
                    oTestCase = tcMaker.Make();
                }
                catch (ObjectDisposedException ode)
                {
                    logger.Trace(ode.Message);
                }
                catch (Exception nre)
                {
                    logger.Trace(nre.Message);
                    throw;                    
                }
              

                  /** fire an artificial log event with the deserialized testCase */
                oResultsTracker.Log(TestResult.ResultType.DONE, oTestCase);

                  /** fire an artificial log event with the deserialized testCase */
                oResultsTracker.Log(ResultType.DONE, oTestCase);

                paramsAccess = new ParameterAccess();
                biComponentsDriver = new BuiltinComponentsDriver(paramsAccess);
                paramsAccess.DeclareVariables(oTestCase);
                oResultsTracker.AddReporter((Observer)paramsAccess);

                int iterationCount = oTestCase.IterationCount();
                for (int i = 0; i < iterationCount; i++)
                {
                    /** Change TestResult iterRs = TestCaseRunner.TTRunner.runIteration(oTestCase.iteration(i)); */
                    TestResult iterRs = TestCaseRunner.TTRunner.RunIteration(oTestCase.Iteration(i), parameterManager);
                    if (iterRs.miscInfo.ContainsKey("abortRs"))
                    {
                        try
                        {
                            logger.Trace("componentDriver.recover()");
                            /** oAppDriver.recover(); */
                        }
                        catch (ObjectDisposedException ode)
                        {
                            logger.HandleError("Iteration recovery failed:", oTestCase, ode.Message);
                        }
                     
                    }
                }
            }

             /**Change */

            public void RunIteration(TestResult result,ParameterManager manager)
            {
                int stepCount = iteration.StepCount();
                for (int stepIx = 0; stepIx < stepCount; stepIx++)
                {   /** run each
                     step */

                    ITestStep oTestStepnew = iteration.Step(stepIx);
                    dynamic stepRs = null;
                    if (oTestStepnew.GetType().FullName == "src.cbf.testAccess.ExcelDeserializer+TestStep")
                    {
                        stepRs = TestCaseRunner.TTRunner.RunStep(oTestStepnew, parameterManager);
                    }
                    else
                    {
                        stepRs = RunSubSteps((src.cbf.testAccess.TestStep)oTestStepnew, stepRs, parameterManager);
                    }

                    if (stepRs.miscInfo.ContainsKey("abortRs"))
                    {
                          /** result.miscInfo.Add("abortRs", (String)stepRs); */
                        if (result != null)
                        {
                            result.miscInfo.Add("abortRs", stepRs);
                        }
                        logger.Trace("aborting at:" + stepIx);                        
                    }
                }
            }
              /**change 20-09-2016 */
            public void RunIteration(TestResult result)
            {
                int stepCount = iteration.StepCount();
                for (int stepIx = 0; stepIx < stepCount; stepIx++)
                {   /** run each
                     step */

                    ITestStep oTestStepnew = iteration.Step(stepIx);
                    dynamic stepRs = null;
                    if (oTestStepnew.GetType().FullName == "src.cbf.testAccess.ExcelDeserializer+TestStep")
                    {
                        stepRs = TestCaseRunner.TTRunner.RunStep(oTestStepnew);
                    }
                    else
                    {
                        stepRs = RunSubSteps((src.cbf.testAccess.TestStep)oTestStepnew, stepRs);
                    }

                    if (stepRs.miscInfo.ContainsKey("abortRs"))
                    {
                          /** result.miscInfo.Add("abortRs", (String)stepRs); */
                        if (result != null)
                        {
                            result.miscInfo.Add("abortRs", stepRs);
                        }
                        logger.Trace("aborting at:" + stepIx);                        
                    }
                }
            }
              /** Change */

            public TestResult RunSubSteps(src.cbf.testAccess.TestStep oTestStepnew, TestResult subStepRes,ParameterManager paramManager)
            {
                List<src.cbf.testAccess.TestStep> testStep = new List<testAccess.TestStep>();
                if (oTestStepnew != null)
                {
                    /** TestResult subStepRes = new TestResult(); */
                    testStep = oTestStepnew.SubSteps();
                }
                if (testStep == null)
                {
                    subStepRes = TestCaseRunner.TTRunner.RunStep(oTestStepnew, paramManager);
                    return subStepRes;
                }


                foreach (src.cbf.testAccess.TestStep subStep in testStep)
                {

                    subStepRes = RunSubSteps(subStep, subStepRes, paramManager);


                    if (subStepRes.miscInfo.ContainsKey("abortRs"))
                    {
                        break;
                    }
                }

                  /** return stepRes; */
                return subStepRes;
            }


            
            public TestResult RunSubSteps(src.cbf.testAccess.TestStep oTestStepnew, TestResult subStepRes)
            {
                List<src.cbf.testAccess.TestStep> testStep = new List<testAccess.TestStep>();
                  /** TestResult subStepRes = new TestResult(); */
                if (oTestStepnew != null)
                {
                    testStep = oTestStepnew.SubSteps();
                }
                if (testStep == null)
                {
                    subStepRes = TestCaseRunner.TTRunner.RunStep(oTestStepnew);
                    return subStepRes;
                }


                foreach (src.cbf.testAccess.TestStep subStep in testStep)
                {

                    subStepRes = RunSubSteps(subStep, subStepRes);


                    if (subStepRes.miscInfo.ContainsKey("abortRs"))
                    {
                        break;
                    }
                }

                  /** return stepRes; */
                return subStepRes;
            }


            public void RunTestStep(TestResult result)
            {
                 /** Configuration GCONFIG; */
                  /**change */
                Dictionary<String, dynamic> outputnew = new Dictionary<String, dynamic>();
                ITestStep tStep = null;
                if (result != null)
                {
                    tStep = (ITestStep)result.entityDetails;
                }
                /** tStep.stepId();
                tStep.componentParameters();*/
                Dictionary<String, dynamic> inputnew = oTestStep.ComponentParameters();
              

                Dictionary<String, Object> testCaseAccessMap = (Dictionary<String, Object>)Harness.GCONFIG.Get("TestCaseAccess");
                String reporterSelection = (String)testCaseAccessMap["plugin"];


                  /** Checking if data is to be accessed from designer */
                if (paramsAccess != null && reporterSelection != "DesignerTestCaseAccess")
                {   /** CHECKE: (DataRow) casting
                    inputpAccess = Utils.Map2DataRow(paramsAccess.resolve(input)); */
                    inputnew = Utils.Map2DataRow(paramsAccess.Resolve(inputnew));
                }
                if (parameterManager != null && reporterSelection == "DesignerTestCaseAccess" && result!=null)
                {   /** CHECKE: (DataRow) casting */
                    inputnew = parameterManager.Resolve(inputnew);
                    result.miscInfo.Add("StepId", tStep.StepId());
                    result.miscInfo.Add("INPUT", inputnew);
                    result.miscInfo.Add("OUTPUT", outputnew);
                }
                
                TestResult componentRs = TestCaseRunner.TTRunner.RunComponent(oTestStep, inputnew, outputnew);
                  /** TestResult.ResultType componentRsType = componentRs.finalRsType; */
                
                TestResult.ResultType componentRsType = componentRs.msRsType;
               

                 /**
                 * TODO: add outputValidation code here. Enhancement... if
                 * (componentRsType.isPassed()) { // ' Component was performed
                 * properly 'if did not match then componentRsType =
                 * ResultType.FAILED;
                 */
                

                
                Boolean bAbortTest = false, bFailTest = false;
                if (!componentRsType.IsPassed())
                {
                    bAbortTest = oTestStep.AbortTestIfUnexpected();
                    bFailTest = oTestStep.FailTestIfUnexpected();
                    
                     /** indication: unexpected result was ignored as per flag */
                    
                    if (!bFailTest &&  result!=null)
                    {
                        /** FIXME: This causes even some earlier failure also tobe ignored */

                        result.finalRsType = (TestResult.ResultType)TestResult.ResultType.WARNING;
                        result.msRsType = (TestResult.ResultType)TestResult.ResultType.WARNING;
                        

                    }

                    if (bAbortTest && result!=null)
                    {
                        result.miscInfo.Add("abortRs", componentRs);
                    }
                    
                }

            }

            public void Runcomponent(TestResult result)
            {
                if (result != null)
                {
                    result.miscInfo.Add("input", (dynamic)input);


                    try
                    {
                        TestCaseRunner.TTRunner.PerformComponent(moduleCode, componentCode, input, output);
                        result.miscInfo.Add("output", (dynamic)output);
                    }
                    catch (RuntimeBinderException rbe)
                    {
                        /** error("", "", "Error during perform:"+ StringUtils.toString(e)); */
                        TestResultLogger.Failed(componentCode + "failed", "Error during perform:" + componentCode,
                            "Error during perform:" + StringUtils.ToString(componentCode) + ". Exception:" + rbe.Message);
                        logger.HandleError("Error during perform:" + componentCode + ". Exception:" + rbe.Message);
                    }
                    catch (Exception e)
                    {
                        /** error("", "", "Error during perform:"+ StringUtils.toString(e)); */
                        TestResultLogger.Failed(componentCode + "failed", "Error during perform:" + componentCode,
                            "Error during perform:" + StringUtils.ToString(componentCode) + ". Exception:" + e.Message);
                        logger.HandleError("Error during perform:" + componentCode + ". Exception:" + e.Message);
                        throw;
                    }

                }

            }

             /** private TestResult componentRs; */
            private readonly String moduleCode;
            private readonly String componentCode;
            private readonly String name;
            private readonly ITcMaker tcMaker;
            private readonly ITestIteration iteration;
            private readonly ITestStep oTestStep;
            private readonly Dictionary<string, dynamic> input;
            private readonly Dictionary<string, dynamic> output;
        }
    }


}
