﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using src.cbf.model;
using System.Data;
using src.cbf.utils;
using System.Diagnostics;

namespace src.cbf.engine
{

abstract public  class BaseAppDriver:IAppDriver
    
    {
        public void Initialize()
        {
            logger = new LogUtils(this);
            moduleDrivers = LoadModuleDrivers();
        }

        /**
         * Invokes function mapped with component code for
         * execution
         * 
         * @param moduleCode
         *            module code for the input component
         * @param componentCode
         *            code for the component under execution
         * @param input
         *            DataRow of input parameters
         * @param output
         *            empty DataRow passed to capture any runtime output during
         *            execution of component
         */
        public void Perform(String moduleCode, String componentCode, Dictionary<string, dynamic> input,
                Dictionary<string, dynamic> output)
        {
            
            IModuleDriver driver = moduleDrivers[moduleCode];
            if (driver == null)
            {
                logger.HandleError("Cannot find driver for module", moduleCode);
                return;
            }
            driver.Perform(componentCode, input, output);
        }

        /**
         * Recovers from abrupt execution interruption
         */
        public void Recover()
        {
            try
            {
                Utils.KillProcess("EXCEL");
            }
            catch (InvalidOperationException ioe)
            {
                logger.Error("BaseAppDriver-recover:" + ioe.Message);
            }
            catch (Exception e)
            {
                logger.Error("BaseAppDriver-recover:" + e.Message);
                throw;
            }
		
        }
    
        abstract public Dictionary<String, IModuleDriver> LoadModuleDrivers();
        private Dictionary<String, IModuleDriver> moduleDrivers;
        protected LogUtils logger;
    }
}
