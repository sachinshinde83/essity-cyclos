﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using src.cbf.engine;
using src.cbf.utils;
using src.cbf.harness;


namespace src.cbf.engine
{
    public class TestResultLogger
    {
        public String ToString()
        {
            return StringUtils.MapString(this, tracker);
        }

        private static TestResultTracker tracker;
        private static TestResultLogger RESULT;
        private TestResultLogger(TestResultTracker tracker)
        {
            TestResultLogger.tracker = tracker;
        }

        /**
         * Method to create a singleton object of TestResultLogger class
         * 
         * @param tracker
         *            object of TestResultTracker
         */

        public static TestResultLogger GetInstance(TestResultTracker tracker)
        {
            if (((String)((Dictionary<String, Object>)Harness.GCONFIG.Get("Runner"))["plugin"]).Equals("TestRunner"))
            {
                RESULT = null;
            }
            if (RESULT == null)
            {
                RESULT = new TestResultLogger(tracker);
            }
            return RESULT;
        }

        /**
         * Logs TestStep result as passed in report
         * 
         * @param name
         *            name of TestStep
         * @param expected
         *            expected result of TestStep
         * @param actual
         *            actual result of TestStep
         */

        public static void Passed(String name, String expected, String actual)
        {
            Log(name, (TestResult.ResultType)TestResult.ResultType.PASSED, expected, actual, true);
        }

        /**
         * Logs TestStep result as failed in report
         * 
         * @param name
         *            name of TestStep
         * @param expected
         *            expected result of TestStep
         * @param actual
         *            actual result of TestStep
         */
        public static void Failed(String name, String expected, String actual)
        {
            LogUtils.TCFail = true; //temp
            Log(name, (TestResult.ResultType)TestResult.ResultType.FAILED, expected, actual, true);
        }

        /**
         * Logs TestStep result as error in report
         * 
         * @param name
         *            name of TestStep
         * @param expected
         *            expected result of TestStep
         * @param actual
         *            actual result of TestStep
         */
        public static void Error(String name, String expected, String actual)
        {
            Log(name, (TestResult.ResultType)TestResult.ResultType.ERROR, expected, actual, true);
        }

        /**
         * Logs TestStep result as done in report
         * 
         * @param name
         *            name of TestStep
         * @param expected
         *            expected result of TestStep
         * @param actual
         *            actual result of TestStep
         */
        public static void Done(String name, String expected, String actual)
        {
            Log(name, (TestResult.ResultType)TestResult.ResultType.DONE, expected, actual, true);
        }

        /**
         * Logs TestStep result as warning in report
         * 
         * @param name
         *            name of TestStep
         * @param expected
         *            expected result of TestStep
         * @param actual
         *            actual result of TestStep
         */
        public static void Warning(String name, String expected, String actual)
        {
            Log(name, (TestResult.ResultType)TestResult.ResultType.WARNING, expected, actual, true);
        }

        /**
         * Logs the execution results in report as per the inputs
         * 
         * @param name
         *            name of TestStep
         * @param rsType
         *            ResultType of TestStep
         * @param expected
         *            expected result of TestStep
         * @param actual
         *            actual result of TestStep
         */
        public static void Log(String name, TestResult.ResultType rsType, String expected, String actual, Boolean screenDump)
        {

             /** tracker.log(rsType, Utils.toMap(new Object[] { "name", name, "expected", expected, "actual", actual, "screenDump", screenDump })); */
            tracker.Log(rsType, new Dictionary<Object,Object> {{ "name", name},{ "expected", expected}, {"actual", actual}, {"screenDump", screenDump }});            
        }

        /**
         * Returns TestResultLogger format string
         */

    }
}
