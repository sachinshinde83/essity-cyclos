﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */
using src.cbf.model;
using src.cbf.utils;
using src.cbf.harness;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using src.cbf.reporting;
using src.cbf.testAccess;


namespace src.cbf.engine
{
    public class Engine
    {
        readonly IAppDriver appDriver;
        readonly TestResultTracker resultTracker;
        readonly TestResultLogger result;
         /** LogUtils logger; */
        readonly ParameterManager parameterManager;

         /** Need clarification on Result Reporter object */
        public Engine(IAppDriver appDriver, TestResultTracker resultTracker)
        {
            /** logger = new LogUtils(this); */
            this.appDriver = appDriver;
            this.resultTracker = resultTracker;
            this.result = TestResultLogger.GetInstance(this.resultTracker);

            Dictionary<String, Object> testCaseAccessMap = (Dictionary<String, Object>)Harness.GCONFIG.Get("TestCaseAccess");
            String reporterSelection = (String)testCaseAccessMap["plugin"];
             /** ParameterManager is intialised only when using designer else assigned as null */
            if (reporterSelection == ConstantVar.REPORTSELECTIONS && resultTracker!=null) 
            {
                ParameterManager parameterManagernew = new ParameterManager();
                this.parameterManager = parameterManagernew;
                parameterManagernew.Initialise();
                Observer obsvr = new Observer(parameterManagernew);
                resultTracker.AddReporter(obsvr);
            }
            else
            {
                parameterManager = null;
            
            }
            if (appDriver != null)
            {
                appDriver.Initialize();
            }

        }

        public TestResult RunTestCase(ITcMaker tcMaker, string TCName)
        {
             /** TestCaseRunner.Track trackobj = new TestCaseRunner.Track(appDriver, resultTracker); */
            return TestCaseRunner.SetRunDetails(appDriver, resultTracker, tcMaker, TCName, parameterManager);

        }

         /** public string toString()
        {
            return StringUtils.mapString(this, appDriver, resultTracker, result);
        } 

        Used with parameterManager */
        public class Observer : TestResultTracker.Observer
        {
            readonly ParameterManager paramManager;
            readonly LogUtils logger;
            public Observer(ParameterManager param)
            {
                this.paramManager = param;
                logger = new LogUtils(this);
            }
            public void Start(TestResult result)
            {                
                logger.Trace("Observer-Start:" + StringUtils.ToString(result));
            }

            public void Log(TestResult result, TestResult.ResultType rsType, Object details)
            {
                logger.Trace("Observer-log:" +  StringUtils.ToString(result)+ ":" + StringUtils.ToString(rsType) + ":" + StringUtils.ToString(details));
            }

            public void Finish(TestResult result, src.cbf.engine.TestResult.ResultType rsType, Object details)
            {

                paramManager.PutTestResult(result);
            }
        }
    }
}
