﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;
using System.Windows.Forms;

using src.cbf.model;
using src.cbf.utils;
using src.cbf.harness;


namespace src.cbf.testAccess
{
    public class ExcelTestSetAccess:ITestSet
    {
        private static Dictionary<int, List<String>> testInstanceInfo = new Dictionary<int,List<string>>();

	/**
	 * Constructor which reads the TestSet sheet and makes a list map for
	 * instances as per the user selection
	 * 
	 * @param param
	 */
        public ExcelTestSetAccess(Dictionary<String, dynamic> param)
        {

            logger = new LogUtils(this);

            this.param = param;
            String testCaseName;
            String folderPath;
            List<Dictionary<String, dynamic>> testInstances;
            DTAccess dtAccess = null;

            if (param != null)
            {
                dtAccess = new DTAccess((String)ResourcePaths.GetInstance().GetSuiteResource("Lab", (String)param["testSetFile"]));
                testInstances = dtAccess.ReadSheet((String)param["testSetSheet"]);

                int index = 0;

                for (int ix = 0; ix < testInstances.Count(); ix++)
                {
                    try
                    {
                        if (!Utils.Stringtoobl(((String)testInstances[ix]["SelectYN"])))
                        {
                            continue;
                        }
                        testCaseName = (String)testInstances[ix]["TestCase Name"];
                        folderPath = testInstances[ix].ContainsKey("Folder Path") ? (String)testInstances[ix]["Folder Path"] : string.Empty;

                        List<String> instanceList = new List<String>();
                        instanceList.Add(testCaseName);
                        instanceList.Add(folderPath);
                        testInstanceInfo.Add(index, instanceList);
                        index++;

                    }
                    catch (NullReferenceException nr)
                    {
                        logger.HandleError("No input value is provided for SelectYN column " + nr.Message);
                    }
                    catch (Exception e)
                    {
                        logger.HandleError("No input value is provided for SelectYN column " + e.Message);
                        throw;
                    }
                }
            }
        }

	/**
	 * Returns test instance object specified at the given index in instance
	 * array
	 * 
	 * @param ix
	 *            index of TestInstance
	 * @return testInstance
	 */
	public ITestInstance TestInstance(int ix) 
    {
		
		List<String> paramnew = testInstanceInfo[ix];
        ITestInstance tstInstance = new TstInstance(paramnew);
        return tstInstance;
        
	}


	/**
	 * Returns number of TestInstances
	 * 
	 * @return TestInsances count
	 */
	public int TestInstanceCount()
    {
		return testInstanceInfo.Count();
	}

	/**
	 * Returns ExcelTestSetAccess format string
	 */
	public String ToString() 
    {
		return StringUtils.MapString(this, param);
	}

	private readonly LogUtils logger;
	private readonly Dictionary<String,dynamic> param;

    }

    public class ExcelTestSetAccess1
    {
        private readonly String msg;
        public ExcelTestSetAccess1(String msg1)
        {
            msg = msg1;
        }

        public void Method1()
        {
            MessageBox.Show(msg);
        }
    }

    public class TstInstance : src.cbf.model.ITestInstance
    {
        public List<String> param;
        public TstInstance(List<String> param)
        {
            this.param = param;
        }

        public src.cbf.model.ITestCase TestCase()
        {
            return null;
        }

        public String Description()
        {
            return null;
        }

        public String InstanceName()
        {
            return param[0];
            
        }

        public src.cbf.model.ITestIteration[] Iterations()
        {
            return null;
        }

        public String FolderPath()
        {
            return param[1];
        }
    }
}
