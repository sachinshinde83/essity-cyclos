﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.Text.RegularExpressions;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using src.cbf.model;
using src.cbf.utils;
using System.IO;


namespace src.cbf.testAccess
{
    public class ExcelDeserializer : Utils
    {
        /**
     * Returns TestCase object
     * 
     * @param dtAccess
     *            object of DataAccess
     * @param testName
     *            name of TestCase
     * @param serializedFileName
     *            name of TestCase file
     * @param params
     * @return object of TestCase
     * @throws FileNotFoundException
     */

        readonly IDataAccess dtAccess;
        readonly DTAccess tcDataAccess;
        readonly String testName, sSerializedFileName;
        readonly Dictionary<String, dynamic> param;
        readonly private LogUtils logger;        

        readonly private String SHN_STEPS = "Steps", SHN_REFERENCES = "References", SHN_ITERATIONS = "Iterations", SHN_VARIABLES = "Variables";

        public static src.cbf.model.ITestCase Deserialize(src.cbf.model.IDataAccess dtAccess, String testName, String serializedFileName, Dictionary<String, dynamic> param)
        {
            if (!new FileInfo(serializedFileName).Exists)
            {                
                throw new FileNotFoundException(serializedFileName);
            }
            return new ExcelDeserializer(dtAccess, testName, serializedFileName, param).Deserialize();
        }

        public ExcelDeserializer(IDataAccess dtAccess, String testName, String serializedFileName, Dictionary<String, dynamic> param)
        {
            logger = new LogUtils(this);
            this.dtAccess = dtAccess;
            this.testName = testName;
            this.sSerializedFileName = serializedFileName;
            this.param = param;

            this.tcDataAccess = new DTAccess(serializedFileName);
        }

        private TestCase Deserialize()
        {

            Dictionary<String, dynamic> references;

            if (src.cbf.utils.ExcelAccess.IsSheetExists(sSerializedFileName, SHN_REFERENCES))
            {
                List<Dictionary<String, dynamic>> refRows = tcDataAccess.ReadSheet(SHN_REFERENCES);
                references = MakeReferences(refRows);
            }
            else
            {
                references = null;
            }
            List<Dictionary<String, dynamic>> vars = new List<Dictionary<String, dynamic>>();
            if (src.cbf.utils.ExcelAccess.IsSheetExists(sSerializedFileName, SHN_VARIABLES))
            {
                vars = tcDataAccess.ReadSheet(SHN_VARIABLES);
            }
            if (references != null && vars != null)
            {
                foreach (Dictionary<String, dynamic> variable in vars)
                {
                    if (references.ContainsKey(variable["name"]))
                    {
                        logger.HandleError("Clash in variable and reference names:" + variable["name"]);
                    }
                }
            }

            List<Dictionary<String, dynamic>> variables = vars;
            /** fetch iteration rows */
            List<Dictionary<String, dynamic>> iterRows = null;
            if (src.cbf.utils.ExcelAccess.IsSheetExists(sSerializedFileName, SHN_ITERATIONS))
            {
                iterRows = tcDataAccess.ReadSheet(SHN_ITERATIONS);
                
                int rowSize = iterRows.Count;
                int count = 0;
                foreach (Dictionary<String, dynamic> iterRow in iterRows)
                {
                    Boolean isSelected = true;
                    if (iterRow != null)
                    {
                        isSelected = Convert.ToBoolean(param["enableIterationSelection"]);
                        if (isSelected)
                        {
                            isSelected = iterRow["_selectYN"].Equals("Y");                            
                        }
                        if (!isSelected)
                        {
                            count++;
                        }
                    }
                }

                if (count == rowSize)
                {
                    iterRows = new List<Dictionary<String, dynamic>>();
                    iterRows.Add(new Dictionary<String, dynamic>());
                }

            }
            else
            {
                iterRows = new List<Dictionary<String, dynamic>>();
                iterRows.Add(new Dictionary<String, dynamic>());
            }

            List<Dictionary<String, dynamic>> stepRows = tcDataAccess.ReadSheet(SHN_STEPS);
            String sName = this.testName;
            
            List<TestIteration> iterations = MakeIterations(iterRows, references, stepRows);
            int itrCnt = iterations.Count;
            if (iterations.Count == 0)
            {
                logger.HandleError("No iterations are selected from the " + SHN_ITERATIONS + " sheet");
                return null;
            }

            return new TestCase(itrCnt, iterations, sName, references, variables);
        }
        public class TestCase : src.cbf.model.ITestCase
        {
            public TestCase(int itrCnt, List<TestIteration> iterations, String sName, Dictionary<String, dynamic> masterReferences, List<Dictionary<String, dynamic>> variables)
            {
                this.itrCnt = itrCnt;
                this.iterations = iterations;
                this.sName = sName;
                this.masterReference = masterReferences;
                this.variable = variables;
            }

            public List<Dictionary<String, dynamic>> Variables()
            {
                return variable;
            }

            public String Name()
            {
                return sName;
            }

            public Dictionary<String, dynamic> MasterReferences()
            {
                return masterReference;
            }

            public int IterationCount()
            {
                return itrCnt;
            }

            public src.cbf.model.ITestIteration Iteration(int iterationIx)
            {
                return iterations[iterationIx];
            }

            readonly private dynamic itrCnt;
            readonly private List<TestIteration> iterations;
            readonly private String sName;
            readonly private Dictionary<String, dynamic> masterReference;
            readonly private List<Dictionary<String, dynamic>> variable;

        }

        private List<TestIteration> MakeIterations(List<Dictionary<String, dynamic>> iterRows, Dictionary<String, dynamic> references, List<Dictionary<String, dynamic>> stepRows)
        {
            List<TestIteration> iterations = new List<TestIteration>();
            Boolean isSelected;
            foreach (Dictionary<String, dynamic> iterRow in iterRows)
            {
                isSelected = true;
                
                if (iterRow.Count != 0)
                {
                    isSelected = Convert.ToBoolean(param["enableIterationSelection"]);
                    if (isSelected)
                    {
                        isSelected = iterRow["_selectYN"].Equals("Y") || iterRow["_selectYN"].Equals("");                       
                    }
                }
                if (isSelected)
                {
                    TestIteration iteration = MakeIteration(iterRow, references, stepRows);
                    iterations.Add(iteration);
                }
            }
            return iterations;
        }

        private TestIteration MakeIteration(Dictionary<String, dynamic> iterRow, Dictionary<String, dynamic> references, List<Dictionary<String, dynamic>> stepRows)
        {

            Dictionary<String, dynamic> iterParamsMap = new Dictionary<String, dynamic>();            

            if (iterRow == null)
            {
                iterParamsMap = null;
            }
            else
            {
                iterParamsMap.Add("ITERATION", iterRow);
            }
            List<Dictionary<String, dynamic>> iterStepRows = Substitute(stepRows, iterParamsMap);            
            List<TestStep> steps = new List<TestStep>();

            int stepIx = 0;
            foreach (Dictionary<String, dynamic> stepRow in iterStepRows)
            {
                Boolean isSelected = true;
                if (Convert.ToBoolean(param["enableStepSelection"]))
                {
                    isSelected = stepRow["_selectYN"].Equals("Y") || stepRow["_selectYN"].Equals("");
                }
                List<TestStep> stepsList = null;
                string[] rowSelOff = stepRow["offlineRowId"].Split(',');
                string[] rowSelIn = stepRow["inlineRowId"].Split(',');
                int rowIndex = 0;
                do
                {
                    if (isSelected)
                    {
                        stepRow["offlineRowId"] = rowSelOff.Count() > rowIndex ? rowSelOff[rowIndex] : "";
                        stepRow["inlineRowId"] = rowSelIn.Count() > rowIndex ? rowSelIn[rowIndex] : "";

                        stepsList = MakeTestStep(stepRow, stepIx, references);
                    }
                    if (stepsList == null)
                    {
                        logger.Trace("Step row skipped:" + StringUtils.MapToString(stepRow).Replace(",", ";") + ":#" + stepIx);
                    }
                    else
                    {
                        foreach (TestStep oneStep in stepsList)
                        {
                            steps.Add(oneStep);
                            logger.Trace("Step added:" + StringUtils.MapToString(stepRow).Replace(",", ";") + ":#" + stepIx);
                        }

                    }
                    rowIndex++;
                } while (rowIndex < rowSelIn.Count() || rowIndex < rowSelOff.Count());
            }
            int stpcnt = steps.Count;
            return new TestIteration(stpcnt, steps, iterRow);
        }
        
        public class TestIteration : src.cbf.model.ITestIteration
        {
            public TestIteration(int stpCnt, List<TestStep> steps, Dictionary<String, dynamic> iterRow)
            {
                this.stpCnt = stpCnt;
                this.steps = steps;
                this.iterRow = iterRow;
            }

            public int StepCount()
            {
                return stpCnt;
            }

            public src.cbf.model.ITestStep Step(int stepIx)
            {
                return steps[stepIx];
            }

            public Dictionary<String, dynamic> Parameters()
            {
                return iterRow;
            }

            readonly private List<TestStep> steps;
            readonly private int stpCnt;
            readonly private Dictionary<String, dynamic> iterRow;
        }


        private List<TestStep> MakeTestStep(Dictionary<String, dynamic> stepRow, int iCount, Dictionary<String, dynamic> references)
        {
            String stepName, moduleCode, componentCode;            
            Dictionary<String, dynamic> componentParameters = new Dictionary<String, dynamic>();            
            List<Dictionary<String, dynamic>> tempOfflineRowID = new List<Dictionary<string, dynamic>>();
            List<Dictionary<String, dynamic>> tempInlineRowID = new List<Dictionary<string, dynamic>>();
            List<Dictionary<String, dynamic>> tempInlineParam = new List<Dictionary<string, dynamic>>();            
            List<TestStep> testStepList = new List<TestStep>();
            if (Utils.Stringtoobl((String)param["enableStepSelection"]))
            {

                Boolean isExpected;
                isExpected = stepRow["_selectYN"].Equals("Y") || stepRow["_selectYN"].Equals("");
                                
                if (!isExpected)
                {
                    logger.Trace("Test Step Skipped!!");
                }
                else
                {

                    moduleCode = (String)stepRow["moduleCode"];
                    componentCode = (String)stepRow["componentCode"];
                    if (moduleCode.Equals("") && componentCode.Equals(""))
                    {
                        logger.HandleError("Blank module and component codes in test file;\n step skipped at: "
                                + (iCount + 1));
                        return null;
                    }
                    String name = (String)stepRow["stepName"];
                    if (name == null || name == "")
                    {
                        name = componentCode;
                    }
                    stepName = name;
                    tempOfflineRowID = ResolveOfflineRowId(moduleCode, componentCode, (String)stepRow["offlineRowId"] ?? (String)stepRow["offlineRowId"]);
                    tempInlineRowID = ResolveInlineRowId(moduleCode, componentCode, (String)stepRow["inlineRowId"]??(String)stepRow["inlineRowId"]);

                    if (stepRow.ContainsKey("parameters"))
                    {
                        tempInlineParam = ResolveInlineParamValues((String)stepRow["parameters"] ??(String)stepRow["parameters"], references);

                    }
                    componentParameters = UnionOfMaps(new [] { tempOfflineRowID, tempInlineRowID, tempInlineParam });
                    
                    Boolean bFailTest = false;
                    Boolean bAbortTest;

                    if (stepRow.ContainsKey("failTestIfUnexpected"))
                    {
                        bFailTest = stepRow["failTestIfUnexpected"].ToUpper().Equals("Y") || stepRow["failTestIfUnexpected"].ToUpper().Equals("YES");
                    }
                    else
                    {
                        bFailTest = true;
                    }

                    bAbortTest = stepRow["abortTestIfUnexpected"].ToUpper().Equals("Y") || stepRow["abortTestIfUnexpected"].ToUpper().Equals("YES");
                    

                    TestStep testStep = new TestStep(stepName, moduleCode, componentCode, bAbortTest, null, bFailTest, componentParameters);
                    testStepList.Add(testStep);

                }

            }
            return testStepList;
        }

        public class TestStep : src.cbf.model.ITestStep
        {
            public TestStep(String stepName, String moduleCode, String componentCode, Boolean bAbortTest,
                String componentOutputValidation, Boolean bFailTest, Dictionary<String, dynamic> componentParam)
            {
                this.stepNames = stepName;
                this.moduleCodes = moduleCode;
                this.componentCodes = componentCode;
                this.componentOutputValidations = componentOutputValidation;
                this.bAbortTest = bAbortTest;
                this.bFailTest = bFailTest;
                this.componentParam = componentParam;
            }
            public String StepId()
            {
                return "";
            }
            public Boolean AbortTestIfUnexpected()
            {
                return bAbortTest;
            }

            public String ComponentCode()
            {
                return componentCodes;
            }

            public String ComponentOutputValidation()
            {
                return componentOutputValidations;
            }

            public Dictionary<String, dynamic> ComponentParameters()
            {
                return componentParam;
            }

            public Boolean FailTestIfUnexpected()
            {
                return bFailTest;
            }

            public String ModuleCode()
            {
                return moduleCodes;
            }

            public String StepName()
            {
                return stepNames;
            }
            readonly private String stepNames;
            readonly private String moduleCodes;
            readonly private String componentCodes;
            readonly private Boolean bAbortTest;
            readonly private String componentOutputValidations;
            readonly private Boolean bFailTest;
            readonly private Dictionary<String, dynamic> componentParam;
        }



        private String CheckRows(List<Dictionary<String, dynamic>> rows)
        {
            if (rows == null)
            {
                return "Table not found";
            }
            string strRowfound = rows.Count == 0 ? "No rows found" : string.Empty;

            return strRowfound;

        }

        private Dictionary<String, dynamic> GetModuleDataRow(String moduleCode, String componentCode,
        String rowSelector)
        {
            List<Dictionary<String, dynamic>> rows = dtAccess.SelectRows(moduleCode, componentCode, rowSelector);
            String sMsg = CheckRows(rows);
            if (sMsg != null && !(sMsg.Equals("")))
            {
                logger.HandleError("Failed to get module data row:"
                        + StringUtils.ToString(new [] { moduleCode,
							componentCode, rowSelector }) + ":" + sMsg);
                return null;
            }
            return rows[0];
        }

        private Dictionary<String, dynamic> MakeReferences(List<Dictionary<String, dynamic>> refRows)
        {
            Dictionary<String, dynamic> oRefs = new Dictionary<String, dynamic>();
            for (int ix = 0; ix < refRows.Count; ix++)
            {
                Dictionary<String, dynamic> row = refRows[ix];
                Dictionary<String, dynamic> masterRow = GetModuleDataRow("Master", (String)row["masterTableName"], (String)row["rowId"]);
                dynamic mRow = masterRow;
                oRefs.Add((string)row["name"], mRow);
            }
            return oRefs;
        }

        readonly private Dictionary<String, dynamic> moduleReferences = new Dictionary<String, dynamic>();

        private Dictionary<String, dynamic> MakeModuleReferences(String sModuleCode)
        {

            Dictionary<String, dynamic> references = null;
            if (moduleReferences.ContainsKey(sModuleCode))
            {
                return moduleReferences[sModuleCode];
                /** Need to discuss */
            }
            List<Dictionary<String, dynamic>> refRows = dtAccess.SelectRows(sModuleCode, "References", "");
            if (refRows != null)
            {
                references = MakeReferences(refRows);
            }
            dynamic refernces1 = references;
            moduleReferences.Add(sModuleCode, refernces1);
            return references;
        }

        private List<Dictionary<String, dynamic>> ResolveOfflineRowId(String moduleCode, String componentCode, String rowSelector)
        {
            if (rowSelector.Equals(""))
            {
                return null;
            }
            String[] rowsSel = rowSelector.Split(',');
            List<Dictionary<String, dynamic>> row = new List<Dictionary<String, dynamic>>();
            foreach (String str in rowsSel)
            {
                row.Add(GetModuleDataRow(moduleCode, componentCode, str));
            }

            if (row == null)
            {
                logger.HandleError("Error in resolving offline row reference:"
                        + StringUtils.ArrayToString(new [] { moduleCode,
							componentCode, rowSelector }));
            }
            else
            {
                row = Substitute(row, MakeModuleReferences(moduleCode));
                return row;
            }

            return null;
        }

        private List<Dictionary<String, dynamic>> ResolveInlineRowId(String moduleCode, String componentCode, String rowSel)
        {
            if (rowSel.Equals(""))
            {
                return null;
            }
            List<Dictionary<String, dynamic>> rows = new List<Dictionary<String, dynamic>>();

            String[] rowsSel = rowSel.Split(',');            
            List<string> sheetNames = new List<string> { moduleCode + "-" + componentCode, componentCode };
            foreach (string sheetName in sheetNames)
            {
                if (src.cbf.utils.ExcelAccess.IsSheetExists(sSerializedFileName, sheetName))
                {
                    foreach (String str in rowsSel)
                    {
                        rows.AddRange(tcDataAccess.ReadSelectedRows(sheetName, str));
                    }
                    
                    String sMsg = CheckRows(rows);
                    if (!(sMsg.Equals("")))
                    {
                        logger.HandleError("Error in inline row selection:"
                                + rowSel + ":" + sMsg);
                        
                    }
                }
            }
            if (rows == null)
            {
                logger.HandleError("Could not resolve inline data ref:" + rowSel);
                return null;
            }

            return rows;

        }

        private List<Dictionary<String, dynamic>> Substitute(List<Dictionary<String, dynamic>> stepRow, Dictionary<String, dynamic> substitutions)
        {
            List<Dictionary<String, dynamic>> result = stepRow;

            if (substitutions != null)
            {
                result = new Substitutor(substitutions).Substitute(stepRow);
            }
            return result;
        }

        public Dictionary<String, dynamic> Substitute(Dictionary<String, dynamic> stepRow, Dictionary<String, dynamic> substitutions)
        {
            Dictionary<String, dynamic> result = stepRow;
            if (substitutions != null)
            {
                result = new Substitutor(substitutions).Substitute(stepRow);
            }
            return result;
        }
        
        private List<Dictionary<String, dynamic>> ResolveInlineParamValues(String paramValueString, Dictionary<String, dynamic> references)
        {
            List<Dictionary<String, dynamic>> paramnew = new List<Dictionary<String, dynamic>>();
            Dictionary<String, dynamic> map = new Dictionary<String, dynamic>();
            if (paramValueString.Equals(""))
            {
                return null;
            }

            Regex regexName = new Regex(@"([^=]*)=([^|]*)\|?");
            MatchCollection mc = regexName.Matches(paramValueString);
            
            foreach (Match m in mc)
            {
                map.Add(m.Groups[1].Value.Trim(), m.Groups[2].Value);
            }
            paramnew.Add(map);            
            paramnew = Substitute(paramnew, references);
            return paramnew;
        }

        private Dictionary<String, dynamic> UnionOfMaps(List<Dictionary<String, dynamic>>[] lists)
        {
            Dictionary<String, dynamic> unionList = new Dictionary<String, dynamic>();
            foreach (List<Dictionary<String, dynamic>> lst in lists)
            {
                if (lst != null)
                {
                    List<Dictionary<String, dynamic>> lsst = lst;
                    foreach (Dictionary<String, dynamic> map in lsst)
                    {
                        if (map != null)
                        {
                            var keys = map.Keys.ToArray();
                            foreach (String key in keys)
                            {
                                String value = (String)map[key];
                                
                                if (String.IsNullOrWhiteSpace(value) || String.IsNullOrEmpty(value) || value.Equals("_BLANK"))
                                {
                                    unionList.Add(key, "");
                                }
                                else if (value.Equals("_NA"))
                                {
                                    if (unionList.ContainsKey(key))
                                    {
                                        unionList.Remove(key);
                                    }
                                }
                                else
                                {

                                    if (unionList.ContainsKey(key))
                                    {
                                        unionList[key] = value;
                                    }
                                    else
                                    {
                                        unionList.Add(key, value);
                                    }
                                }
                            }

                        }
                    }
                }
            }
            return unionList;

        }

       
        /**
         * Returns ExcelDeserializer format string
         */
        public String ToString()
        {
            return StringUtils.MapString(this, dtAccess, tcDataAccess, testName,
                    sSerializedFileName, param);
        }

    }

}



