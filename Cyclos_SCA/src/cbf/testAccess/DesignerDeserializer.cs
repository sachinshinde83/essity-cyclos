﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using src.cbf.model;
using src.cbf.utils;

namespace src.cbf.testAccess
{
    public class DesignerDeserializer
    {

        /**
         * Returns TestCase object
         * 
         * @param param
         *            map containing parameters
         * 
         * @param testName
         *            name of TestCase
         * @param folderPath
         *            folder path for test case
         * @return object of TestCase
         * @throws Exception
         */
        public static TestCase Deserialize(Dictionary<String, dynamic> param, String testName, String folderPath)
        {

            return new DesignerDeserializer(param, testName, folderPath).Deserialize();
        }

        readonly Dictionary<String, dynamic> info = new Dictionary<String, dynamic>();

        private DesignerDeserializer(Dictionary<String, dynamic> param, String testName, String folderPath)
        {         
            info.Keys.Union(param.Keys).ToDictionary(k => k, k => param.ContainsKey(k) ? param[k] : info[k]);
            info.Add("testName", testName);
            info.Add("folderPath", folderPath);
        }

        private TestCase Deserialize()
        {
            dynamic jsonObj = new DesignerAccessUtil(info).AccessDesigner();
            return GetTestCase(jsonObj);
        }

        private TestCase GetTestCase(dynamic inputjsonObj)
        {
            dynamic jsonObj = inputjsonObj["ok"];
            String sName = (String)(jsonObj["attributes"]["name"]);

            dynamic itrCnt = jsonObj["iterations"];
            List<TestIteration> iterations = MakeTestIterations(itrCnt);
            return new TestCase(itrCnt, iterations, sName);
        }

        private List<TestIteration> MakeTestIterations(dynamic itrCnt)
        {
            List<TestIteration> iterations = new List<TestIteration>();
            foreach (dynamic item in itrCnt)
            {
                iterations.Add(MakeTestIteration(item));
            }

            return iterations;
        }

        private TestIteration MakeTestIteration(dynamic innerItr)
        {

            dynamic stpCnt = innerItr["steps"];
            List<TestStep> steps = MakeTestSteps(stpCnt);
            return new TestIteration(stpCnt, steps);
        }

        private List<TestStep> MakeTestSteps(dynamic stpCnt)
        {
            List<TestStep> steps = new List<TestStep>();

            foreach (dynamic itr in stpCnt)
            {
                steps.Add(MakeTestStep(itr));
            }

            return steps;
        }

        private TestStep MakeTestStep(dynamic innerStep)
        {
            String moduleCode = (String)innerStep["moduleCode"];
            String componentCode = (String)innerStep["componentCode"];

            if (moduleCode.Equals("") && componentCode.Equals(""))
            {
                /**    logger.handleError("Blank module and component codes in test file;\n step skipped at: "); */
                return null;
            }


            Dictionary<String, dynamic> param = new Dictionary<String, dynamic>();
            Dictionary<String, Object> parameters;
            parameters = innerStep.ContainsKey("expandedParameters")?innerStep["expandedParameters"]:null;
            
            if (parameters != null)
            {

                foreach (KeyValuePair<String, Object> entry in parameters)
                {
                    param.Add(entry.Key, (String)entry.Value);
                }

            }

            if (innerStep.ContainsKey("substeps"))
            {
                innerStep["subSteps"] = SubSteps(innerStep);
            }


            return new TestStep(innerStep, param);

        }

        /**private LogUtils logger = new LogUtils(this);* /


        /**
         * Overloaded toString() method to return DesignerDeserializer format String
         */
        public String ToString()
        {
            return "DesignerDeserializer()";
        }

        public List<TestStep> SubSteps(dynamic innerStep)
        {
            if (innerStep["substeps"] == null)
            {
                return null;
            }
            else
            {
                List<TestStep> subSteps = new List<TestStep>();
                foreach (dynamic itr in innerStep["substeps"])
                {
                    subSteps.Add(MakeTestStep(itr));
                }
                return subSteps;
            }
        }

    }

    public class TestStep : src.cbf.model.ITestStep
    {
        public TestStep(Dictionary<String, dynamic> innerStep, Dictionary<String, dynamic> param)
        {
            this.innerStep = innerStep;
            this.param = param;
        }
        /**change */
        public String StepId()
        {
            return (String)innerStep["masterstepIx"];
        }
        public String StepName()
        {
            return (String)innerStep["stepname"];

        }

        public String ModuleCode()
        {
            return (String)innerStep["moduleCode"];
        }

        public Boolean FailTestIfUnexpected()
        {
            return (Boolean)innerStep["failTestIfUnexpected"];
        }

        public Dictionary<String, dynamic> ComponentParameters()
        {
            return this.param;
        }

        public String ComponentOutputValidation()
        {
            return null;
        }

        public String ComponentCode()
        {
            return (String)innerStep["componentCode"];
        }

        public Boolean AbortTestIfUnexpected()
        {
            return (Boolean)innerStep["abortTestIfUnexpected"];
        }

        public List<TestStep> SubSteps()
        {
            List<TestStep> lstTstStep;
            lstTstStep = innerStep.ContainsKey("subSteps") ? (List<TestStep>)innerStep["subSteps"] : null;

            return lstTstStep;
        }

        private readonly Dictionary<String, Object> innerStep;
        private readonly Dictionary<String, dynamic> param;
    }

    public class TestIteration : src.cbf.model.ITestIteration
    {
        public TestIteration(dynamic stpCnt, List<TestStep> steps)
        {
            this.stpCnt = stpCnt;
            this.steps = steps;
        }

        public int StepCount()
        {
            return stpCnt.Length;
        }

        public src.cbf.model.ITestStep Step(int stepIx)
        {
            return steps[stepIx];
        }

        public Dictionary<String, dynamic> Parameters()
        {
            return null;
        }

        private readonly List<TestStep> steps;
        private readonly dynamic stpCnt;

    }

    public class TestCase : src.cbf.model.ITestCase
    {
        public TestCase(dynamic itrCnt, List<TestIteration> iterations, String sName)
        {
            this.itrCnt = itrCnt;
            this.iterations = iterations;
            this.sName = sName;
        }

        public List<Dictionary<String, dynamic>> Variables()
        {
            return null;
        }

        public String Name()
        {
            return sName;
        }

        public Dictionary<String, dynamic> MasterReferences()
        {
            return null;
        }

        public int IterationCount()
        {
            return itrCnt.Length;
        }

        public src.cbf.model.ITestIteration Iteration(int iterationIx)
        {
            return iterations[iterationIx];
        }

        private readonly dynamic itrCnt;
        private readonly List<TestIteration> iterations;
        private readonly String sName;

    }

}
