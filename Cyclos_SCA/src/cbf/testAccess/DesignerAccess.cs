﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using src.cbf.model;
using src.cbf.utils;

namespace src.cbf.testAccess
{
    public class DesignerAccess:ITestCaseAccess
    {
        
	/**
	 * Constructor to initialize CONFIG parameter
	 * 
	 * @param param
	 *            map containing parameters
	 */
	public DesignerAccess(Dictionary<String, dynamic> param)
    {
		this.param = param;
	}

	/**
	 * Deserializes test file, resolves references and returns TestCase object
	 * 
	 * @param info
	 *            Map having info related to TestCase like test name
	 * @return object of TestCase
	 * 
	 */
    public src.cbf.model.ITestCase GetTestCase(Dictionary<String, dynamic> info)
    {
        String testName = string.Empty;
        String folderPath = string.Empty;

        if (info != null)
        {
            testName = (String)info["instanceName"];
            folderPath = (String)info["folderPath"];
        }
        /**String folderPath = ""; */
		TestCase oTestCase;
        /**try */
        /**{ */
			oTestCase = DeserializeTest(testName, folderPath);
        /**}  */
        /**catch (FrameworkException e)  */
        /**{ */
        /**    logger.handleError("Error in deserializing test step file:" + folderPath); */
        /**} */

		return oTestCase;
	}

    private readonly Dictionary<String,dynamic> param;

	private TestCase DeserializeTest(String testName, String folderPath)
    {      
		return (TestCase)DesignerDeserializer.Deserialize(param, testName, folderPath);
	}

	/**
	 * Returns DesignerAccess format string
	 */
	public String ToString() 
    {
		return StringUtils.MapString(this, param);
	}

    }
}
