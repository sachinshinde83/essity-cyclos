﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using src.cbf.model;
using System.Threading.Tasks;
using System.IO;
using System.Xml;
using src.cbf.testAccess;
using src.cbf.plugin;
using src.cbf.utils;
using src.cbf.harness;



/**
 * 
 * Implements TestCaseAccess interface and makes TestCase from test file
 * 
 */
namespace src.cbf.testAccess
{
    public class ExcelAccess : ITestCaseAccess
    {

        /**
         * Constructor for initializing resource paths and data table access
         * 
         * @param params
         *            map containing parameters
         */
        readonly private LogUtils logger;
        public ExcelAccess(Dictionary<String, dynamic> param)
        {
            logger = new LogUtils(this);
            this.param = param;
        }


        /**
         * Deserializes test file, resolves references and returns TestCase object
         * 
         * @param info
         *            Map having info related to TestCase like test name
         * @return object of TestCase
         */
        public src.cbf.model.ITestCase GetTestCase(Dictionary<String, dynamic> info)
        {
            logger.Trace("getTestCase()");
            String testName=null;
            if (info != null)
            {
                testName = (String)info["instanceName"];
            }
            
            String testFile = "";
            if (testFile.Equals(""))
            {
                testFile = ResourcePaths.GetInstance().GetSuiteResource(
                        "Plan/TestCases", testName + ".xls");
                if ((testFile == null) || !(new FileInfo(testFile).Exists))
                {
                    logger.Trace("Test Case file " + testFile + " does not exists. ",
                            testName, testFile);
                    testFile = null;
                }
            }
            if (testFile == null)
            {
                testFile = ResourcePaths.GetInstance().GetSuiteResource(
                        "Plan/TestCases", testName + ".xlsx");
                if ((testFile == null) || !(new FileInfo(testFile).Exists))
                {
                    logger.Trace("Test Case file " + testFile + " does not exists. ",
                            testName, testFile);
                    testFile = null;
                }
            }
            if (testFile == null)
            {
                logger.HandleError("Deserializing test step file:" + testFile + ": can not find it");
            }
            src.cbf.model.ITestCase oTestCase = null;
            try
            {
                oTestCase = DeserializeTest(testName, testFile);
                
            }
            catch (Exception e)
            {
                logger.HandleError("Deserializing test step file:", testFile, e);
                throw;
            }
            return oTestCase;
        }

        private src.cbf.model.ITestCase DeserializeTest(String testName, String serializedFileName)
        {

            logger.Trace("DeserializeTest(" + testName + ";" + serializedFileName
                    + ")");
            try
            {
                if (testName == "")
                {
                    logger.HandleError("Testcase name is not provided in testset file ", testName, serializedFileName);
                    return null;
                }
                else
                {
                   return ExcelDeserializer.Deserialize(GetDataAccess(), testName, serializedFileName, param);
                }
               
            }

            catch (FileNotFoundException e)
            {
                logger.HandleError("File doesn't exists ", serializedFileName, e);
                return null;
            }

        }

        private IDataAccess GetDataAccess()
        {
            Dictionary<String, Object> dataAccessMap = null;
            try
            {
                dataAccessMap = (Dictionary<String, dynamic>)Harness.GCONFIG.Get("DataAccess");
                if (dataAccessMap == null)
                {
                    logger.HandleError("DataAccess is not configured");
                }

                return (IDataAccess)PluginManager.GetPlugin(dataAccessMap);
            }
            catch (Exception e)
            {
                logger.HandleError("'DataAccess' is configured incorrectly", dataAccessMap, e);
                throw;
                return null;
            }
        }

        /**
         * Returns ExcelAccess format string
         */
        /**public static Boolean isSheetExists(string sSerializedFileName, string SHN_REFERENCES) */
        /**{ */
        /**            return true; */
    /**     } */

        public String ToString()
        {
            return StringUtils.MapString(this, param);
        }

        /**private LogUtils logger = new LogUtils(this); */
        readonly private Dictionary<String, dynamic> param;
        /**private String testName; */
    }
}
