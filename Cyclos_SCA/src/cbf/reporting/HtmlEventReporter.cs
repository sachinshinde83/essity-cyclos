﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using src.cbf.engine;
using src.cbf.harness;
using src.cbf.model;
using src.cbf.utils;
using System.Globalization;

namespace src.cbf.reporting
{

    /**
     * 
     * Implements ResultReporter and generates HTML reports
     * 
     */
    public class HtmlEventReporter : IResultReporter, IDisposable
    {

        private TextWriter fileWriter;
        /** private TextWriter bufWriter1, bufWriter2; */
        readonly private LogUtils logger;
        private String summaryPath, detailsPath, logoPath;
        private String entityName;
        private String tcName = "", testSetPath;
        private int failed, testCases;
        private DateTime testStart, testFinish, startTime;
        readonly private List<String> detailsArray = new List<String>();
        readonly private List<String> summaryArray = new List<String>();
        readonly private List<String> testSetArray = new List<String>();
        readonly private List<String> testCasesArray = new List<String>();
        readonly private Dictionary<String, dynamic> param;


        /**
         * Constructor to initialize parameters
         * 
         * @param params
         *            map containing parameters
         */
        enum Check
        {
            START,
            FINISH,
            DETAILS
        };
        public HtmlEventReporter(Dictionary<String, dynamic> param)
        {
            if (param == null)
            {
                throw new ArgumentNullException();
            }
            logger = new LogUtils(this);
            this.param = param;
            ReporterInit(param);
        }

        public void ReporterInit(Dictionary<String, dynamic> param)
        {
            summaryPath = (!this.param.ContainsKey("summaryPath")) ? "" : (String)param["summaryPath"];

            if (summaryPath.Equals(""))
            {
                summaryPath = ResourcePaths.GetInstance().GetRunResource("", "");
            }
            if (!(FileUtils.MakeFolder(summaryPath)))
            {
                logger.HandleError("Cant create/access html reports folder; these will not be generated: "
                        + summaryPath);
            }
            detailsPath = (!this.param.ContainsKey("detailsPath")) ? "" : (String)param["detailsPath"];


            /**  detailsPath = ""; */
            if (detailsPath.Equals(""))
            {
                detailsPath = ResourcePaths.GetInstance().GetRunResource("HtmlEvents", "");
            }
            if (!(FileUtils.MakeFolder(detailsPath)))
            {
                logger.HandleError("Cant create/access html reports folder; these will not be generated: "
                        + detailsPath);
            }

            logoPath = ResourcePaths.GetInstance().GetFrameworkResource("Resources", "Capgemini.jpg");
        }


        /**
         * Reporter open method
         * 
         * @param headers
         *            contains header info, like run name, config details etc
         */
        public void Open(Dictionary<String, Object> headers)
        {
            logger.Trace("Reporter open method");
        }

        /**
         * Reporter close method
         */
        public void Close()
        {
            logger.Trace("Reporter close method");
        }

        /**
         * Reports entity execution start details
         * 
         * @param result
         *            entity object
         */
        public void Start(TestResult result)
        {
            if (result == null)
            {
                throw new ArgumentNullException();
            }
            Report("START", result, result.entityDetails);
        }

        /**
         * Logs execution details in report
         * 
         * @param result
         *            entity details
         * @param rsType
         *            result type of the current executed entity
         * @param details
         *            execution details of the current executed entity
         */
        public void Log(TestResult result, TestResult.ResultType rsType, Object details)
        {
            Report("DETAILS", result, details);
        }

        /**
         * Reports execution details along with result counts
         * 
         * @param result
         *            execution details
         * @param rsType
         *            result type of the current executed entity
         * @param details
         *            execution details of the current executed entity
         */
        public void Finish(TestResult result, TestResult.ResultType rsType, Object details)
        {
            if (result == null)
            {
                throw new ArgumentNullException();
            }
            Report("FINISH", result, result.entityDetails);
        }

        private void WriteTestSet()
        {
            testStart = startTime;
            StartSummaryFile("TestSet" + ".html", "TestSet");
            String str = "<td>" + testCases + "</td><td>" + failed + "</td><td>"
                    + testStart + "</td><td>" + testFinish + "</td><td>"
                    + CalculateDuration(testFinish, testStart) + "</td><td>"
                    + EReportLink("ExecutionReport");
            testSetArray.Add(str);
            LogSummary(testSetArray);
            TestCaseSummaryTemplate();
            LogSummary(testCasesArray);
            FinishSummaryFile();
        }

        private void Report(String eventType, TestResult result, Object eventDataa)
        {
            
            String strhtml = string.Empty;
            Check TestKey = (Check)Enum.Parse(typeof(Check), eventType);
            try
            {
                
                if (result.entityType == TestResult.EntityType.Iteration)
                {
                    if (Check.START.Equals(TestKey)) /** Check.START.Equals(TestKey) */
                    {
                        if (testStart == null)
                        {
                            testStart = result.startTime;
                        }                        
                        String iterName = "";
                        ITestIteration iteration = (ITestIteration)eventDataa;
                        Dictionary<String, dynamic> iterParams = iteration.Parameters();
                        if (!iterParams.Equals(null))/** iterParams != null */
                        {
                            if (iterParams.ContainsKey("_rowId"))
                            {
                                iterName = iterParams["_rowId"].ToString();
                            }
                            if (string.IsNullOrEmpty(iterName))
                            {
                                iterName = "(" + result.parent.childCount + " of " + ((ITestCase)result.parent.entityDetails).IterationCount() + ")";
                            }
                        }
                        tcName = result.parent.entityName;
                        if (!iterName.Equals(null))
                        {
                            tcName = tcName + " " + iterName;
                        }

                        detailsArray.Add("<td colspan = 8 align = center>" + tcName + "</td>");

                    }


                    if (Check.FINISH.Equals(TestKey))
                    {
                        ITestIteration ab = (ITestIteration)eventDataa;
                        testCases++;
                        testFinish = result.finishTime;
                        String str = "";
                        String status = "";
                        StringBuilder sbstr = new StringBuilder();
                        if (result.msRsType.IsPassed())
                        {
                            status = "Passed";
                        }
                        else
                        {
                            status = "Failed";
                            failed++;
                        }
                        sbstr.Append("<td>").Append((ab).StepCount()).Append("</td><td>").Append(result.childCount).Append("</td><td>").Append(result.startTime)
                            .Append("</td><td>").Append(result.finishTime).Append("</td><td>").Append(CalculateDuration(result.finishTime,result.startTime));

                        strhtml = "<td>" + tcName + "</td><td>" + status + "</td>"
                                + sbstr.ToString();
                        summaryArray.Add(strhtml);
                        strhtml = "<td>"
                                + tcName
                                + "</td><td>"
                                + TestCaseFileLink(
                                        entityName, status)
                                + "</td>" + sbstr.ToString();
                        testCasesArray.Add(strhtml);
                    }
                }

                if (result.entityType == TestResult.EntityType.TestCase)
                {
                    if (Check.START.Equals(TestKey))
                    {
                        entityName = result.entityName;
                        testSetPath = "../TestSet" + ".html";
                        StartDetailsFile(entityName + ".html", entityName);
                    }
                    if (Check.FINISH.Equals(TestKey))
                    {
                        LogDetails(summaryArray);
                        ComponentsTemplate();
                        LogDetails(detailsArray);

                        FinishDetailsFile();
                        if (((String)((Dictionary<String, Object>)Harness.GCONFIG.Get("Runner"))["plugin"]).Equals("TestSetRunner"))
                        {
                            WriteTestSet();
                        }
                        summaryArray.Clear();
                        detailsArray.Clear();
                    }
                }
                if (result.entityType == TestResult.EntityType.TestStep)
                {
                    if (Check.START.Equals(TestKey))
                    {
                        startTime = result.startTime;
                        /**detailsArray.add(str); */
                    }
                    if (eventType.Equals(Check.FINISH) && !(result.finalRsType.Equals(TestResult.ResultType.DONE)))
                    {

                        String str = "<td + rightSpan + >"
                                + result.entityName.ToString() + "</td><td/><td/>"
                                + "<td>";

                        str = result.msRsType.IsPassed() ? str + "PASSED" : str + "FAILED";
                        str = str + "</td><td>" + result.startTime + "</td>";
                        str = str + "<td>" + result.finishTime + "</td>";
                        str = str
                                + "<td>"
                                + CalculateDuration(result.finishTime,
                                        result.startTime) + "</td>";

                    }
                }
                if (result.entityType == TestResult.EntityType.Component && Check.DETAILS.Equals(TestKey))
                {
                    Dictionary<object, object> detailsMap = (Dictionary<object, object>)eventDataa;
                    StringBuilder sbstr = new StringBuilder();
                    string td = "<td>";
                    //String str = "<td + rightSpan + >" + detailsMap["name"] + "</td>";
                    sbstr.Append("<td + rightSpan + >").Append(detailsMap["name"]).Append(td).Append(td)
                        .Append(result.finalRsType).Append(td).Append(td).Append(detailsMap["expected"]).Append(td)
                        .Append(td).Append(ScreenDumpLink((string)detailsMap["actual"], result)).Append(td).Append(startTime).Append(td).Append(td)
                        .Append(DateTime.Now).Append(td).Append(td).Append(CalculateDuration(DateTime.Now, startTime)).Append(td);

                    startTime = DateTime.Now;
                    detailsArray.Add(sbstr.ToString());

                }



            }

            catch (FileLoadException e)
            {
                logger.HandleError("Error in HTML reporting", e);
            }

        }

        private String StylingTemplate()
        {
            StringBuilder sbstr = new StringBuilder();
            sbstr.Append("body {background-color: #FFFFCC;}").Append("table {background-color: #DCDCDC; text-align: center;}")
                .Append("th { background-color: #003399;text-align: center;  color: #FFFFFF; font-family:  Candara, Calibri, Segoe, 'Segoe UI', Optima, Arial, sans-serif;font-size: 15px;}")
                .Append("tr { background-color: #E6E6E6; color: #1F1F7A;  font-family:  Candara, Calibri, Segoe, 'Segoe UI', Optima, Arial, sans-serif;font-size: 13px;}")
                .Append("tr.d0 td { background-color: #01A9DB; }")
                .Append("h1 { align=center; text-align: center; color: #003399; font-family:  Candara, Calibri, Segoe, 'Segoe UI', Optima, Arial, sans-serif;font-size: 25px; }")
                .Append("h4 {text-align=right;font-family:  Candara, Calibri, Segoe, 'Segoe UI', Optima, Arial, sans-serif;font-size: 13px; }");

            return sbstr.ToString();
        }

        private void StartDetailsFile(String fileName, String title)
        {
            OpenDetailsFile(detailsPath + "/" + fileName);

            WriteDetails("<html><head><style>");
            WriteDetails(StylingTemplate());
            WriteDetails("<title>" + title + "</title>");
            WriteDetails("</style></head><body>");
            WriteDetails("<img src=" + logoPath + " align=right>");
            WriteDetails("<h1>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<u>TEST CASE REPORT</u></h1>");
            TestCaseDetailsTemplate();

        }

        private void StartSummaryFile(String fileName, String title)
        {
            OpenSummaryFile(summaryPath + "/" + fileName);

            WriteSummary("<html><head><style>");
            WriteSummary(StylingTemplate());
            WriteSummary("<title>" + title + "</title>");

            WriteSummary("</style></head><body>");
            WriteSummary("<img src=" + logoPath + " align=right>");
            WriteSummary("<h1>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<u>TEST SET REPORT</u></h1>");
            TestSetTemplate();

        }

        private void FinishDetailsFile()
        {
            WriteDetails("</table><h4 align=right><a href='" + testSetPath + "'>TestSetFile</a></h4></body></html>");

            CloseDetailsFile();
        }

        private void FinishSummaryFile()
        {
            WriteSummary("</table></body></html>");

            CloseSummaryFile();
        }

        private void TestSetTemplate()
        {
            WriteSummary("&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<table align=center border=3 width=100%>");
            WriteSummary("<th colspan=7>TEST SET SUMMARY</th>");
            WriteSummary("<tr class=d0><td><b>Tests Executed</b>" +
                "</td><td><b>Tests Failed</b></td>" +
                "<td><b>Start time</b></td><td>" +
                "<b>End time</b></td><td><b>Duration</b>" +
                "</td><td><b>Execution Report</b></td></tr><br>");
        }

        private void TestCaseDetailsTemplate()
        {
            WriteDetails("&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<table align=center border=3  width=100%>");
            WriteDetails("<th colspan=7>TEST CASE SUMMARY</th>");
            WriteDetails("<tr class=d0><td><b>TestCase Name</b>" +
                "</td><td><b>Status</b></td><td>" +
                "<b>No. of components</b></td><td>" +
                "<b>Components Run</b></td><td>" +
                "<b>Start time</b></td><td><b>End time</b>" +
                "</td><td><b>Duration</b></td></tr>");
        }

        private void TestCaseSummaryTemplate()
        {
            WriteSummary("<table align=center border=3 width=100%>");
            WriteSummary("<br><br><br>");
            WriteSummary("<th colspan=7>TEST CASE SUMMARY</th>");
            WriteSummary("<tr class=d0><td>" +
                "<b>TestCase Name</b></td><td><b>Status</b></td><td><b>" +
                "No. of components</b></td><td><b>Components Run</b></td>" +
                "<td><b>Start time</b></td><td><b>End time</b></td><td><b>" +
                "Duration</b></td></tr>");
        }

        private void ComponentsTemplate()
        {
            WriteDetails("<table align=center border=3 width=100%>");
            WriteDetails("<br><br><br>");
            WriteDetails("<th colspan=10>TEST CASE DETAILS</th>");
            WriteDetails("<tr class=d0>"
                    + "<td><b>Step</b></td><td><b>Result</b></td>" +
                    "<td><b>Expected Result</b></td>" +
                    "<td><b>Actual Result</b></td>" +
                    "<td><b>Start Time</b></td>" +
                    "<td><b>End Time</b></td>" +
                    "<td><b>Duration</b></td></tr>");
        }

        private void LogDetails(List<String> strArray2)
        {
            foreach (String str in strArray2)
            {
                WriteDetails("<tr>" + str + "</tr>");
            }
        }

        private void LogSummary(List<String> strArray2)
        {
            foreach (String str in strArray2)
            {
                WriteSummary("<tr>" + str + "</tr>");
            }
        }

        private String ScreenDumpLink(String name, TestResult eventData)
        {
            String sDumpFile;
            try
            {
                /**(String) ((Map) ((Map) eventData.miscInfo).get("screenDump")).get("filePath"); */
                sDumpFile = (String)(((Dictionary<String, dynamic>)eventData.miscInfo)["screenDump"]["filePath"]);//["filePath"]); //doubt
                sDumpFile = sDumpFile.Replace(ResourcePaths.GetInstance().GetRunResource("", ""), "..");
            }
            catch (FileNotFoundException e)
            {
                logger.HandleError(
                        "Error ", e.Message);
                return name;
            }

            if (sDumpFile == null)
            {
                /**  return null; */
                return name;
            }

            return "<a href='" + sDumpFile + "'>" + name + "</a>";
        }

        private String EReportLink(String name)
        {
            String filepath;
            try
            {
                filepath = "./" + name + ".xls";

            }
            catch (FileLoadException e)
            {
                logger.HandleError(
                        "Error in accesing excel report link in HTML report", name, e);
                return "";
            }

            if (filepath == null)
            {
                return "";
            }

            return "<a href='" + filepath + "'>" + name + "</a>";
        }

        private String TestCaseFileLink(String name, String status)
        {
            String filepath;
            try
            {
                filepath = "./HtmlEvents/" + name + ".html";

            }
            catch (FileLoadException e)
            {
                logger.HandleError(
                        "Error in accesing test Case link in HTML report", name, e);
                return "";
            }

            if (filepath == null)
            {
                return "";
            }

            return "<a href='" + filepath + "'>" + status + "</a>";
        }

        private String CalculateDuration(DateTime d2, DateTime d1)
        {
            /** long diff = d2. - d1.GetDateTimeFormats; */
            try
            {
                return (d2 - d1).ToString(@"hh\:mm\:ss", CultureInfo.InvariantCulture);
            }

            /**  String tym= (d2 - d1).ToString(@"hh\:mm\:ss");
              long diffSeconds = d2.Second-d1.Second; //diff / 1000 % 60;
              long diffMinutes = d2.Minute - d1.Minute; //diff / (60 * 1000) % 60;
              long diffHours = d2.Hour-d1.Hour; //diff / (60 * 60 * 1000) % 24;
              try {
              

                  String diffTime = cal( Convert.ToString(diffHours)) + ":"
                          + cal( Convert.ToString(diffMinutes)) + ":"
                          + cal(Convert.ToString(diffSeconds));
                  return diffTime;*/
            catch (ArithmeticException e)
            {
                logger.HandleError("Error in calculating duration in HTML report",
                        e);
                return null;
            }

        }

        /** private String cal(String time)
        {
            while (time.Length != 2)
                time = "0" + time;
            return time;
        } */

        private void OpenDetailsFile(String filePath)
        {
            try
            {
                fileWriter = new StreamWriter(filePath);
                /** bufWriter1 = new BufferedWriter(fileWriter); */
            }
            catch (IOException e)
            {
                logger.HandleError("Exception caught while trying to open a file ",
                        filePath, e);
            }
        }

        private void OpenSummaryFile(String filePath)
        {
            try
            {
                fileWriter = new StreamWriter(filePath);
                /** bufWriter2 = new StreamWriter(fileWriter); */
            }
            catch (IOException e)
            {
                logger.HandleError(
                        "Exception caught : When trying to open a file ", filePath,
                        e);
            }
        }

        private void CloseDetailsFile()
        {
            try
            {
                fileWriter.Close();
            }
            catch (IOException e)
            {
                logger.HandleError("Exception caught while closing details file ",
                        e);
            }
            finally
            {
                /** fileWriter = bufWriter1 = null; */
                fileWriter = null;
            }
        }

        private void CloseSummaryFile()
        {
            try
            {
                fileWriter.Close();
            }
            catch (IOException e)
            {
                logger.HandleError("Exception caught while closing summary file ",
                        e);
            }
            finally
            {
                try
                {
                    fileWriter.Close();
                }
                catch (IOException e)
                {
                    logger.HandleError(
                            "Exception caught while closing summary file ", e);
                }
                finally
                {
                    /** fileWriter = bufWriter2 = null; */
                    fileWriter = null;
                }
            }
        }

        private void WriteDetails(String lines)
        {
            try
            {
                fileWriter.WriteLine(lines);
            }
            catch (IOException e)
            {
                logger.HandleError(
                        "Exception caught while writing details in HTML : ", e);
            }
        }

        private void WriteSummary(String lines)
        {
            try
            {
                fileWriter.WriteLine(lines);
            }
            catch (IOException e)
            {
                logger.HandleError("Exception caught while writing details in HTML : ", e);
            }
        }

        /**
         * Returns HtmlEventReporter along with html report folder path format
         * string
         */
        public String ToString()
        {
            return StringUtils.MapString(this, param);

        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);            
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing && fileWriter != null)
            {               
                
                    fileWriter.Dispose();
                    fileWriter = null;
                
            }
             
            
        }
    }

}
