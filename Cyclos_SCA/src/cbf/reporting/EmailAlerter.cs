﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using System.Text.RegularExpressions;
using src.cbf.model;
using src.cbf.engine;
using src.cbf.harness;
using src.cbf.utils;

using Microsoft.Office.Interop.Excel;
using Microsoft.Office.Interop;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Windows.Forms;
using System.Runtime;
using System.Net.Mail;
using Outlook = Microsoft.Office.Interop.Outlook;
using System.Globalization;

namespace src.cbf.reporting
{
    public class EmailAlerter : IResultReporter
    {
       readonly private LogUtils logger;
        readonly private String toField = string.Empty;
        private static String subject = string.Empty;
        private String body = string.Empty;
        private static String tcBody = string.Empty;
        private String tsBody = string.Empty;
        private static Boolean isFailed;
        private static List<String> tsMap = new List<String>();
        private static int  index;
        private const int numone = 1;
        private const int numfive = 5;
        private const int numeight = 8;


        public EmailAlerter(Dictionary<String, dynamic> param)
        {
            if(param==null)
            {
                throw new ArgumentNullException();
            }
            logger = new LogUtils(this);
            logger.Trace(ConstantVar.EMAILTRACER, param);
            toField = (String)param[ConstantVar.MAILTO];
            subject = ConstantVar.MAILSUBJECT;
            body = ConstantVar.MAILBODY;

        }

        enum TestTerms
        {
            Testcase,
            TestStep,
            Iteration,
            Component
        };



        public void Close()
        {
            if (isFailed)
            {
                
                List<string> attachments = new List<string>();
                tsBody = MakeHTMLTableList(tsMap);
                body = body + "<br>" + tcBody + "<br><br>" + tsBody
                        + "<br><br>Regards,<br>Automation Team";
                int indexa = tsMap.Count - numone;
                while (indexa > numfive)
                {
                    attachments.Add(tsMap[indexa]);
                    indexa = indexa - numeight;
                }
                /** sendMail(toField, subject, body, tsMap[index]); */
                SendMail(toField, subject, body, attachments);
            }

        }

        public void Open(Dictionary<String, dynamic> headers)
        {
            logger.Trace("Open File For Header");
        }
        /**public void open()
        {
        } */
        public void Finish(TestResult result, TestResult.ResultType rsType, Object details)
        {
            Report("FINISH", result, details);
        }

        public static void Report(String eventType, TestResult result, Object eventDatas)
        {
            if(eventDatas==null||eventType==null||result==null)
            {
                throw new ArgumentNullException();
            }
            Dictionary<dynamic, dynamic> e = (Dictionary<dynamic,dynamic>)eventDatas;
            /**String enType = result.entityType.ToString(); */
            TestTerms TestKey = (TestTerms)Enum.Parse(typeof(TestTerms), result.entityType.ToString());
            switch (TestKey)
            {
                case TestTerms.Testcase:
                    if (eventType.Equals("FINISH"))
                    {
                        subject = subject + result.entityName.ToString();

                        Dictionary<dynamic, dynamic> tcMap = new Dictionary<dynamic, dynamic>{{ "Test Case Name", result.entityName.ToString()}};                                                

                        String status = eventDatas.ToString();
                        status = eventDatas.ToString().ToLower(CultureInfo.InvariantCulture).Contains("fail") ? "<b><font color='#FF0000'>" + status
                                    + "<font></b>" : "<b><font color='#00FF00'>" + status
                                    + "<font></b>";
                       
                        tcMap.Add("Test Case Status", status);
                        tcMap.Add("Start Time", result.startTime);
                        tcMap.Add("End Time", result.finishTime);
                        tcBody = MakeHTMLTable(tcMap);
                    }
                    break;
                case TestTerms.Iteration:
                    break;
                case TestTerms.TestStep:
                    if (eventType.Equals("FINISH")
                            && !result.finalRsType.ToString().ToUpper(CultureInfo.InvariantCulture)
                                    .Equals("PASSED"))
                    {
                        isFailed = true;
                        tsMap.Insert(index, "Step Name");
                        tsMap.Insert(index + 1, result.entityName.ToString());
                        index = index + 8;

                    }
                    break;
                    
                case TestTerms.Component:
                    if (eventType.Equals("DETAILS") && !result.finalRsType.ToString().ToUpper(CultureInfo.InvariantCulture)
                                    .Equals("PASSED"))
                    {

                        tsMap.Add("Expected Result");
                        tsMap.Add(e["expected"]);
                        tsMap.Add("Actual Result");
                        tsMap.Add(e["actual"]);
                        tsMap.Add("ScreenDumpPath");
                        tsMap.Add(((Dictionary<dynamic, dynamic>)result.miscInfo["screenDump"])["filePath"]);


                    }
                    break;
                    default:
                    throw new InvalidOperationException("Invalid operation performed");
                    
            }

        }


        public void Log(TestResult result, TestResult.ResultType rsType, Object details)
        {
            Report("DETAILS", result, details);

        }

        /** void log(TestResult result, TestResult.ResultType rsType, Object details)
        { }*/

        public void Start(TestResult result)
        {

            logger.Trace("Start logging Test Result");
        }

        private void SendMail(String tooField, String mailsubject, String mailbody, List<string> attachments)
        {

            String pathSendMailVbs = ResourcePaths.GetInstance().GetFrameworkResource("Resources", "vbUtils.vbs");
            logger.Trace("Path of vbUtils ", pathSendMailVbs);
            if (attachments.Count == 0)
            {
                attachments = null;
            }
            try
            {
                logger.Trace("wscript " + pathSendMailVbs + " \"" + tooField + "\" \"" + mailsubject + "\" \"" + body + "\" \"" + attachments + "\"");
                Outlook.Application oApp = new Outlook.Application();
                Outlook.MailItem mail = oApp.CreateItem(Outlook.OlItemType.olMailItem) as Outlook.MailItem;
                mail.Subject = mailsubject;
                mail.HTMLBody = mailbody;
                Outlook.AddressEntry currentUser = oApp.Session.CurrentUser.AddressEntry;
                if (currentUser.Type == "EX")
                {                    
                    mail.Recipients.Add(toField);
                    mail.Recipients.ResolveAll();
                   
                    int indexe = attachments.Count - 1;
                    while (indexe >= 0)
                    {
                        mail.Attachments.Add(attachments[indexe]);
                        indexe--;
                    }
                    ((Outlook._MailItem)mail).Send();
                    /** mail.Send(); */
                    
                }

            }
            catch (IOException e)
            {
                logger.HandleError("sendMail:Unable to send mail", e.Message);
            }
        }

        private static String MakeHTMLTable(Dictionary<dynamic, dynamic> Data)
        {
            String result;  /**  = ""; */
            result = "<table border='2' style='font-size:14px; border:2px solid #98bf21; padding:3px 7px 2px 7px;'><tbody>";
            foreach (KeyValuePair<dynamic, dynamic> entry in Data)
            {

                String name = entry.Key;
                String value = "";
                value = value + entry.Value;
                result = result + "<tr border='2' style='font-size:14px; border:2px solid #98bf21; padding:3px 7px 2px 7px;'><td><b>" + name + "</b></td><td>" + value + "</td></tr>";

            }
            result = result + "</tbody>" + "</table>";
            return result;
        }


        private String MakeHTMLTableList(List<String> Data)
        {
            String result;
            result = "<table border='2' style='font-size:14px; border:2px solid #98bf21; padding:3px 7px 2px 7px;'><tbody>";
            for (int size = 0; size < Data.Count; size = size + 2)
            {

                int indexes = size + 1;

                if (size != 0 && size < Data.Count && Decimal.Floor(size % 8) == 0)
                {
                    result = result + "</tbody>" + "</table>" + "<br>" + "<table border='2' style='font-size:14px; border:2px solid #98bf21; padding:3px 7px 2px 7px;'><tbody>";

                }
                result = result + "<tr><td><b>" + Data[size] + "</b></td><td>" + Data[indexes] + "</td></tr>";

            }
            result = result + "</tbody>" + "</table>";
            return result;
        }


        public String ToString()
        {
            return StringUtils.MapString(this, toField);

        }
    }
}
