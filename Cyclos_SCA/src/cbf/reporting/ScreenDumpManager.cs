﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using System.Text.RegularExpressions;
using src.cbf.model;
using src.cbf.engine;
using src.cbf.harness;
using src.cbf.utils;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.Drawing;

namespace src.cbf.reporting
{
    public class ScreenDumpManager : IResultReporter
    {


        /**
         * Constructor to initialize folder path
         * 
         * @param param
         *            map containing parameters
         */
        public ScreenDumpManager(Dictionary<String, dynamic> param)
        {
            if(param==null)
            {
                throw new ArgumentNullException();
            }
            logger = new LogUtils(this);

            this.param = param;
            dumpFolder = (!this.param.ContainsKey("summaryPath")) ? "" : (String)param["folderpath"];
           
           
            if (dumpFolder.Equals(""))
            {
                dumpFolder = ResourcePaths.GetInstance().GetRunResource("ScreenShots", "");
            }

            if (!FileUtils.MakeFolder(dumpFolder))
            {
                logger.HandleError("Cant create/access ScreenShots folder; these will not be generated: ", dumpFolder);
            }
        }

        /**
         * Closes the dump manager
         */
        public void Close()
        {
            logger.Trace("Closes the Dump Manager");
        }

        /**
         * Reporter open method
         * 
         * @param headers
         *            contains header info, like run name, config details etc
         */
        public void Open(Dictionary<String, dynamic> headers)
        {
            logger.Trace("Open");
        }


        /**
         * Reporter finish method
         * 
         * @param result
         *            execution details
         * @param rsType
         *            result type of the current executed entity
         * @param details
         *            execution details of the current executed entity
         */
        public void Finish(TestResult result, TestResult.ResultType rsType, Object details)
        {
            logger.Trace("Finish");
        }

        /**
         * Logs Screenshot
         * 
         * @param result
         *            entity details
         * @param rsType
         *            result type of the current executed entity
         * @param details
         *            execution details of the current executed entity
         */
        public void Log(TestResult result, TestResult.ResultType rsType, Object details)
        {
            if(result==null)
            {
                throw new ArgumentNullException();
            }
            /** Dictionary<String, dynamic> details = (Dictionary<String, dynamic>)details1;	*/	
            Dictionary<Object, dynamic> detailss = (Dictionary<Object, dynamic>)details;

            logger.Trace("Report log");

            if (!IsScreenDump(result, detailss))
            {
                return;
            }
            /** 
             logger.trace(details.containsKey("screenDump"));
              logger.trace(details.get("screenDump"));
            
            if (details["screenDump"].toString().equalsIgnoreCase("true")) */
            if (detailss["screenDump"].ToString().Equals("True"))
            {

                String dumpName = MakeDumpName(result);
                String fileName = dumpName + ".png";
                fileName = CheckFileName(dumpFolder, fileName.Replace(" ", "_"));
                filePath = dumpFolder + "\\" + fileName.Replace(" ", "_");
                DumpScreen(filePath);
                if (!result.miscInfo.ContainsKey("screenDump"))
                {
                    result.miscInfo.Add("screenDump", new Dictionary<Object,Object>{{ "name", dumpName}, {"fileName", fileName}, {"filePath", filePath} });
                }
                else
                {                    
                    result.miscInfo["screenDump"] = new Dictionary<Object,Object>{{"name", dumpName}, {"fileName", fileName}, {"filePath", filePath }};
                }
            }
        }


        public String CheckFileName(String folderName,String fileName)
        {
            string [] fileEntries = Directory.GetFiles(folderName);
            Random rnd = new Random();
            /**DateTime dt = DateTime.Now.ToString(); */
            
            foreach (string file in fileEntries)
            {
                if (file.Contains(fileName))
                {
                    String[] temp = fileName.Split('.');
                    fileName = String.Empty;
                    fileName =temp[0]+"_"+rnd.Next()+".png";
                    /**fileName = temp[0] + "_" + DateTime.Now.ToString() + ".png"; */
                }
            
            }
            return fileName;
        }
        /**
         * Starts the screenshot process
         * 
         * @param result
         *            object of TestResult
         */
        public void Start(TestResult result)
        {
            logger.Trace("Start");
        }

        /**
         * private void dumpScreen(String fileName) { logger.trace("DumpScreen : " ,
         * fileName); Rectangle screenRectangle = new
         * Rectangle(Toolkit.getDefaultToolkit() .getScreenSize()); Robot robot =
         * null; try { robot = new Robot(); } catch (AWTException e) {
         * logger.handleError("Exception in ScreenDumpManager " , e); }
         * BufferedImage image = robot.createScreenCapture(screenRectangle); try {
         * ImageIO.write(image, "png", new File(fileName)); } catch (IOException e)
         * { logger.handleError("Exception caught while creating screen dump",
         * fileName, e); } }
         */

        private void DumpScreen(String fileName)
        {
            logger.Trace("DumpScreen : " + fileName);
            try
            {
                /**Create a new bitmap. */
                var bmpScreenshot = new Bitmap(Screen.PrimaryScreen.Bounds.Width,
                                               Screen.PrimaryScreen.Bounds.Height,
                                               PixelFormat.Format32bppArgb);

                /** Create a graphics object from the bitmap.*/
                var gfxScreenshot = Graphics.FromImage(bmpScreenshot);

                /** Take the screenshot from the upper left corner to the right bottom corner. */
                gfxScreenshot.CopyFromScreen(Screen.PrimaryScreen.Bounds.X,
                                            Screen.PrimaryScreen.Bounds.Y,
                                            0,
                                            0,
                                            Screen.PrimaryScreen.Bounds.Size,
                                            CopyPixelOperation.SourceCopy);

               /** Save the screenshot to the specified path that the user has chosen. */
                bmpScreenshot.Save(@fileName, ImageFormat.Png);

            }
            catch (FileNotFoundException e)
            {
                logger.HandleError("Exception in ScreenDumpManager " + e.Message);
            }

        }

        private String MakeDumpName(TestResult result)
        {
            String name;
            name = result.parent.parent.parent.entityName;
            name = name + "_" + result.parent.entityName;/** step */
            name = name + "_" + result.childCount; /**check#. Makes sure that sName */
            /** is unique, even when
            checkName isnt */
            UniqueUtils uniqueUtils = new UniqueUtils();
            uniqueSuffix = uniqueUtils.UniqueString("3");
            /**name = name + param["pattern"] + "_" + uniqueSuffix; */
            name = name + "_" + uniqueSuffix;

           /** ' blank out suspicious characters
            Pattern pattern = Pattern.compile("[^\\w-_]+");
            Matcher m = pattern.matcher(name);
            while (m.find()) 
            {
                m.replaceAll(" ");
            }*/
            return name;
        }

        /** ' Determine if screen dump is needed */
        private Boolean IsScreenDump(TestResult result, dynamic eventData)
        {
            if (result.entityType != TestResult.EntityType.Component)
            {
                return false;
            }/** eventData["screenDump"] = false; */
            Boolean isPassed = result.finalRsType.IsPassed();
            if (!isPassed || result.finalRsType == TestResult.ResultType.PASSED)
            { /** ' Not
                // passed or
                // warning */
                eventData["screenDump"] = true;
                /** return true; // ' Enable screen dump for all failed logs */
            }

            return (Boolean)eventData["screenDump"];
        }

        /**
         * Returns ScreenDumpManager along with screendump file path format string
         */

        public String ToString()
        {
            return StringUtils.MapString(this, param);
        }

        readonly private String dumpFolder;
        private String filePath;
        String uniqueSuffix;
        readonly private Dictionary<String, dynamic> param;
        public LogUtils logger;

    }
}
