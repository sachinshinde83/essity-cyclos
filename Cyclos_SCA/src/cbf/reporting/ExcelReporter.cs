﻿/*Copyright Â© 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using System.Text.RegularExpressions;
using src.cbf.model;
using src.cbf.engine;
using src.cbf.harness;
using src.cbf.utils;

using Microsoft.Office.Interop.Excel;
using Microsoft.Office.Interop;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Windows.Forms;
using System.Globalization;

namespace src.cbf.reporting
{
    /**
    * 
    * Implements ResultReporter and generates Excel reports
    * 
    */


    public class ExcelReporter : IResultReporter
    {

        readonly private LogUtils logger;
        readonly List<String> testCases = new List<String>();

        /**
         * Constructor to initialize parameters
         * 
         * @param params
         *            map containing parameters
         */
        public ExcelReporter(Dictionary<String, dynamic> param)
        {
            if (param == null)
            {
                throw new ArgumentNullException();
            }
            logger = new LogUtils(this);
            filePath = (String)param["filepath"];
            if (filePath.Equals(""))
            {
                filePath = ResourcePaths.GetInstance().GetRunResource("ExecutionReport" + ".xls", "");
            }
            templatePath = ResourcePaths.GetInstance().GetFrameworkResource("Resources", "ReportTemplate.xls");
            SHN_SUMMARY = "Summary";
            SHN_DETAILS = "Details";
            SHN_COVER = "Cover";
            LINK_COL = "A";
        }

        /**
         * Function: open(public) Goal: Makes the engine ready for use Out Params:
         * Boolean - Didn't the engine manage to start?
         */

        public void Open(Dictionary<String, dynamic> headers)
        {
            logger.Trace("Report: open");
            /** File srcFile = new File(templatePath); */
            FileInfo srcFile = new FileInfo(templatePath);
            /**File targetFile = new File(filePath); */
            FileInfo targetFile = new FileInfo(filePath);

            /** boolean isExists = targetFile.exists(); */
            if (!File.Exists(filePath))
            {
                try
                {
                    File.Copy(templatePath, filePath, true);
                }
                catch (IOException e)
                { /** FIXME: specific exception */
                    logger.HandleError("Error in making a new report file from template", e, srcFile, targetFile);
                }
            }

            if (File.Exists(filePath))
            { /** new file */
                OpenFile();
                WriteHeaders(headers);
            }
        }

        public void ReleaseObject(object obje)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obje);
                obje = null;
            }
            catch (ObjectDisposedException ex)
            {
                obje = null;
                logger.HandleError("Error" + ex.Message);
            }
            finally
            {
                GC.Collect();
            }
        }

        /**
         * Function: close(public) Goal: Stops the engine In Params: None Out
         * Params: None
         */
        public void Close()
        {
            logger.Trace("Report: Close");

            /**evaluateFormulae(); */
            try
            {
                /**outputStream = new StreamWriter(filePath);
                newWorkbook.write(outputStream);
                newWorkbook.Worksheets.Add(outputStream); */
                newWorkbook.Save();
                ReleaseObject(oSummarySheet);
                ReleaseObject(oDetailsSheet);
                ReleaseObject(oCoverSheet);

                object misValue = System.Reflection.Missing.Value;

                newWorkbook.Close(true, misValue, misValue);
                xlApp.Quit();
                Marshal.FinalReleaseComObject(xlApp);


            }
            catch (IOException e)
            {
                logger.HandleError("Error closing excel", e);
            }

            /**try
            {
                /**((StreamReader)newWorkbook).Close();
                ((StreamWriter)newWorkbook).Close();
                int TCcount = testCases.Count;
            }
            catch (IOException e)
            {
                logger.handleError("Error closing excel", e);
            } */
        }

        public void Start(TestResult result)
        {
            if (result == null)
            {
                throw new ArgumentNullException();
            }
            Report("START", result, result.entityDetails);
        }

        public void Log(TestResult result, TestResult.ResultType rsType, Object details)
        {
            Report("DETAILS", result, details);
        }

        public void Finish(TestResult result, TestResult.ResultType rsType, Object details)
        {
            Report("FINISH", result, details);
        }

        private Workbook newWorkbook;
        Microsoft.Office.Interop.Excel.Application xlApp;
        private _Worksheet oSummarySheet;
        private _Worksheet oDetailsSheet;
        private _Worksheet oCoverSheet;
        private int detailsRowNum, summaryRowNum;
        /**private CreationHelpers createHelper;
        private StreamReader inputStream;
        private StreamWriter outputStream; */

        /**
         * 'Function Name: openFile 'Description: This Function opens a created
         * Result File 'Parameter: FilePath - Path of the result file
         */
        private void OpenFile()
        {
            try
            {
                

                xlApp = ExcelAccess.GetxlApp();
                object _missingValue = System.Reflection.Missing.Value;
                newWorkbook = xlApp.Workbooks.Open(filePath,
                                                                        _missingValue,
                                                                        false,
                                                                        _missingValue,
                                                                        _missingValue,
                                                                        _missingValue,
                                                                        true,
                                                                        _missingValue,
                                                                        _missingValue,
                                                                        true,
                                                                        _missingValue,
                                                                        _missingValue,
                                                                        _missingValue);

                /** newWorkbook = ExcelAccess.getWorkbook(xlApp, filePath); */
                newWorkbook.Save();
                /**newWorkbook = new XSSFWorkbook(fs);
                }
                else if (fileExtensionName.Equals(".xls")) 
                {
                    newWorkbook = new HSSFWorkbook(inputStream);
                }
 
                Sheets sheet = newWorkbook.Worksheets;
                Worksheet worksheet = (_Worksheet)sheet.get_Item(sheetName); */

                oSummarySheet = newWorkbook.Worksheets[SHN_SUMMARY];
                oDetailsSheet = newWorkbook.Worksheets[SHN_DETAILS];
                oCoverSheet = newWorkbook.Worksheets[SHN_COVER];


                summaryRowNum = oSummarySheet.UsedRange.Rows.Count + 1;
                detailsRowNum = oDetailsSheet.UsedRange.Rows.Count + 1;
            }
            catch (IOException e)
            {
                logger.HandleError("Error while accessing workbook ", e);
            }

        }

        private void WriteHeaders(Dictionary<String, dynamic> headers)
        {
            foreach (KeyValuePair<String, Object> entry in headers)
            {

                try
                {
                    Object cell = FindCellByName("Hdr" + entry.Key, oCoverSheet);
                    if (cell != null)
                    {
                        WriteCellValue(oCoverSheet, (Excel.Range)cell, entry.Value);
                    }
                }
                catch (COMException e)
                {
                    logger.Warning("Hdr" + entry.Key + ": failed to write this header: " + e.Message);


                }

            }
        }

        /**
         * Function: report(public) Goal:Reports an event
         */
        private void Report(String eventType, TestResult result, Object eventData)
        {
            logger.Trace("Report:" + eventType + ":" + StringUtils.ToString(result) + ":" + StringUtils.ToString(eventData));

            try
            {
                switch (result.entityType)
                {

                    case TestResult.EntityType.Iteration:
                        if (eventType.Equals("START"))
                        {
                            ITestIteration iteration = (ITestIteration)eventData;
                            String iterName = "";
                            Dictionary<String, dynamic> iterParams = iteration.Parameters();
                            if (iterParams != null)
                            {
                                if (iterParams.ContainsKey("_rowId"))
                                {
                                    iterName = iterParams["_rowId"];
                                }
                                if (iterName == null || iterName.Equals(""))
                                {
                                    iterName = "("
                                            + result.parent.childCount
                                            + " of "
                                            + ((ITestCase)result.parent.entityDetails)
                                                    .IterationCount() + ")";
                                }
                            }
                            String tcName = result.parent.entityName;
                            testCases.Add(tcName);
                            if (iterName != null)
                            {
                                tcName = tcName + " " + iterName;
                            }

                            StartTestCase(tcName, iteration.StepCount(), "",
                                    result.startTime); /**' Skip execStatus */
                        }
                        if (eventType.Equals("FINISH"))
                        {
                            String msg = "";
                            FinishTestCase(result.msRsType.IsPassed(), msg,
                                    result.finishTime, result.childCount);
                        }
                        break;
                    case TestResult.EntityType.TestStep:
                        if (eventType.Equals("START"))
                        {
                            ITestStep testStep = (ITestStep)eventData;
                            StartTestStep(result.parent.childCount, result.entityName,
                                    testStep.StepName());
                        }
                        if (eventType.Equals("FINISH"))
                        {
                            FinishTestStep(result.entityName,
                                    result.msRsType.IsPassed(), result.startTime,
                                    result.finishTime);
                        }
                        break;
                    case TestResult.EntityType.Component:
                        if (eventType.Equals("DETAILS"))
                        {
                            Dictionary<dynamic, dynamic> attibs = (Dictionary<dynamic, dynamic>)eventData;
                            ReportCheck(attibs["name"].ToString(), result, attibs, attibs["screenDump"].ToString());
                        }
                        break;
                    default:
                        logger.Trace("Error");
                        break;

                }

            }
            catch (IndexOutOfRangeException e)
            {
                logger.HandleError("Error in Excel reporting", e.Message);
            }
        }

        private Object Hyperlinked(Object main, String sheetName, int rowNum)
        {
            return new[] { (String)main, sheetName + "!" + LINK_COL + rowNum };
            /**return oSummarySheet.Hyperlinks.Add("A1",
                                         string.Empty,
                                         LINK_COL + rowNum,
                                         "Screen Tip Text",
                                         "Hyperlink Title"); */
        }

        public void StartTestCase(String TestCaseName, int stepsCount, String execStatus, DateTime startTime)
        {
            if (execStatus == null)
            {
                throw new ArgumentNullException();
            }
            Dictionary<Object, Object> data = new Dictionary<Object, Object> {
                { "TestCase", TestCaseName },
                { "Result", Hyperlinked("In Progress", 
                SHN_DETAILS, detailsRowNum) },
                { "StepsCount", stepsCount },
                { "StartTime", startTime } };

            if (!execStatus.Equals("") && execStatus != "No Run")
            {
                data.Add("ExecStatus", "*");
            }

            WriteSumValues(data, false);
            WriteDtlValues("TC", new Dictionary<Object, Object> { { "Name", TestCaseName } });
        }


        public void FinishTestCase(Boolean isPassed, String msg, DateTime finishTime, int stepsRun)
        {
            String sStatus;
            String[] eventReport = { "", "" }, executionLog = { "", "" };
            /**if ((result.miscInfo["EventReport"]) != null)
            {
                eventReport[0] = (String)((dynamic)result.miscInfo["EventReport"])["fileName"];
                eventReport[1] = (String)((dynamic)result.miscInfo["EventReport"])["filePath"];
            }*/

            sStatus = (isPassed) ? "Passed" : "Failed";

            Dictionary<Object, Object> data = new Dictionary<Object, Object>{
                { "Result", sStatus},
                { "ErrMsg", msg},
                {"FinishTime", finishTime},
                { "StepsRun", stepsRun},
                { "EventReport",eventReport},
                { "ExectionLog", executionLog }};

            WriteSumValues(data, true);
            summaryRowNum = summaryRowNum + 1;
        }

        public void StartTestStep(int stepNum, String stepName,
                String stepDescription)
        {

            Dictionary<Object, Object> data = new Dictionary<Object, Object>{{ "Ix", stepNum}, {"Name", stepName},
                    {"Description", stepDescription }};

            WriteDtlValues("StepHdr", data);
        }

        public void FinishTestStep(String stepName, Boolean isPassed,
               DateTime startTime, DateTime finishTime)
        {
            String sStatus;
            sStatus = isPassed ? "Passed" : "Failed";


            WriteDtlValues("StepFtr", new Dictionary<Object, Object>{{"Name", stepName}, {"Result", sStatus},
                            {"StartTime", startTime}, {"FinishTime", finishTime }});
        }

        private void ReportCheck(String checkName, TestResult result, Dictionary<Object, Object> details, String scrDumpPath)
        {
            String sExpected = "";
            Object sActual;
            try
            {
                sExpected = details["expected"].ToString();
                sActual = details["actual"].ToString();
            }
            catch (ObjectDisposedException e)
            {
                sExpected = "";
                sActual = "";
                logger.HandleError("Error" + e.Message);
            }

            /** ' Link screen dump */

            if (scrDumpPath.ToUpper(CultureInfo.InvariantCulture).Equals("TRUE") && (!sActual.Equals("")))
            {

                /** List<String> temp = null; */
                List<String> temp = new List<string>();
                temp.Add(sActual + ". Click here to view screenshot.");
                temp.Add(((dynamic)result.miscInfo["screenDump"]).ContainsKey("filePath") ? (String)((dynamic)result.miscInfo["screenDump"])["filePath"] : "");

                sActual = temp;


            }

            try
            {

                WriteDtlValues("Ck", new Dictionary<Object, Object> {
                    { "Name", checkName },
                    { "Result", result.finalRsType },
                    { "Expected", sExpected },
                    { "Actual", sActual },
                    { "Ts", DateTime.Now } });
            }
            catch (ObjectDisposedException e)
            {
                logger.HandleError("Error while updating report with validation details", e);
            }
        }

        private void WriteDtlValues(String tplType, Dictionary<Object, Object> data)
        {
            try
            {
                WriteValuesFromTpl("Dtl" + tplType + "Row", "Dtl" + tplType, data, detailsRowNum, oDetailsSheet, true);
                detailsRowNum = detailsRowNum + 1;
            }
            catch (FileNotFoundException e)
            {
                logger.HandleError("Failed to write values in DETAILS sheet", e);
            }
        }

        private void WriteSumValues(dynamic data, Boolean bClearValues)
        {
            try
            {
                WriteValuesFromTpl("SumRow", "Sum", data, summaryRowNum, oSummarySheet, bClearValues);
            }
            catch (FileNotFoundException e)
            {
                logger.HandleError("Failed to write values in SUMMARY sheet", e.Message);
            }
        }

        private void WriteValuesFromTpl(String tplRowName, String pfx, dynamic data, int targetRow, _Worksheet sheet, Boolean bClearValues)
        {
            if (data.ContainsKey("Description"))
            {
                data.Remove("Description");

            }
            dynamic keys = data.Keys;
            foreach (object key in keys)
            {

                try
                {
                    Excel.Range cell = (Excel.Range)FindCellByName(pfx + key, sheet);
                    if (cell != null)
                    {
                        WriteCellValue(sheet, cell, data[key]);
                    }
                }
                catch (IOException e)
                {
                    logger.HandleError("Template column:" + pfx + key + ":to:" + data[key] + ":didnt happen:" + e.Message, e.Message);
                }
            }

            try
            {
                CopyRow(tplRowName, targetRow, sheet);
            }
            catch (IOException e)
            {
                logger.HandleError("Error while copying result details", e.Message);
            }

            if (bClearValues)
            {
                try
                {
                    ClearContents(tplRowName, sheet);
                }
                catch (IOException e)
                {
                    logger.HandleError("Error while clearing template values", e);
                }
            }
        }

        private void WriteCellValue(_Worksheet sheet, Excel.Range cell, dynamic value)
        {
            String tempValue = (String)String.Join(" ", value);

            try
            {
                /** clearCell(cell, sheet); */

                if (tempValue.Contains("PASSED"))
                {
                    value = string.Join(" ", value);
                    cell.Value = value;
                }

                if (tempValue.Contains("FAILED"))
                {
                    value = string.Join(" ", value);
                    cell.Value = (String)value;
                }

                if (tempValue.Contains("In Progress"))
                {
                    cell.Value = (String)value[0];
                    sheet.Hyperlinks.Add(cell, String.Empty, value[1], "Screen Tip Text", "Hyperlink Title");
                }

                if (value == null)
                {
                    value = string.Join(" ", value);
                    cell.Value = "--null--";
                    return;
                }
                else
                {
                    if (tempValue.Contains(". Click here to view screenshot."))
                    {
                        sheet.Hyperlinks.Add(cell, value[1].ToString().Replace(" ", "+"), String.Empty, value[1].ToString(), value[0].ToString());
                        return;
                    }
                    else
                    {
                        value = string.Join(" ", value);
                        cell.Value = value;
                        return;
                    }

                }


                /** try hyperlink
                String[] vals = (String[])value;
                if (!vals[0].Equals("") && !vals[1].Equals(""))
                {
                    sheet.Hyperlinks.Add(cell, "file:///" + vals[1].ToString(), Type.Missing, vals[0].ToString(), vals[0].ToString());
                    return;
                }
                
 
                try
                {
                    DateTime today = DateTime.Now;
                    cell.set_Value(today);
                    cell.Style = cell.Style;
                    cell.set_Item(cell.GetType());
                    //cell.setCellType(cell.getCellType());
                    return;
                }
                catch (Exception)
                {
                }
 
                try
                {
                    Regex regexName = new Regex("(\\d{1})|(\\d{2}) ");
                    String value1 = value.toString();
                    Match m = regexName.Match(value1);
                    if (m.Success)
                    {
                        try
                        {
                            cell.Value = Int32.Parse(value1);
                            //cell.setCellType(0);
                        }
                        catch (Exception)
                        {
 
                            cell.Value = value.toString();
                            cell.set_Item(cell.Style);
                        }
                    }
 
                    else
                    {
 
                        cell.Value = value.toString();
                        cell.set_Item(cell.Style);
 
                    }
 
                    return;
                }
                catch (Exception)
                {
                } */

            }
            catch (IndexOutOfRangeException e)
            {
                logger.HandleError("Error while writing to cell", e.Message);
                return;
            }

            /**logger.handleError("Cant write value of this type:", value, value.getClass()); */
        }

        private void CopyRow(String tplRowName, int targetRow, _Worksheet sheet)
        {
            try
            {
                Range src, dest;
                Range r1, r2;
                src = sheet.get_Range(tplRowName);
                /**if(tplRowName.Equals("DtlTCName"))
                {
                    dynamic mergeArea;
                    mergeArea = src.MergeArea;
 
                    r1 = sheet.Cells[targetRow, 1];
                    r2 = sheet.Cells[targetRow + mergeArea.Rows.Count - 1, mergeArea.Columns.Count];
 
                    dest = sheet.get_Range(r1, r2);
                }
                else
                {
                    r1 = sheet.Cells[targetRow, 1];
                    r2 = sheet.Cells[targetRow + src.Rows.Count - 1, src.Columns.Count];
                    dest = sheet.get_Range(r1, r2);
                }*/

                if (tplRowName != "DtlStepHdrRow")
                {
                    if (src.MergeCells)
                    {
                        dynamic mergeArea;
                        mergeArea = src.MergeArea;

                        r1 = sheet.Cells[targetRow, 1];
                        r2 = sheet.Cells[targetRow + mergeArea.Rows.Count - 1, mergeArea.Columns.Count];

                        dest = sheet.get_Range(r1, r2);
                        /**dest = sheet.get_Range(sheet.get_Range(targetRow, 1), sheet.get_Range(targetRow + mergeArea.Rows.Count - 1, mergeArea.Columns.Count)); */
                    }
                    else
                    {
                        r1 = sheet.Cells[targetRow, 1];
                        r2 = sheet.Cells[targetRow + src.Rows.Count - 1, src.Columns.Count];
                        dest = sheet.get_Range(r1, r2);
                        /** dest = sheet.get_Range(sheet.get_Range(targetRow, 1), sheet.get_Range(targetRow + src.Rows.Count - 1, src.Columns.Count)); */
                    }
                }
                else
                {
                    r1 = sheet.Cells[targetRow, 1];
                    r2 = sheet.Cells[targetRow + src.Rows.Count - 1, src.Columns.Count];
                    dest = sheet.get_Range(r1, r2);
                    /** dest = sheet.get_Range(sheet.get_Range(targetRow, 1), sheet.get_Range(targetRow + src.Rows.Count - 1, src.Columns.Count)); */
                }

                src.Copy(dest);
                dest.EntireRow.Hidden = false;
            }
            catch (IndexOutOfRangeException e)
            {
                logger.HandleError("Error while copying updated result row to target row", e.Message);
            }

        }




        /**private void clearContents(String tplRowName, _Worksheet sheet)
        //{
 
        //    int namedCellIdx = newWorkbook.getNameIndex(tplRowName);
        //    Name aNamedCell = newWorkbook.getNameAt(namedCellIdx);
 
        //    AreaReference aref = new AreaReference(aNamedCell.getRefersToFormula());
        //    CellReference cellRef1 = aref.getFirstCell();
        //    Row r = sheet.getRow(cellRef1.getRow());
        //    Cell firstcell = r.getCell(cellRef1.getCol());
 
        //    CellReference cellRef2 = aref.getLastCell();
        //    Row r1 = sheet.getRow(cellRef2.getRow());
        //    Cell lastcell = r1.getCell(cellRef2.getCol());
 
        //    int startRow = firstcell.getRowIndex();
        //    int endRow = lastcell.getRowIndex();
        //    int startCol = firstcell.getColumnIndex();
        //    int endCol = lastcell.getColumnIndex();
 
        //    CellReference[] crefs = aref.getAllReferencedCells();
 
        //    try
        //    {
        //        for (int col = startCol; col <= endCol; col++)
        //        {
 
        //            Row r3 = sheet.getRow(crefs[col].getRow());
        //            Cell cell = r3.getCell(col);
        //            if (cell.getCellType() == Cell.CELL_TYPE_FORMULA)
        //            {
        //                logger.trace("Do not clear cell content as cell has formula");
        //            }
        //            else
        //            {
        //                clearCell(cell, sheet);
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        logger.handleError("Error while Clearing ", e);
        //    }
        //}
 
        //private void clearCell(Excel.Range cell, _Worksheet sheet)
        //{
        //    if (cell == null)
        //    {
        //        return;
        //    }
        //    object value = sheet.Cells.Value;
        //    switch (value.GetType().ToString())
        //    {   //case CellFormat
        //        case Cell.CELL_TYPE_STRING:
        //            try
        //            {
        //                //String value = cell.getStringCellValue();
        //                String Value = sheet.Cells.Value;
        //                Regex regexName = new Regex("(\\d{2})(.*)");
 
        //                Match m = regexName.Match(Value);
        //                if (m.Success)
        //                {
        //                    cell.set_Value("09/15/2015 13:31:32");
        //                }
        //                else
        //                {
        //                    cell.set_Value(" ");
        //                }
        //            }
        //            catch (Exception e)
        //            {
        //                logger.handleError("Error while clearing String cell", e);
        //            }
        //            break;
        //        case (cellType.NUMBER)
        //            try
        //            {
        //                if (DateUtil.isCellDateFormatted(cell))
        //                {
        //                    cell.setCellValue(0.0);
        //                }
        //                else
        //                {
        //                    DateFormat dateFormat = new SimpleDateFormat(
        //                            "MM/dd/yyyy HH:mm:ss");
        //                    Date today = (java.util.Date)(new Date("01-Jan-01"));
        //                    cell.setCellValue(dateFormat.format(today));
        //                }
        //                cell.setCellValue(0.0);
        //            }
        //            catch (Exception e)
        //            {
        //                logger.handleError("Error while clearing Number cell", e);
        //            }
        //            break;
        //        case Cell.CELL_TYPE_BOOLEAN:
        //            try
        //            {
        //                cell.setCellValue(false);
        //            }
        //            catch (Exception e)
        //            {
        //                logger.handleError("Error while clearing Boolean cell", e);
        //            }
        //            break;
        //        case Cell.CELL_TYPE_BLANK:
        //            try
        //            {
        //                cell.setCellValue("");
        //            }
        //            catch (Exception e)
        //            {
        //                logger.handleError("Error while clearing Blank cell", e);
        //            }
        //            break;
        //        case Cell.CELL_TYPE_FORMULA:
        //            try
        //            {
        //                cell.setCellValue(0);
        //            }
        //            catch (Exception e)
        //            {
        //                logger.handleError("Error while clearing Formula cell", e);
        //            }
        //            break;
        //    }
        //} */

        public void ClearContents(String tplRowName, _Worksheet sheet)
        {
            if (sheet == null)
            {
                throw new ArgumentNullException();
            }
            /** Range cell; */
            Range range = sheet.get_Range(tplRowName);
            ClearCell(range, sheet);

        }

        public void ClearCell(Excel.Range range, _Worksheet sheet)
        {
            if(range==null)
            {
                throw new ArgumentNullException();
            }
            if (range.Cells.Count > 1)
            {
                foreach (Range cell in range.Cells)
                {
                    ClearCell(cell, sheet);
                }
                return;
            }

            if (range.HasFormula)
            {
                return;
            }

            if (range.MergeCells)
            {
                range.MergeArea.ClearContents();
            }
            else
            {
                range.ClearContents();
            }



        }


        private Object FindCellByName(String name, _Worksheet sheet)
        {

            return sheet.get_Range(name);

            /** int namedCellIdx = newWorkbook.getNameIndex(name);
 
            //if (namedCellIdx == -1)
            //    return null;
            //Name aNamedCell = newWorkbook.getNameAt(namedCellIdx);
 
            //// retrieve the cell at the named range and test its contents
            //AreaReference aref = new AreaReference(aNamedCell.getRefersToFormula());
            //CellReference[] crefs = aref.getAllReferencedCells();
 
            //try
            //{
            //    for (int i = 0; i < crefs.Count; i++)
            //    {
            //        _Worksheet sheet = newWorkbook.getSheet(crefs[i].getSheetName());
            //        Object r = sheet.getRow(crefs[i].getRow());
            //        Object cell = r.getCell(crefs[i].getCol());
 
            //        if (cell == null)
            //        {
            //            return null;
 
            //        }
            //        else if ((cell.getCellType() != Cell.CELL_TYPE_BLANK)
            //              || !(cell == null))
            //        {
            //            return cell;
            //        }
            //    }
 
            //}
            //catch (Exception e)
            //{
            //    logger.handleError(
            //            "Error while copying updated result row to target row", e);
            //}
 
            //return null; */
        }

        /**
         * Function: evaluateFormulae(public) Goal: To Evaluate the formulae of
         * formulae cells in excel Params: None
         */




        /** private Dictionary<Object, Object> toMap(Object[] arr)
        //{
        //    return Utils.toMap(arr);
        //} */

        /**
         * Overrides toString() method of Object class to return ExcelReporter
         * format string
         */
        public String ToString()
        {

            return StringUtils.MapString(this, filePath);
        }

        private enum cellType
        {
            EMPTY, LABEL, NUMBER, NUMERICALFORMULA, DATE, DATEFORMULA,
        };


        readonly private String SHN_SUMMARY, SHN_DETAILS, SHN_COVER, LINK_COL;

        readonly private String filePath, templatePath;

    }

}