﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using src.cbf.plugin;
using src.cbf.harness;
using src.cbf.model;

using System.Threading;

namespace src.cbf.harness
{
    public class Script
    {

        public static void Run(Dictionary<String, dynamic> runMap, String runName)
        {
            Harness harness = new Harness(runMap, runName);
            Dictionary<String, Object> runnerMap = (Dictionary<String, Object>)Harness.GCONFIG.Get("Runner");
            dynamic runner = PluginManager.GetPlugin(runnerMap);
            runner.SetHarness(harness);            
            runner.Run(runMap, runName);            
            
        }
    }
}
