﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using src.cbf.utils;

namespace src.cbf.harness
{
    public class ResourcePaths
    {
        static String autoHome, workHome, runHome;

        /**
         * Returns suites path
         * 
         * @param relPath
         * @param relName
         * @return singleton
         */
        public String GetSuiteResource(String relPath, String relName)
        {

            return CreatePath(autoHome, relPath, relName);
        }

        public String GetFrameworkResource(String relPath, String relName)
        {
            return CreatePath(workHome, relPath, relName);
        }

        public String GetRunResource(String relPath, String relName)
        {
            return CreatePath(runHome, relPath, relName);
        }

        public static ResourcePaths GetInstance(String autoHome, String workHome,
                String runHome)
        {
            singleton = new ResourcePaths(autoHome, workHome, runHome);
            return singleton;
        }

        private ResourcePaths(String autoHome, String workHome, String runHome)
        {
            ResourcePaths.autoHome = autoHome;
            ResourcePaths.workHome = workHome;
          
            ResourcePaths.runHome = runHome;
        }

        /**private static ResourcePaths singleton = null;*/
        private static ResourcePaths singleton;

        public static ResourcePaths GetInstance()
        {
         
            return GetInstance(autoHome, workHome, runHome);

        }

        private String CreatePath(String home, String relPath, String relName)
        {
            if (!relPath.Equals(""))
            {
                home = home + "\\" + relPath;
            }
            if (!relName.Equals(""))
            {
                home = home + "\\" + relName;
            }
            return home;
        }

        /**
         * Returns ResourcePaths format string         
        public String toString()
        {
            return StringUtils.mapString(this, autoHome, workHome, runHome);
        }*/

    }
}
