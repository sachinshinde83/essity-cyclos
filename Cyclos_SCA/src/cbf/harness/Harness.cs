﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

/**
 * 
 * Main class for Running Engine from Harness. Defines the runTestCase()
 * function.
 * 
 */
using System.Collections.Generic;
using System;
using src.cbf.model;
using src.cbf.engine;
using src.cbf.utils;
using src.cbf.plugin;
using System.IO;

namespace src.cbf.harness
{
    public class Harness
    {
        public static ResourcePaths resourcePaths;
        public static Configuration GCONFIG;
        public static IAppDriver appDriver;
        
        /**
         * Initializes logs, reporters in the results folder
         * 
         * @param runMap
         * @param runName
         */
        public Harness(Dictionary<String, dynamic> runMap, String runName)            
        {
            if (runMap != null)
            {
                LoadConfiguration(runMap["configFilePath"]);

                logger = new LogUtils(this);
                logger.Trace(runMap["configFilePath"], runName);

                /**substitute(runMap);*/
                InitializeResourcePaths();
                MakeReporter();
                LoadAppDriver();
                LoadEngine();
            }
            
        }

        

        /**public void RunTestCase()  */
        /**{ */

        /**} */


        /**
         * Alternate constructor. runName defaults to 'Dummy'
         * 
         * @param runMap
         
        public Harness(Dictionary<string, string> runMap)
        {
            this(runMap, "Dummy");
        }

        /**
         * Executes test and returns result
         * 
         * @param instanceName
         *            name of Instance
         * @return Result message
         */
        public TestResult RunTest(ITcMaker tcMaker, string instanceName)
        {
            return engine.RunTestCase(tcMaker, instanceName);
        }

        /**
         * Finalize method to call garbage collector
         */
        public void Finalization()
        {
            reportingManager.Close();
            appDriver.Recover(); // TODO : remove it from here
        }

        /**
         * adds reporter to the tracker report list
         */
        public void AddTracker(TestResultTracker.Observer reporter)
        {
            tracker.AddReporter(reporter);
        }

        public static void LoadAppDriver()
        {
            appDriver = new AppLoader().LoadApp(GCONFIG);
            
        }

        private void LoadEngine()
        {
            try
            {
                tracker = new TestResultTracker(reportingManager.Reporters());
                engine = new Engine(appDriver, tracker);
            }
            catch (Exception e)
            {
                logger.HandleError("Failed to load engine: ", e);
                throw;
            }
        }

        private void InitializeLogs(String resultsFolder)
        {
            logger.Trace("InitializeLogs(" + resultsFolder + ")");
            /**logger.initialize();*/
        }

        private void MakeReporter()
        {
            /** Add desired Reporters using ManageReporters*/
            Object reporterObj = GCONFIG.Get("ResultReporter");

            reportingManager = new ReportingManager(reporterObj, GCONFIG);
        }

        private static void LoadConfiguration(String configFileName)
        {
            
                GCONFIG = new Configuration(configFileName);              
                        
        }

        /**private void substitute(Dictionary<String, String> runMap)
        {
            GCONFIG.set(new Substitutor(runMap).substitute(GCONFIG
                    .getAllProperties()));
            // TODO : Substitute Map is returning as a map while GCONFIG is used
            // everywhere that is an object of Configuration class.
        }*/

        private void InitializeResourcePaths()
        {
                        
            string currentFilePath = new System.Diagnostics.StackTrace(true).GetFrame(0).GetFileName();
            String workHome = Directory.GetParent(Directory.GetParent(Directory.GetParent(Directory.GetParent(currentFilePath).ToString()).ToString()).ToString()).ToString();
            if (workHome == null || !(FileUtils.MakeFolder(workHome)))
            {
                logger.HandleError("Can't access work home folder: ", workHome);
            }

            
            String autoHome = workHome + "\\" + (String)Harness.GCONFIG.Get("AutoHome");                                     
            logger.Trace("AutoHome:", autoHome);
            if (autoHome == null || autoHome.Equals(""))
            {
                logger.HandleError("AUTO_HOME is invalid:", autoHome);
                return;
            }
            /** TODO: handle invalid folder. Defaulting possibilities*/
                        

            String runHome = (String)GCONFIG.Get("RunHome");

            /** TODO : Make folder failing while creating the nested folders.*/

            if (runHome == null || !(FileUtils.MakeFolder(runHome)))
            {
                logger.HandleError("Can't create/access run home folder: ", runHome);

            }
            
            resourcePaths = ResourcePaths.GetInstance(autoHome, workHome, runHome);

            InitializeLogs(runHome);
        }
        
        readonly private LogUtils logger; 
        private Engine engine;
        private ReportingManager reportingManager;
        private TestResultTracker tracker;
    }
}
