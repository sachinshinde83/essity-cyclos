﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using src.cbf.utils;
using src.cbf.model;
using src.cbf.plugin;

namespace src.cbf.harness
{
    public class TestSetAccess
    {
        static LogUtils Logger;

        public TestSetAccess() 
        {
            Logger = new LogUtils(this);
	    }

        /**
         * Instantiates TestSet
         * 
         * @return TestSet object
         */
        public static ITestSet Instantiate()        
        {
		try {
			Dictionary<String, Object> testSetAccessMap = (Dictionary<String, Object>) Harness.GCONFIG.Get("TestSetAccess");
			Dictionary<String, Object> param = new Dictionary<String, Object>();

            if (testSetAccessMap["parameters"] != null)
            {
                param = (Dictionary<String, Object>)testSetAccessMap["parameters"];
            }				

			return ((ITestSet) PluginManager.GetPlugin((String) testSetAccessMap["plugin"], param));

		}
        catch (ObjectDisposedException e)
        {
            Logger.HandleError("Value for 'TestSetAccess' is not proper in user config file ", e);
        }

		return null;

	}

        
    }
    
}
