﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using Selenium;

using src.cbf.model;
using src.ModuleDrivers;
using src.cbf.utils;
using src.cbf.plugin;

namespace src.cbf.harness
{
    public class AppLoader
    {
        readonly LogUtils logger;

        public AppLoader()
        {            
            logger = new LogUtils(this);
        }

        /**
         * Loads application driver and returns it's instance
         * 
         * @return object of AppDriver
         */
        public IAppDriver LoadApp(Configuration CONFIG) 
        {

        IAppDriver driver = null;
        /**Dictionary<string, IWebDriver> param = new Dictionary<string, IWebDriver>();
        driver = new CompositeAppDriver(param);*/

            logger.Trace("LoadApp()");
            String driverPlugin = null;
            try
            {
                Dictionary<String, dynamic> param = new Dictionary<String, dynamic>();
                if (CONFIG!= null )
                {
                    param.Add("UIDrivers", CONFIG.Get("UIDrivers"));
                    /**
                     * param.put("Browser", CONFIG.get("Browser")); param
                     * .put("BrowserDriverFolder", CONFIG .get("BrowserDriverFolder"));
                     */

                    Dictionary<String, Object> map = (Dictionary<String, Object>)CONFIG.Get("AppDriver");

                    driverPlugin = (String)map["plugin"];
                    logger.Debug("AppDriverBootStrap: " + driverPlugin);
                    String className = (String)map["classname"];
                    /** Dictionary<String, Object> param1 = (Dictionary<String, Object>)map["parameters"];
                     if (param1 != null)
                         AddRange(param, param1);
                     param.Add("parameters", param1);*/
                    
                    driver = (IAppDriver)PluginManager.InitializePlugin(className,param);
                }

            }
            catch (Exception e)
            {
                logger.HandleError("Error in loading AppDriver:", driverPlugin, e);
                throw;
            }                    

		return driver;
	}

        

    }

    

}
