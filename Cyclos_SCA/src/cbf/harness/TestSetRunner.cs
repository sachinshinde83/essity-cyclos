﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using src.cbf.harness;
using src.cbf.engine;
using src.cbf.model;
using src.cbf.utils;
using src.cbf.plugin;
using System.Globalization;


namespace src.cbf.harness
{
    public class TestSetRunner:IRunner
    {
        /**
	 * Run the test set from parameters
	 * 
	 * @param runMap
	 *            command line arguments
	 * @param runName
	 * 
	 */
        public static TestSetRunner runner;

        public void Run(Dictionary<String, dynamic> runMap, String runName) 
    {
        /**Harness harness = new Harness(runMap, runName); */
        /**this.harness = harness; */

		runner = new TestSetRunner(this.harness);

		runner.RunTestSet();

		harness.Finalization();
	}
        

	/**
	 * Convenience method
	 */
	public static void Run(Dictionary<String, dynamic> runMap) 
    {
        runner.Run(runMap, "Dummy");
	}

    public TestSetRunner(Dictionary<String, dynamic> param) 
    {
        this.param = param;
        logger = new LogUtils(this);
	}

    public TestSetRunner(Harness hrness)
    {
        this.harness = hrness;
        logger = new LogUtils(this);
    }

    public void SetHarness(Harness hrness)
    {
        this.harness = hrness;
    }

	/**
	 * Executes each instance from the test set
	 */
	public void RunTestSet() 
    {
		ITestSet ts = TestSetAccess.Instantiate();

		int tsCount = ts.TestInstanceCount();
		for (int ix = 0; ix < tsCount; ix++) {
			if (ix > 0) { // delay between 2 test cases
				DelayTestCase();
			}

			Dictionary<String, dynamic> instanceMap = new Dictionary<String, dynamic>();

			instanceMap.Add("folderPath", ts.TestInstance(ix).FolderPath());
			instanceMap.Add("instanceName", ts.TestInstance(ix).InstanceName());

			try 
            {
				RunTestInstance(instanceMap);
                GC.Collect();
                GC.WaitForPendingFinalizers();                 
			}
           
            catch (Exception e)
            {
                logger.HandleError("Unknown error during testinstance execution ", e, " ", ix, " ", instanceMap);
                throw;
            }          
		}
	}

	/**
	 * Triggers execution for the current instance(runName)
	 * 
	 * @param instanceMap
	 *            name of instance
	 * @throws ReflectiveCopyException
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	public TestResult RunTestInstance( Dictionary<String, dynamic> instanceMap)
    {
        String instanceName=null; 
        if (instanceMap != null)
        {
            instanceName = instanceMap["instanceName"];
        }

        TcMaker tcMaker = new TcMaker(instanceMap, instanceName);
		return harness.RunTest(tcMaker, instanceName);
	}

       public class TcMaker:model.ITcMaker
        {
           public TcMaker(Dictionary<String, dynamic> instanceMap, String instanceName)
           {
               this.instanceMap = instanceMap;
               this.instanceName = instanceName;
           }


           readonly Dictionary<String, dynamic> instanceMap;
           readonly String instanceName;           

           public ITestCase Make()
           {
               
               return runner.GetTestCase(instanceMap);				
			}

			public String ToString() 
            {
				return instanceName;
			}
		};

	public ITestCase GetTestCase(Dictionary<String, dynamic> instanceMap)
    {
		logger.Trace("GetTestCase(), instanceMap");
		try 
        {
			return GetTestCaseAccess().GetTestCase(instanceMap);
		}
        catch (Exception e)
        {
			logger.HandleError("Error in building test case from ",
					instanceMap, e);
            throw;
		}
		return null;
	}

	private void DelayTestCase() {
		int interTcDelay;
        interTcDelay = int.Parse((String)Harness.GCONFIG.Get("InterTestCaseDelay"), CultureInfo.InvariantCulture);
        /**interTcDelay = Int32.Parse((String)Harness.GCONFIG.Get("InterTestCaseDelay"));*/
		if (interTcDelay != 0) 
        {
			try
            {
				Thread.Sleep(interTcDelay * 1000);
				logger.Trace("Test case delay:" + interTcDelay);
			} 
            catch (Exception e) 
            {
				logger.HandleError("Exception caught in delay test case : ", e,interTcDelay);
                throw;

			}
		}
	}

    private  ITestCaseAccess GetTestCaseAccess()
    {
        if (testCaseAccess != null)
        {
            return testCaseAccess;
        }
		Dictionary<String, Object> pluginParams = null;
		try {
            pluginParams = (Dictionary<String, Object>)Harness.GCONFIG.Get("TestCaseAccess");
			if (pluginParams == null) 
            {
				logger.HandleError("TestCaseAccess is not configured; cannot build test case");
			}
            
			testCaseAccess = (ITestCaseAccess) PluginManager.GetPlugin(pluginParams);
            
            
		} catch (Exception e)
        {
			logger.HandleError("'TestCaseAccess' is not configured correctly",pluginParams, e);
            throw;
		}

		return testCaseAccess;
	}

	/**
	 * Returns TestSetRunner format string
	 */
	public override String ToString() 
    {
		return StringUtils.MapString(this, harness);
	}

	/* @CHECKME: check if harness can be privatized */
	public new Harness harness;    
	private ITestCaseAccess testCaseAccess;
    readonly private LogUtils logger;
    readonly private Dictionary<String, dynamic> param;

    /**private LogUtils logger = new LogUtils(); */

    }
}
