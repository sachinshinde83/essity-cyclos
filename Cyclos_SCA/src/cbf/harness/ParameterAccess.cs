﻿/**Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using src.cbf.engine;

using src.cbf.model;
using src.cbf.utils;

namespace src.cbf.harness
{

/****
 * 
 * Implements interface TestResultTracker.Reporter and provides access to
 * parameters
 * 
 */
public class ParameterAccess : TestResultTracker.Observer 
{
    readonly private LogUtils logger;
   
    public ParameterAccess()
    {
        logger = new LogUtils(this);
    }
    static Dictionary<TestResult.EntityType, String> entityType2Scope = new Dictionary<TestResult.EntityType, String>() 
    {
        
        {TestResult.EntityType.TestCase, "T"},
        {TestResult.EntityType.Iteration, "I"}
        
    };

    /****
     * Copies parameters
     * 
     * @param inputGroupName
     *            name of input group parameters
     * @param outputGroupName
     *            name of output group parameters
     */

    public void CopyRecentParameters(String inputGroupName,
            String outputGroupName)
    {
        int ix = 0;
        GroupInfo info;
        String[] groupNames = { inputGroupName, outputGroupName };
        foreach (String groupName in groupNames)
        {

            if (groupName == null)
            {
                ix = ix + 1;
                continue;
            }

            /*** FIXME: copy contents group to groups[groupName]*/
            info = sGrp[groupName];
            if (info == null)
            {
                logger.HandleError("Invalid group name: ", groupName
                        + ": Declare group before use.");
                return;

            }

            ix = 0;
            
            Tuple<Dictionary<String, dynamic>, Dictionary<String, dynamic>> d = mostRecentparam[ix];
            Dictionary<dynamic, dynamic> params1 = new Dictionary<dynamic, dynamic>();
            if ((d.Item1.Count > 0))
            {

                List<String> lo = d.Item1.Keys.ToList();
                List<dynamic> val = d.Item1.Values.ToList();
                for (int index = 0; index <= lo.Count - 1; index++)
                {
                    params1.Add(lo[index], val[index]);
                }

            }
            if ((d.Item2.Count > 0))
            {

                List<String> lo = d.Item2.Keys.ToList();
                List<dynamic> val = d.Item2.Values.ToList();
                for (int index = 0; index <= lo.Count - 1; index++)
                {
                    params1.Add(lo[index], val[index]);
                }
            }

            Dictionary<String, dynamic> finalGrp = new Dictionary<string, dynamic>();
            if (info.scope == "G")
            {
                PersistentMap group;
                group = info.values;
                PersistentDataAccess m = group.globalDataAccess;                
                finalGrp.Add("globalDataAccess", m.Getval());
                finalGrp.Add("logger", m.Getval().ElementAt(0).Value);
                finalGrp.Add("objStrUtils", group.objStrUtils);
                finalGrp.Add("superMap", group.Getval());

                if (params1 != null)
                {
                    Dictionary<dynamic, dynamic>.KeyCollection keys = params1.Keys;
                    foreach (dynamic key in keys)
                    {
                        if (finalGrp.ContainsKey(key))
                        {
                            finalGrp.Remove(key);
                            group.Remove(key);

                        }
                        finalGrp.Add(key, params1[key]);
                        group.Put(key, params1[key]);
                        
                    }
                }
                ix = ix + 1;
                groups.Add(groupName, new GroupInfo(info.name, info.scope, group));
                nestedParams.Add(groupName, group);
            }

            else
            {
               
                Dictionary<String, dynamic> group;
                group = info.values;
                if (params1 != null)
                {
               
                    Dictionary<dynamic, dynamic>.KeyCollection keys = params1.Keys;
                    foreach (dynamic key in keys)
                    {
                        if (finalGrp.ContainsKey(key))
                        {
                            finalGrp.Remove(key);

                            group.Remove(key);
                        }
                        finalGrp.Add(key, params1[key]);
                        group.Add(key, params1[key]);
                    }
                }
                ix = ix + 1;
                groups.Add(groupName, new GroupInfo(info.name, info.scope, group));
                nestedParams.Add(groupName, group);
                
            }


        }
    }

    /***
     * Starts execution of TestCase
     * 
     * @param result
     *            Object of TestResult
     */
    public void Start(TestResult result)
    {
        String entype = null;
        if (result != null)
        {
            entype = result.entityType.ToString();

            if (entype.Equals("Testcase") && (result.entityDetails.GetType() == typeof(ITestCase)))
            {
             return;
             
            }

            if (entype.Equals("Iteration") && !entities.ContainsKey(TestResult.EntityType.TestCase.ToString()))
            {
                Start(result.parent);             
            }


            if (!entities.ContainsKey(result.entityType.ToString()))
            {
                entities.Add(result.entityType.ToString(), result.entityDetails);
            }

            /**String scope;
            scope = entityType2Scope.ContainsKey(result.entityType) ? entityType2Scope[result.entityType] : null;*/
        }
    }
    /***
     * Sets Variables scope
     * 
     * @param scope
     *            variables scope
     */
    public void SetGroupsInScope(String scope) {
        Dictionary<String, List<GroupInfo>> temp = new Dictionary<String, List<GroupInfo>>();
        List<GroupInfo> groupListAdd=new  List<GroupInfo>();
        foreach (String key in groups.Keys) 
        {
            GroupInfo info = groups[key];
            if (info.scope.Equals(scope)) 
            {
                if (temp.ContainsKey(scope)) 
                {
                    List<GroupInfo> groupList = temp[scope];
                    groupList.Add(info);
                    temp[scope] = groupList;                    
                } else 
                {                    
                    groupListAdd.Add(info);
                    temp.Add(key, groupListAdd);
                }
            }
        }

    }

    /***
     * Logs the details in logger
     * 
     * @param result
     *            object of TestResult
     * @param rsType
     *            object of ResultType
     * @param details
     *            Map containing details
     */
   
    public void Log(TestResult result, TestResult.ResultType rsType, Object details)
    {
        logger.Trace("ParameterAccess-Log:" + StringUtils.ToString(result) + ":" + StringUtils.ToString(rsType) + ":" + StringUtils.ToString(details));
    }


    /***
     * Finishes TestCase execution
     * 
     * @param result
     *            object of TestResult
     * @param rsType
     *            object of ResultType
     * @param details
     *            object containing details
     */
    public void Finish(TestResult result, src.cbf.engine.TestResult.ResultType rsType, Object details)
    {
        String scope=null;
        if (result != null)
        {
            scope = entityType2Scope.ContainsKey(result.entityType) ? entityType2Scope[result.entityType] : null;

            if (scope != null)
            {
                ResetGroupsInScope(scope);
            }

            switch (result.entityType.ToString())
            {
                case "Testcase":
                    DestroyVariables();
                    break;
                case "Iteration":
                    nestedParams.Remove("INPUT");
                    nestedParams.Remove("OUTPUT");
                    if (nestedParams.ContainsKey("INPUTS"))
                    {
                        nestedParams.Remove("INPUTS");
                        nestedParams.Add("INPUTS", null);
                    }
                    if (nestedParams.ContainsKey("OUTPUTS"))
                    {
                        nestedParams.Remove("OUTPUTS");
                        nestedParams.Add("OUTPUTS", null);
                    }

                    break;
                case "Component":
                    SaveComponentParameters((ITestStep)result.entityDetails, (Dictionary<String, dynamic>)result.miscInfo["input"], (Dictionary<String, dynamic>)result.miscInfo["output"]);
                    break;
                default:
                    logger.Trace("ParameterAccess-Finish:" + StringUtils.ToString(result));
                    break;
            }
            entities.Remove(result.entityType.ToString());
        }
    }

    	private void ResetGroupsInScope(String scope) {
		 Dictionary<String, dynamic>.KeyCollection keys = groups.Keys;
		foreach (String key in keys) {
			GroupInfo info = groups[key];
			if (info.scope.Equals(scope)) {
				info.values.Clear();
			}
		}
	}

        /***
         * Declares TestCase variables
         * 
         * @param testCase
         *            object of TestCase
         */



    public void DeclareVariables(ITestCase testCase)
    
    {
        if (testCase != null && testCase.Variables() == null)
        { 
            return;
        }

        if (testCase != null)
        {
            List<Dictionary<String, dynamic>> variables = testCase.Variables();
            foreach (Dictionary<String, dynamic> variable in variables)
            {
                AddGroup((String)variable["name"], (String)variable["scope"]);
            }
        }
    }



    /***
     * ' Resolve references in an object
     ' Any combo of Dictionary/Array/String etc can be passed as parameter
     * 
     * Resolves map data
     * 
     * @param data
     *            Map containing data
     */
    public Dictionary<String, dynamic> Resolve(Dictionary<String, dynamic> data)
    {
        try
        {
            return new Substitutor(nestedParams).Substitute(data);
        }
        catch (NullReferenceException e)
        {
            logger.HandleError("Exception : ", e);
        }
        catch (Exception e)
        {
            logger.HandleError("Exception : ", e);
            throw;
        }
       
        return data;
    }

    /***
     * Returns entity
     * 
     * @param enType
     *            object of EntityType
     * @return entity object
     */
    public Object GetEntity(src.cbf.engine.TestResult.EntityType enType)
    {
        return entities[enType.ToString()];
    }


    private void DestroyVariables() {
        /** destroy all variables in scope 'T' or 'I'
         Keep G variables*/
        foreach (String key in  groups.Keys) {
            GroupInfo info = groups[key];
            if (!(info.scope.Equals("G"))) {
                info.values.Remove(key);
            }
        }
    }

    private GroupInfo AddGroup(String groupName, String scope)
    {
         GroupInfo info=null;
        if (groups.ContainsKey(groupName))
        {
            logger.HandleError("Duplicate variable names: ", groupName);
            return null;
        }

        if ((scope ?? "" ).Equals(""))
        {
            scope = "I";
        }

       
        Dictionary<String, dynamic> group = null;
        Object grp = null;
        switch (scope.ElementAt(0))
        {
            case 'G':
                String globalDataFolder = ResourcePaths.GetInstance().GetRunResource("Params", "");
                grp = new PersistentMap(groupName, globalDataFolder);
                 info = new GroupInfo(groupName, scope,grp);
                groups.Add(groupName, info);
                nestedParams.Add(groupName, grp);
                break;
            case 'I':
            case 'T':               
                group = new Dictionary<String, dynamic>();
                 info = new GroupInfo(groupName, scope, group);
                 groups.Add(groupName, info);
                 nestedParams.Add(groupName, group);                
                break;
            default:
                logger.HandleError("Invalid scope: " + scope + " : for variable: "
                        + groupName);
                break;
        }
        /** GroupInfo info = new GroupInfo(groupName, scope, group);
         groups.Add(groupName, info);
         nestedParams.Add(groupName, group);// ' make it available for resolution
         nestedParams.Add(groupName, grp);*/
        sGrp = groups;
        return info;
    }


    private void SaveComponentParameters(ITestStep step, Dictionary<String, dynamic> input, Dictionary<String, dynamic> output)
    {
        StringBuilder combinedIn = new StringBuilder();
        StringBuilder combinedOut = new StringBuilder();
        if(nestedParams.ContainsKey("INPUT"))
        {
            nestedParams.Remove("INPUT");
        }
        nestedParams.Add("INPUT", input);
        if (nestedParams.ContainsKey("OUTPUT"))
        {
            nestedParams.Remove("OUTPUT");
        }
        nestedParams.Add("OUTPUT", output);
       /** Dictionary<dynamic, dynamic> ip = new Dictionary<dynamic,dynamic>(){
                  {input,output}
        };
        mostRecentparameters =new Dictionary<dynamic, dynamic>()[1];
        mostRecentparameters[0].Add(input,output);
         mostRecentparameters = new Dictionary<Dictionary<String, dynamic>, Dictionary<String, dynamic>>();*/
        mostRecentparameters = new List<Tuple<Dictionary<String, dynamic>, Dictionary<String, dynamic>>>();
        mostRecentparameters.Add(new Tuple<Dictionary<String, dynamic>, Dictionary<String, dynamic>>( input, output));
        mostRecentparam = mostRecentparameters;
        /** {
         {  input, output }
         };
         Dictionary<String, Object>*/
        if (input.Count >= 1)
        {

            String keyy, vall;
            foreach(KeyValuePair<String,dynamic>temp in input)
            {
                keyy = temp.Key;
                vall =(String) temp.Value;
                combinedIn = combinedIn.Append(keyy + "=" + vall);
                combinedIn = combinedIn.Append(";");
              
            }
            
        }
        if (output.Count >= 1)
        {

            String keyy, vall;
            foreach (KeyValuePair<String, dynamic> temp in output)
            {
                keyy = temp.Key;
                vall = (String)temp.Value;
                combinedOut = combinedOut.Append(  keyy + "=" + vall);
                combinedOut = combinedOut.Append(";");

            }
        }
       
        if (nestedParams.ContainsKey("INPUTS"))
        { 
            nestedParams.Remove("INPUTS");
            nestedParams.Add("INPUTS", step.ModuleCode() + "-" + step.ComponentCode() + "{" + combinedIn + "}");
        }

        if (nestedParams.ContainsKey("OUTPUTS"))
        {
            nestedParams.Remove("OUTPUTS");
            nestedParams.Add("OUTPUTS", step.ModuleCode() + "-" + step.ComponentCode() + "{" + combinedOut + "}");
        }

       /** ((Dictionary<String, dynamic>)nestedParams["INPUTS"]).Add(step.moduleCode() + "-" + step.componentCode(), combined);
        (( Dictionary<String, dynamic>)nestedParams["OUTPUTS"]).Add(step.moduleCode() + "-" + step.componentCode(), output);*/
    }

    /** Information about a variable group
     ([name, scope, values-of-vars-in-the-group])
    private static class GroupInfo*/
    public  class GroupInfo
    {
        public String name; // groupName
        public String scope; // I/T/G      
        public dynamic values;
        public GroupInfo() { }
        
        public GroupInfo(String n, String sc, Object vals)
        {
            name = n;
            scope = sc;
            values = vals;
        }
    }
    public class Substitute : Substitution
    {
       
        public Substitute() { }
       /** public Substitute(ParameterManager manager)
        {
            this.manager = manager;
        }*/
        public String Get(String refv)
        {
            return refv;
        }
        public String GetUniqUtil(String refv)
        {
            dynamic obj = Activator.CreateInstance(typeof(UniqueUtils));
            return obj.uniqueString(refv);
        }
    }

    readonly private Dictionary<String, Object> nestedParams = new Dictionary<String, Object>() {
        
            {"INPUTS", new Dictionary<String, Dictionary<dynamic,dynamic>>()},
            {"OUTPUTS", new Dictionary<String, Dictionary<dynamic,dynamic>>()},
            {"CONFIG", Harness.GCONFIG.FetchAllProp()},
            {"UNIQUE", new ParameterAccess.Substitute()}  
    };



     /**Input/Output used by the most recent component
     Used to resolve references to INPUT/OUTPUT variables*/
   
    private List<Tuple<Dictionary<String, dynamic>, Dictionary<String, dynamic>>> mostRecentparameters;
    private static List<Tuple<Dictionary<String, dynamic>, Dictionary<String, dynamic>>> mostRecentparam;

    readonly private Dictionary<String, dynamic> groups = new Dictionary<String, dynamic>();
    public static Dictionary<String, dynamic> sGrp = new Dictionary<String, dynamic>();
    readonly private Dictionary<String, dynamic> entities = new Dictionary<String, dynamic>();

   /** public static KeyValuePair<object, object> CastFrom(Object obj)
    {
        var type = obj.GetType();
        if (!type.IsGenericType)
        {
            if (type != typeof(KeyValuePair<,>))
            {
                var key = type.GetProperty("Key");
                var value = type.GetProperty("Value");
                var keyObj = key.GetValue(obj, null);
                var valueObj = value.GetValue(obj, null);
                return new KeyValuePair<object, object>(keyObj, valueObj);
            }
            else
                return default(KeyValuePair<object, object>);
        }
        else
            return default(KeyValuePair<object, object>);
    }*/   
}
}
