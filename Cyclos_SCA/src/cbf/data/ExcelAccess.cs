﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using src.cbf.model;
using src.cbf.harness;
using src.cbf.utils;
using System.IO;

namespace src.cbf.data
{
    public class ExcelAccess : IDataAccess
    {
        /**
     * 
     * Constructor to initialize parameters
     * 
     * @param params
     *            map containing parameters
     */
        public ExcelAccess(Dictionary<string, dynamic> param)
        {
            this.param = param;
        }

        /**
         * Returns row data for the selected rows
         * 
         * @param moduleCode
         *            contains moduleCode value
         * @param componentCode
         *            contains componentCode value
         * @param rowSelector
         *            contains row selection value
         * @return list of Map
         */
        public List<Dictionary<string, dynamic>> SelectRows(String moduleCode, String componentCode, String rowSelector)
        {

             /**		logger.trace("SelectRows(" + moduleCode + "-" + componentCode + "-" + rowSelector + ")"); */

            List<Dictionary<string, dynamic>> data = null;
            String filePath = GetFilePath(moduleCode, componentCode);          

            DTAccess dtAccess = new DTAccess(filePath);

             /** CHECKME: sheetExists call looks dubious */
            if (cbf.utils.ExcelAccess.IsSheetExists(filePath, componentCode))
            {
                data = dtAccess.ReadSelectedRows(componentCode, rowSelector);
            }         

            return data;
        }

        private String GetFilePath(String moduleCode, String componentCode)
        {
            String filePath = null;
            if (Utils.Stringtoobl((String)ResourcePaths.GetInstance().GetSuiteResource("Plan", "")))
            {
                filePath = TryFilePath(moduleCode, componentCode + ".xls");

            }
            if (filePath == null)
            {
                filePath = TryFilePath(moduleCode, componentCode + ".xlsx");
            }
            if (filePath == null)
            {
                filePath = TryFilePath("", moduleCode + "Data.xls");
            }
            if (filePath == null)
            {
                filePath = TryFilePath("", moduleCode + "Data.xlsx");
            }            
            return filePath;
        }

        private String TryFilePath(String branchPath, String fileName)
        {
             /**logger.trace("TryFilePath(" + branchPath + "-" + fileName + ")"); */
            String filePath = (String)param["folderpath"];
            if (filePath.Equals(""))
            {
                String folderPath = "Plan/Data";
                if (!(branchPath.Equals("")))
                {
                    folderPath = folderPath + "/" + branchPath;
                }
                
                filePath = resourcePaths.GetSuiteResource(folderPath, fileName);
                if ((filePath == null) || !(new FileInfo(filePath).Exists))
                {
                     /** logger.trace("Data file " + fileName + " does not exists. ", filePath, folderPath); */
                    return null;
                }
            }
             /** logger.trace("TryFilePath(" + branchPath + "-" + fileName + ")=" + filePath); */
            return filePath;
        }

        /**
         * Returns ExcelAccess format string
         */
         /**	public String toString() {
        		return StringUtils.mapString(this, param);
        	} */

         /** private LogUtils logger = new LogUtils(this); */
        readonly private ResourcePaths resourcePaths = ResourcePaths.GetInstance();
        readonly private Dictionary<string, dynamic> param;

    }
}
