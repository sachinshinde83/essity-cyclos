﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using src.cbf.model;
using src.cbf.harness;
using src.cbf.utils;

namespace src.cbf.data
{
    public class DbAccess : IDataAccess
    {
        readonly private Dictionary<string, dynamic> param;
        readonly private DBUtils dbUtils;
        readonly private LogUtils logger;
        public DbAccess(Dictionary<string, dynamic> param)
        {
            this.param = param;
            dbUtils = new DBUtils(param);
            logger = new LogUtils(this);
        }

        public List<Dictionary<string, dynamic>> SelectRows(String moduleCode, String componentCode,String rowSelector)
        {
            logger.Trace("SelectRows(" + componentCode + "-" + rowSelector + ")");
            String tableName = moduleCode + "__" + componentCode;
            List<Object> rwSelect=new List<Object> ();
            rwSelect.Add(rowSelector);

            try
            {
                List<Dictionary<string, dynamic>> list = dbUtils.RunQuery("Select * from " + tableName + " where [_rowId]=" + "@rowVal" + ";", rwSelect);
                return list;
            }
            catch (IndexOutOfRangeException e)
            {
                if (!dbUtils.CheckExists(tableName))
                {
                    logger.HandleError("Table " + tableName+ " does not exist for component: - " + componentCode);
                    return null;
                }

                logger.HandleError("Error in executing query : ", e, rowSelector,moduleCode, componentCode);
                return null;
            }
        }


        protected void Ffinalize()
        {
            dbUtils.Disconnect();
        }

        public String TooString() 
        {
		    return StringUtils.MapString(this, param);
	    }
    }

    	
}
