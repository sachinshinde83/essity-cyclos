﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using log4net;

using src.cbf.harness;

namespace src.cbf.utils
{

    /**
     * 
     * Utility provides functionalities related to logging and error handling Levels
     * of logging: debug|trace|details|warning|error
     * 
     * Logging utilities accept varargs of objects. StringUtils is used to
     * stringify.
     * 
     * handleError if first vararg (after text msg) is an Exception, it is treated
     * as root cause
     */
    public class LogUtils
    {
        public static Boolean TCFail;  //Temp
        readonly ILog log;
        /**ILog log1; */
        private readonly Object owner;        

	/**
	 * Overloaded constructor to initialize owner
	 * 
	 * @param owner
	 *            value of owner
	 */
        

	public LogUtils(Object owner)
    {
        this.owner = owner;
        /**log4net.Config.XmlConfigurator.Configure(); */
        /**log4net.Config.XmlConfigurator.Configure(new Uri("E:\\Sunil\\Automation_CoE\\CBF-C#\\CBF_CSharp\\CBF_CSharp\\Test\\Sample1\\Plan\\Log4net.config"));         */
        String RunHome = (String)Harness.GCONFIG.Get("RunHome");
        
        String traceFilePath=RunHome + "\\Trace.csv";
        String errorFilePath = RunHome + "\\Error.csv";        

        log4net.GlobalContext.Properties["TraceLogFilePath"] = @traceFilePath;
        log4net.GlobalContext.Properties["ErrorLogFilePath"] = @errorFilePath;

        string currentFilePath = new System.Diagnostics.StackTrace(true).GetFrame(0).GetFileName();
        String workHome = Directory.GetParent(Directory.GetParent(Directory.GetParent(Directory.GetParent(currentFilePath).ToString()).ToString()).ToString()).ToString();

        /**String log4netConfig = workHome + "\\" + (String)Harness.GCONFIG.get("AutoHome") + "\\Plan\\Log4net.config"; */
        String log4netConfig = workHome + "\\Resources\\Log4net.config";
        
        /**String log4netConfig = Directory.GetParent(Directory.GetParent(Environment.CurrentDirectory).ToString()) + "\\" + (String)Harness.GCONFIG.get("AutoHome") + "\\Plan\\Log4net.config";  */

        log4net.Config.XmlConfigurator.Configure(new Uri(@log4netConfig));
        if (owner != null)
        {
            log = log4net.LogManager.GetLogger(owner.ToString());
        }
        
        /**ILog log = log4net.LogManager.GetLogger(typeof(log4netLogger));        */

	}

	

	
	/**
	 * Default constructor
	 */
	public LogUtils() 
    {
           
	}

	/**
	 * Logs arguments as debug
	 * 
	 * @param varargs
	 *            object of arguments
	 */
	public void Debug(Object varargs) 
    {
        log.Debug(StringUtils.ToString(owner) + DELIM + ToString(varargs));
	}

	/**
	 * Logs arguments as trace
	 * 
	 * @param varargs
	 *            object of arguments
	 */
	public void Trace(params dynamic[] varargs ) 
    {
		log.Info(StringUtils.ToString(owner) + DELIM + ToString(varargs));
	}

	/**
	 * Logs arguments as detail
	 * 
	 * @param varargs
	 *            object of arguments
	 */
    public void Detail(params dynamic[] varargs) 
    {
        log.Info(StringUtils.ToString(owner) + DELIM + ToString(varargs));
	}

	/**
	 * Logs arguments as warning
	 * 
	 * @param varargs
	 *            object of arguments
	 */
    public void Warning(params dynamic[] varargs) 
    {
		log.Warn(StringUtils.ToString(owner) + DELIM + ToString(varargs));       
        
	}

	/**
	 * Logs arguments as error
	 * 
	 * @param varargs
	 *            object of arguments
	 */
    public void Error(params dynamic[] varargs) 
    {
        LogUtils.TCFail = true;     
        log.Error(StringUtils.ToString(owner) + DELIM + ToString(varargs));
	}

	/**
	 * Handles error and logs arguments
	 * 
	 * @param msg
	 *            text message for exception
	 * @param varargs
	 *            object of arguments
	 */
	public void HandleError(String msg, params dynamic[] varargs) 
    {
        LogUtils.TCFail = true;     //Temp

        String text = "Error: " + msg + DELIM + ToString(varargs);
		Error(text);
		
	}

	/**
	 * TODO: public void handleError(String msg, Exception exception, Object...
	 * varargs) Useful in highlighting root cause
	 */

	/**
	 * Rethrow an exception after due logging
	 * 
	 * @param msg
	 *            text message for exception
	 * @param exception
	 *            object of FrameworkException
	 */
	public void HandleError(String msg, Exception exception) {
		Error("Error: " + msg + DELIM + "Re-throwing exception");
		throw exception;
	}

	private static String DELIM = "|";

	private static String ToString(dynamic varargs) {
        StringBuilder sb = new StringBuilder();	
		foreach (Object obj in varargs) {
            sb.Append(StringUtils.ToString(obj));		
		}
            String s =sb.ToString();
		return s;
	}

	/**
	 * Returns Log Utils format string
	 * 
	 */
	public String ToString()
    {
		return StringUtils.MapString(this,"log");

	}

    }


    
}
