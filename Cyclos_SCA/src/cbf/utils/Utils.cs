/**Copyright � 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Management;
namespace src.cbf.utils
{
    public class Utils
    {
        public static bool Stringtoobl(string value)
        {
            bool result = false;
            if (StringUtils.Match(value, StringUtils.Pattern("(Pass|True|Yes|Y|true)", true)))
            {
                result = true;
            }
            if (StringUtils.Match(value, StringUtils.Pattern("(Fail|False|No|N|false)", true)))
            {
                result = false;
            }
            return result;
        }




        /**
    * Check the running process and kill it
    * 
    * @param serviceName
    *            Give name of the process that you want to kill
    * @return Boolean
    * @throws IOException
    */


        public static void KillProcess(String serviceName)
        {
            if(serviceName==null)
            {
                throw new ArgumentNullException();
            }

            /**foreach (System.Diagnostics.Process myProc in System.Diagnostics.Process.GetProcessesByName(serviceName)) */
            /** { */
            /**  if (myProc.ProcessName == serviceName) */
            /**  { */
            /**     myProc.Kill(); */
            /**  } */
            /**  } */

            foreach (System.Diagnostics.Process myProc in System.Diagnostics.Process.GetProcesses())
            {
                if (myProc.ProcessName.ToUpper(CultureInfo.InvariantCulture).Trim() == serviceName.ToUpper(CultureInfo.InvariantCulture).Trim())
                {
                    int procId= myProc.Id;
                    String currUserName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                    if (currUserName == GetProcessOwner(procId))
                    {
                        myProc.Kill();
                    }

                }
            }

            
        }

        public static string GetProcessOwner(int processID)
        {
            string query = "Select * from Win32_Process Where ProcessID = \"" + processID + "\"";
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(query);

            ManagementObjectCollection processList = searcher.Get();

            foreach (ManagementObject obj in processList)
            {
                string[] argList = new [] { string.Empty, string.Empty };
                int returnVal = Convert.ToInt32(obj.InvokeMethod("GetOwner", argList), CultureInfo.InvariantCulture);
                if (returnVal == 0)
                {
                    /** return DOMAIN\user */
                    string owner = argList[1] + "\\" + argList[0];                    
                    return owner;
                }
            }

            return "NO OWNER";
        }



        /**
     * Converts passed object array to map
     * 
     * @param array
     *            of object to be converted
     * @return Map of objects
     */
        /**public static Dictionary<Object, Object> toMap(Object[] array)//Added by Avneesh */
        /**{ */
        /**    Dictionary<Object, Object> map = new Dictionary<Object, Object>(); */
        /**    for (int i = 0; i < array.Length; i = i + 2) */
        /**    { */
        /**        map.Add(array[i], array[i + 1]); */
        /**    } */
        /**    return map; */
        /**} */

        /**
	 * Converts passed map to DataRow object
	 * 
	 * @param map
	 *            to be converted
	 * @return object of DataRow
	 */
        public static Dictionary<String, dynamic> Map2DataRow(Dictionary<String, dynamic> map)
        {
            return new Dictionary<String, dynamic>(map);
        }


    }
}
