﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.CSharp.RuntimeBinder;

namespace src.cbf.utils
{
    public class StringUtils
    {
        private static LogUtils logger;
        public StringUtils()
            {           
                logger = new LogUtils(this);
                
            }


        /**
         * Converts passed object to string format
         * 
         * @param o
         *            Object to be converted
         * @return String form of object
         */
        public static String ToString(dynamic o)
        {
            try
            {
                if (o == null)
                {
                    return "<null>";
                }

                if (o.GetType().Name == "TestResultTracker")
                {
                    return MapToString((Dictionary<string, dynamic>)o);
                }
                if (o.GetType().Name == "Dictionary`2")
                {
                    return MapToString((Dictionary<string, dynamic>)o);
                }
                if (o.GetType().Name == "Array")
                {
                    return ArrayToString((string[])o);
                    /**o.Aggregate((current, next) => current + ", " + next) */
                    /** return string.Join(", ", o); */
                }

                else
                {
                    return o.ToString();
                }

            }
            catch (RuntimeBinderException rbe)
            {
                //logger.Trace(rbe.Message);
            }
            catch (Exception e)
            {
                logger.Trace(e.Message);

            }


            /**try */
            /**{ */
            /**    return exceptionToString((Exception)o); */
            /**} */
            /**catch (Exception) */
            /**{ */
            /**} */



            return "<ObjectWithoutStringForm>";

            /**return "<ObjectWithoutStringForm>";   */
        }

        /**
         * Converts given map to String format
         * 
         * @param o
         *            Map to be converted
         * @return String form of Map
         */
        public static String MapToString(Dictionary<string, dynamic> o)
        {
            String resultStr = "";
            StringBuilder sb = new StringBuilder();
            if (o != null && o.Count >= 1)
            {
                
                    String delim = "";
                    foreach (KeyValuePair<string, dynamic> entry in o)
                    {
                        sb.Append(resultStr).Append(delim).Append(entry.Key).Append("=").Append(entry.Value);
                        delim = " , ";
                    }

                    return "{ " + sb + " }";
                
            }
            return "";
        }

        /**
         * Converts given object ellipsis to string form
         * 
         * @param varargs
         *            ellipsis of Objects
         * @return String form of String array
         */
        public static String ArrayToString(String[] varargs)
        {
            String conCatValue = "";
            StringBuilder sb = new StringBuilder();
            if (varargs != null)
            {
                foreach (String input in varargs)
                {
                    sb.Append(conCatValue).Append(input);                  
              }
            }
            return sb.ToString();
        }

        /**
         * Converts passed exception value to string form
         * 
         * @param exception
         *            exception need to be converted
         * @return String form of exception passed
         */

        /**public static String exceptionToString(Exception exception) */
        /**{ */
        /**    return exception.ToString(); */
        /**} */

        public static String ExceptionToString(Exception exception)
        {
            if(exception==null)
            {
                throw new ArgumentNullException();
            }
            return exception.ToString();
        }

       

        /**
         * Compiles string and returns Pattern
         * 
         * @param patternStr
         *            string to be compiled
         * @param isExactMatch
         *            boolean value for exact match
         * @param bIgnoreCase
         *            boolean value for ignore case
         * @return object of Pattern
         */
        public static Regex Pattern(String patternStr, Boolean bIgnoreCase)
        {

            if (bIgnoreCase)
            {
                return new Regex(patternStr, RegexOptions.IgnoreCase);
            }
            else
            {
                return new Regex(patternStr);
            }


        }

        public static String MapString(Object o, params dynamic[] varargs)
        {
            if (o != null)
            {

                return o.GetType().Name + "{ " + ToString(varargs) + " }";
            }
            return string.Empty;

        }


        /**
         * Matches passed value with Pattern and returns boolean result
         * 
         * @param valueToBeMatched
         *            string to be matched
         * @param pattern
         *            pattern with which string needed to matched
         * @return boolean result
         */
        public static Boolean Match(String valueToBeMatched, Regex pattern)
        {
            if(pattern==null)
            {
                throw new ArgumentNullException();
            }
            if (valueToBeMatched != null)
            {
                return pattern.Match(valueToBeMatched).Success;
            }
            return false;

        }

    }
}
