﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.Globalization;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace src.cbf.utils
{

    public class PersistentMap 

    {
        public LogUtils logger;
        public StringUtils objStrUtils;
        public PersistentDataAccess globalDataAccess;
        public Dictionary<String, dynamic> superMap = new Dictionary<String, dynamic>();
        public PersistentMap() { }

        public PersistentMap(String groupName, String globalDataFolder)
        {
            logger = new LogUtils(this);

            if (!FileUtils.MakeFolder(globalDataFolder))
            {
                logger.HandleError("Can't create/access global data folder: ", globalDataFolder);
            }

            globalDataAccess = new PersistentDataAccess(globalDataFolder, groupName);

            /** Retrieve list of latest values */
            this.PutAll(globalDataAccess.Retrieve());
        }
        public Dictionary<String, dynamic> Getval()
        {
            return superMap;
        }
        /**
         * Adds key, value pair to global map
         * 
         * @param key
         *            unique key for map
         * @param value
         *            value for that key
         * @return String value
         */


        public String Put(String key, String value)
        {

            /**super.put(key, value); */
            if (superMap.ContainsKey(key))
            {
                superMap.Remove(key);
            }
            superMap.Add(key, value);
            globalDataAccess.Save(superMap);

            return value;
        }

        /**  public Dictionary<String, String> getElements(PersistentMap obj)
          {
             dynamic d= obj.objStrUtils;

          }*/

        /**
         * Adds complete map to global map
         * 
         * @param m
         *            Map to be added
         */
        public void PutAll(Dictionary<String, String> m)
        {
            /**Utils.AddRange(superMap, m);             */
            globalDataAccess.Save(superMap.Keys.Union(m.Keys).ToDictionary(k => k, k => m.ContainsKey(k) ? m[k] : superMap[k]));
        }

        /**
         * Removes specified key from global map
         * 
         * @param key
         *            to be removed
         * @return value linked to that particular key
         */
        public String Remove(Object key)
        {
            String k = (String)key;
            String value = superMap[k];
            superMap.Remove(k);
            globalDataAccess.Save(superMap);
            return value;
        }

        /**
         * Overriding toString() method and returning PersistentMap format string
         */
        public String ToString()
        {
            return "PersistentMap()";
        }
    }
    public class PersistentDataAccess
    {
        public LogUtils logger;
        public String fileNameNew;

        public PersistentDataAccess(String folderPath, String dataName)
        {
            logger = new LogUtils(this);

            String fileName = folderPath + "/" + dataName + ".dat";
            this.fileNameNew = fileName;
            if (!new FileInfo(fileName).Exists)
            {
                try
                {
                    FileStream read = File.Create(fileName);
                    read.Close();
                }
                catch (System.IO.IOException e)
                {
                    logger.HandleError("Can't create persistent data file ", fileName, e);
                }
            }

        }

        public Dictionary<String, dynamic> Getval()
        {
            Dictionary<String, dynamic> dic = new Dictionary<String, dynamic>();
            dic.Add("logger", logger);
            dic.Add("fileName", fileNameNew);
            return dic;
        }

        public void FileNameone(String fileName)
        {
            this.fileNameNew = fileName;
        }

        public void Save(Dictionary<String, dynamic> values)
        {

            try
            {
                Save(fileNameNew, values);
            }
            catch (IOException e)
            {
                logger.HandleError("Save data failed: ", fileNameNew, values, e);
            }

        }

        private void Save(String fileName, Dictionary<String, dynamic> values)
        {

            StreamWriter sw = File.AppendText(fileName);

            foreach (var item in values)
            {
                sw.WriteLine(item.Key + " = " + (item.Value));
            }
            sw.Close();

        }



        public Dictionary<String, String> Retrieve()
        {

            try
            {
                return Retrieve(fileNameNew);
            }
            catch (NullReferenceException nre)
            {
                logger.HandleError("Retrieve data failed: ", fileNameNew, nre);
            }
            
            return null;
        }

        private Dictionary<String, String> Retrieve(String fileName)
        {
            Dictionary<String, String> values = new Dictionary<String, String>();
            if (!new FileInfo(fileName).Exists)
            {
                return values;
            }

            StreamReader reader = File.OpenText(fileName);
            StringBuilder sb = new StringBuilder();

            String text;
            do
            {
                text = reader.ReadLine();
                if (!string.IsNullOrEmpty(text))
                {
                    text = text.Trim();
                    
                    int separatorIndex = text.Contains("=") ? text.IndexOf("=", StringComparison.OrdinalIgnoreCase) : 0;
                    if (separatorIndex != 0)
                    {
                        int index = separatorIndex + 2;
                        String s ;
                        while (index < text.Length)
                        {
                            sb.Append(text.ElementAt(index));
                            index++;
                        }
                        s = sb.ToString();
                        /**values.Add(text.Substring(0, separatorIndex - 1), text.Substring((separatorIndex + 2), (text.Length - 1))); */
                        if (!values.ContainsKey(text.Substring(0, separatorIndex - 1)))
                        {
                            values.Add(text.Substring(0, separatorIndex - 1), s);
                        }
                            
                    }
                }
            } while (text != null);

            reader.Close();
            return values;
        }


        /**public override String toString()  */
        /**{ */
        /**    //return StringUtils.mapString(this, fileName); */
        /**} */
    }

    /**private PersistentDataAccess globalDataAccess;	 */

}
