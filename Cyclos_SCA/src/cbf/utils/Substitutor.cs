﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Collections;

using src.cbf.model;
using src.cbf.harness;

using src.cbf.utils;
using src.cbf.engine;

namespace src.cbf.utils
{
    public interface Substitution
    {
        /**
         * Retrieves string for passed reference
         * 
         * @param refv
         *            for which value is required
         * @return String value for particular reference
         */
        String Get(String refv);
        String GetUniqUtil(String refv);

    }


    public class Substitutor
    {
        readonly private LogUtils logger;

        public class NestableMap : Substitution
        {
            public String GetUniqUtil(String refv) { return ""; }
            private readonly Dictionary<String, dynamic> map;

            public String refv2;
            public NestableMap(Dictionary<String, dynamic> m)
            {
                this.map = m;
            }

            public void Add(String key, dynamic value)
            {
                if (map.ContainsKey(key))
                {
                    map.Remove(key);
                }
                else
                {
                    map.Add(key, value);
                }

            }

            public String Get(String refv)
            {
                if (refv == null)
                {
                    throw new ArgumentNullException();
                }
                Object o;
                o = null;
                CompareInfo ci = CultureInfo.CurrentCulture.CompareInfo;
                if (map.ContainsKey(refv))
                {
                    o = map[refv];
                }

                if (o != null)
                {

                    return (String)o; // classcastexception handling

                }
                if (!string.IsNullOrEmpty(refv) && refv.Contains("UniqueUtils"))
                {
                    return UniqueUtils.UniqueDate();
                }
                int dotIx = ci.IndexOf(refv, ".", CompareOptions.None);
                if (dotIx < 0)
                {
                    return string.Empty;
                }

                String refvone = refv.Substring(0, dotIx).Trim();
                o = map[refvone];
                if (refvone == null)
                {
                    return null;
                }

                String refvtwo = refv.Substring(dotIx + 1).Trim();
                src.cbf.utils.PersistentMap t = o as src.cbf.utils.PersistentMap;
                if (o.GetType() == typeof(PersistentMap))
                {
                    Dictionary<String, dynamic> finalGrp = new Dictionary<string, dynamic>();

                    finalGrp.Add("globalDataAccess", (t).globalDataAccess);
                    finalGrp.Add("logger", (t).logger);
                    finalGrp.Add("objStrUtils", (t).objStrUtils);
                    finalGrp.Add("superMap", (t).superMap);
                    if (finalGrp.ContainsKey(refvtwo))
                    {
                        finalGrp.Remove(refvtwo);
                    }
                    finalGrp.Add(refvtwo, (t).superMap[refvtwo]);
                    return (new NestableMap(finalGrp)).Get(refvtwo);
                }

                if (o.GetType() == typeof(src.cbf.harness.ParameterAccess.Substitute))
                {
                    return ((src.cbf.harness.ParameterAccess.Substitute)o).Get(refvtwo);
                }

                if (o.GetType() == typeof(ParameterManager.Substitute))
                {
                    return ((ParameterManager.Substitute)o).Get(refvtwo);
                }


                if (o.GetType() == typeof(Dictionary<string, dynamic>))
                {
                    return (new NestableMap((Dictionary<String, dynamic>)o)).Get(refvtwo);

                }

                return null;
            }
        };

        public Substitution substitution;

        /**
         * Constructor to initialize Substitution object
         * 
         * @param substitution
         *            object of Substitution
         */
        public Substitutor(Substitution substitution)
        {
            this.substitution = substitution;
        }

        /**
         * Overloaded constructor to initialize Map of substitution objects
         * 
         * @param substitution
         *            Map of substitution objects
         */
        public Substitutor(Dictionary<String, dynamic> substitution)
        {
            this.substitution = new NestableMap(substitution);
        }

        /**
         * Substitutes given string with references values
         * 
         * @param s
         *            string to be substituted
         * @return new string with substitution
         */
        public String Substitute(String s)
        {
            Regex rx = new Regex(@"\$\{([^}]+)\}");
            return rx.Replace(s, new MatchEvaluator(Replace));

        }

        public String Replace(Match m)
        {
            GroupCollection groupCollection = null;
            if (m != null)
            {
                groupCollection = m.Groups;
            }
            String refv = groupCollection[1].ToString();
            return substitution.Get(refv);
        }

        public object Substitute(Object o)
        {            
            if (o == null)
            {
                return null;
            }

            try
            {
                return Substitute((String)o);
            }
            catch (ObjectDisposedException e)
            {
                logger.HandleError("Error" + e.Message);
            }

            try
            {
                return Substitute((Dictionary<String, String>)o);
            }
            catch (ObjectDisposedException e)
            {
                logger.HandleError("Error" + e.Message);
            }

            try
            {
                return Substitute((List<dynamic>)o);
            }
            catch (ObjectDisposedException e)
            {
                logger.HandleError("Error" + e.Message);
            }

            return o;
        }

        /**
         * Substitutes given map for references
         * 
         * @param map
         *            to be substituted
         * @return Substituted map
         */
        public Dictionary<String, dynamic> Substitute(Dictionary<String, dynamic> map)
        {
            Dictionary<String, dynamic> substitutedMap = new Dictionary<String, dynamic>();
            if (map != null)
            {
                var keys = map.Keys.ToArray();
                foreach (String key in keys)
                {
                    Object value = map[key];
                    substitutedMap.Add(key, Substitute(value));
                }
            }
            return substitutedMap;
        }

        /**
         * Substitutes given list for references
         * 
         * @param list
         *            to be substituted
         * @return substituted list
         */
        public List<Dictionary<String, dynamic>> Substitute(List<Dictionary<String, dynamic>> list)
        {
            List<Dictionary<String, dynamic>> substitutedList = new List<Dictionary<String, dynamic>>();

            if (list != null)
            {
                for (int ix = 0; ix < list.Count(); ix++)
                {

                    dynamic temp = list[ix];
                    if (temp.GetType() == typeof(Dictionary<String, dynamic>))
                    {
                        temp = Substitute((Dictionary<String, dynamic>)temp);
                    }
                    if (temp.GetType() == typeof(String))
                    {
                        temp = Substitute((String)temp);
                    }

                    substitutedList.Add(temp);
                }
            }
            return substitutedList;

        }

        /**
         * Returns Substituter format string
         * 
         */
        public String ToString()
        {
            return StringUtils.MapString(this, substitution);

        }
    }

}
