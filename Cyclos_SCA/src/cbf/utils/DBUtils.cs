﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using src.cbf.utils;
using System.Data;
namespace src.cbf.utils
{
    class DBUtils
    {
        private SqlConnection connection;
        private readonly LogUtils logger;
        public DBUtils(Dictionary<string,dynamic> param) 
        {
            logger = new LogUtils(this);
            connection = GetConnection(param);
	    }

        public Boolean CheckExists(String tableName) {
		Boolean result = false;
        try
        {

            DataTable dbTables = this.connection.GetSchema("Tables");
            foreach (System.Data.DataRow row in dbTables.Rows)
            {
                String tname = row[2].ToString();
                if (tname.Equals(tableName))
                {
                    result = true;
                }
            }
            return result;

        }
        catch (NullReferenceException ne)
        {
            logger.HandleError("Exception caught while accessing database for", tableName, ne);
            return result;
        }
            
    }

        public void Disconnect() 
        {
            if (connection == null)
            {
                return;
            }
            try
            {
                logger.Trace("disconnecting");
                connection.Close();
            }
            catch (TimeoutException te)
            {
                logger.HandleError("Database is already disconnected : ", te);
            }
            
            finally 
            {
			    connection = null;
		    }
	    }

        private SqlConnection GetConnection(Dictionary<string,dynamic> param) 
        {

		    String dbName = (String) param["dbname"];
		    String userID = (String) param["userID"];
		    String password = (String) param["password"];
            String server = (String)param["server"];
            String url = "Server=" + server + ";" + "Database=" + dbName + ";" + "Uid=" + userID + ";" + "Pwd=" + password + ";";
            try
            {

                connection = new SqlConnection(url);
                connection.Open();
                return connection;

            }
            catch (TimeoutException ex)
            {
                logger.HandleError("Error in connecting to the database ", dbName, " for user : ", userID, " and password : ", password, ex);
                return null;
            }
           

	    }
        
	private SqlDataReader Execute(SqlConnection newconnection, String queryString,List<Object> param) 
    {
		
		SqlDataReader dataReader;
        SqlCommand cmd=new SqlCommand(queryString,newconnection);
      
		if (param != null) 
        {
			foreach (Object temp in param)
                 
            {
                cmd.Parameters.AddWithValue("@rowVal", temp.ToString());
			}
		}     
        dataReader = cmd.ExecuteReader();

        return dataReader;
	}


    private List<Dictionary<String,dynamic>> Rs2Map(SqlDataReader resultSet)
    {
        List<Dictionary<String, dynamic>> list = new List<Dictionary<String, dynamic>>();    
        Dictionary<String, dynamic> map = new Dictionary<String, dynamic>();       
        try
        {
            while (resultSet.Read())
            {
                for (int index = 0; index < resultSet.FieldCount; index++)
                {
                    String key = resultSet.GetName(index);
                    String value = ""+resultSet.GetValue(index);                    
                    map.Add(key, value);               
                }
              list.Add(map);   
            }
        }
        catch (ObjectDisposedException e)
        {
            logger.HandleError("rs2Map:Failed to get values from resultset",e.Message);
        }
       
        return list;
    }


    public List<Dictionary<String,dynamic>> RunQuery(String queryString, List<Object> param)
    {
         SqlDataReader resultSet ;
         resultSet = Execute(connection, queryString, param);
     	 List<Dictionary<String,dynamic>> list;
		 list = Rs2Map(resultSet);
         resultSet.Close();
		 return list;
	}

    public String ToString()
    {
        return StringUtils.MapString(this, connection);
    }


 }
}
