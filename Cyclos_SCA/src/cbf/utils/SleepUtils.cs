﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using src.cbf.utils;
using src.cbf.harness;
using src.cbf.engine;
using System.Threading;


namespace src.cbf.utils
{



/**
 * 
 * Utility to handle sleep with different time slabs.
 * 
 */
    public class SleepUtils 
    {

	

	private static LogUtils logger = new LogUtils();
	private static List<Int32> timerSlabs = new List<Int32>();
	    
        public static Boolean Sleep(TimeSlab level) 
        {
            int timer;
            if (level != null)
            {
                logger.Trace("Sleep:" + level);
            }
		    Boolean sleep = false;
            if (level != null)
            {
                timer = Interval(level.Level());
                if (timer > 0)
                {
                    Thread.Sleep(timer);
                    sleep = true;
                }

                logger.Trace("Slept:" + level + "-" + timer);
            }
		return sleep;
	    }

	/**
	 * 
	 * Enum to define different sleep levels
	 *  
	 */
        public class TimeSlab
        {
            public static int YIELD, LOW = 1, MEDIUM = 2, HIGH = 3;

            public enum try22 { YIELD = 0 };
            public TimeSlab() { }
            public TimeSlab(String n, int lvl)
            {
                name = n;
                level = lvl;
            }

            /**
             * Retrieves level of TimeSlab
             * 
             * @return TimeSlab level
             */
            public int Level()
            {
                return level;
            }

            /**
             * Overridden toString() method of Object class to return name for
             * specified level
             */
            public String ToString()
            {
                return name;
            }

            public int level;
            public String name;
        }

	/**
	 * Defines interval for sleep
	 * 
	 * @param lvl
	 *            level value for TimeSlab
	 * @return interval value
	 */
        public static int Interval(int lvl)
        {
            int timer = 0;
            List<Int32> lsttimerSlabs = TimerSlabs();
            if (lvl >= 0)
            {

                timer = lvl <= lsttimerSlabs.Count ? lsttimerSlabs[lvl] : lsttimerSlabs[(lsttimerSlabs.Count - 1) * (lvl - lsttimerSlabs.Count + 1)];
            }
            if (timer < 0)
            {
                timer = 0;
            }
            return timer;
        }

	/**
	 * Retrieves TimeSlab levels and returns List
	 * 
	 * @return List of levels
	 */
	public static List<Int32> TimerSlabs() {
        if (timerSlabs != null)
        {
			return timerSlabs;
		}
		String slabs = (String) Harness.GCONFIG.Get("SleepTimerSlabs");
		if (!slabs.Equals(string.Empty) || slabs != null) {
			foreach (String temp in slabs.Trim().Split(',')) 
            {
                timerSlabs.Add(Int32.Parse(temp,CultureInfo.CurrentCulture));
			}
		}
        
		if (timerSlabs ==  null) {// Not specified; use the default
            timerSlabs = new List<Int32>{ 500, 3000, 10000, 20000 };
		}
		logger.Trace("TimerSlabs Used=" + StringUtils.ToString(timerSlabs));
		return timerSlabs;
	}

	/**
	 * Inserts wait
	 * 
	 * @param secs
	 *            Time to sleep(In Seconds)
	 */
	public static void Sleep(int secs) {
        try
        {
            Thread.Sleep(secs * 1000);
        }
        catch (ThreadAbortException tae)
        {
            logger.HandleError("Error while performing sleep operation", tae);
        }
       
	}

	private SleepUtils() 
    {
	}

}

}
