﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace src.cbf.utils
{
    public class DTAccess
    {

        private readonly String filePath;

        /**
         * 
         * Interface to select row
         * 
         */
        public interface RowSelector
        {
            /**
             * Returns true/false depending on row value
             * 
             * @param row
             *            Map of row values
             * @param rowIx
             *            index of row
             * @return row exists or not
             */
            bool Select(Dictionary<string, dynamic> row, int rowIx);
        }

        /**
         * 
         * Implements RowSelector interface and selects row by ID
         * 
         */
        public class RowSelectorByRowId : RowSelector
        {
            private readonly string rowId;

            /**
             * Constructor to initialize rowId
             * 
             * @param rowId
             *            contains rowId value
             */
            public RowSelectorByRowId(String rowId)
            {
                this.rowId = rowId;
            }

            /**
             * Returns true/false depending on row value
             * 
             * @param row
             *            Map of row values
             * @param rowIx
             *            index of row
             * @return row exists or not
             */
            public bool Select(Dictionary<string, dynamic> row, int rowIx)
            {
                if(row==null)
                {
                    throw new ArgumentNullException();
                }
                return rowId.Equals(string.Empty) || rowId.Equals((String)row["_rowId"]);               
            }
        }

        /**
         * Constructor to initialize excelFilePath variable
         * 
         * @param excelFilePath
         *            path of excel file
         */
        public DTAccess(String excelFilePath)
        {
            this.filePath = excelFilePath; // FIXME: check for file
                                           /** existence/readability; */

        }

        /**
         * Returns List containing excel sheet rows
         * 
         * @param sheetName
         *            name of excel sheet
         * @return List of rows
         */
        public List<Dictionary<string, dynamic>> ReadSheet(String sheetName)
        {
            return ReadSelectedRows(sheetName, "");
        }

        /** FIXME:catch exception and call handleError */

        /**
         * Reads selected rows and returns list
         * 
         * @param sheetName
         *            name of excel sheet
         * @param selector
         *            object of RowSeletor
         * @return list of selected rows
         */

        class MyHandler : src.cbf.utils.ExcelAccess.MapReader
        {
            /**public Dictionary<string, string> outrow; */
            public List<Dictionary<string, dynamic>> rows = new List<Dictionary<string, dynamic>>();
            public readonly RowSelector selector;
            public MyHandler(RowSelector sa)
            {
                selector = sa;
            }

            public override bool HandleRow(Dictionary<string, dynamic> row, int rowIx)
            {
                if (selector == null || selector.Select(row, rowIx))
                {
                    rows.Add(row);
                }
                return true;
            }
        }

        public List<Dictionary<string, dynamic>> ReadSelectedRows(String sheetName, RowSelector selector)
        {

            MyHandler mh = new MyHandler(selector);
            ExcelAccess.AccessSheet(filePath, sheetName, mh);
            return mh.rows;
        }

        /** convenience */
        /**
         * Reads selected rows and returns list
         * 
         * @param sheetName
         *            name of excel sheet
         * @param rowId
         *            index of row
         * @return list of selected rows
         */
        public List<Dictionary<string, dynamic>> ReadSelectedRows(string sheetName, String rowId)
        {
            RowSelector selector = new RowSelectorByRowId(rowId);
            List<Dictionary<string, dynamic>> selectedRows;
            selectedRows = ReadSelectedRows(sheetName, selector);
            return selectedRows;
        }

        /**
         * Overriding toString() method and returning DTAccess format string
         */
        public override string ToString()
        {
            return StringUtils.MapString(this, filePath);
        }

    }
}
