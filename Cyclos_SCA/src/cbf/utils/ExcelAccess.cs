using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Interop.Excel;
using System.IO;
using Microsoft.Office.Interop;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Data.OleDb;
using System.Data;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace src.cbf.utils
{

    /**
     * 
     * Handles all the function related to Excel Data sheet like reading
     * sheet,reading row,etc..
     * 
     */
    public class ExcelAccess
    {
            private static LogUtils logger;

            public ExcelAccess()
            {           
                logger = new LogUtils(this);
                
            }

        [DllImport("user32.dll")]
        static extern int GetWindowThreadProcessId(int hWnd, out int lpdwProcessId);
        
        /**
         * 
         * Handles row data
         * 
         */
        
        public interface RowHandler
        {
            
            /**
             * Accesses row and returns boolean result
             * 
             * @param rowAccess
             *            object of RowAccess
             * @param rowIx
             *            index of row to be accessed
             * @return boolean result
             */
            Boolean HandleRow(RowAccess rowAccess, int rowIx);
        }

        /**
         * 
         * Accesses row of sheet and sets respective values
         * 
         */
        public interface RowAccess
        {
            String[] Get();

            void Set(String[] values);

         };

        /**
         * Accesses the particular sheet of workbook
         * 
         * @param fileName
         *            name of workbook
         * @param sheetName
         *            name of excel sheet
         * @param rowHandler
         *            object of RowHandler
         * @return number of rows
         */
        public static int AccessSheet(String fileName, String sheetName, RowHandler rowHandler)
        {
            string fileExtension = Path.GetExtension(fileName);
            ISheet sheet;
            using (FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                if (fileExtension.Equals(".xlsx", StringComparison.OrdinalIgnoreCase))
                {
                    XSSFWorkbook excelFile = new XSSFWorkbook(file);
                    file.Close();
                    sheet = excelFile.GetSheet(sheetName);
                }
                else
                {
                    HSSFWorkbook excelFile = new HSSFWorkbook(file);
                    file.Close();
                    sheet = excelFile.GetSheet(sheetName);
                }
            }
            int accessCount = Access(sheet, rowHandler);

            return accessCount;


        }

        public static Microsoft.Office.Interop.Excel.Application GetxlApp()
        {
            Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
            return xlApp;
        }

        /**
         * Accesses the particular sheet of uiMap(locators) workbook
         * 
         * @param fileName
         *            name of workbook
         * @param rowHandler
         *            object of RowHandler
         * @return number of Locators
         */

        public static int AccessLocatorSheet(String fileName, RowHandler rowHandler)
        {
            int numberofLoc = 0;
            ISheet sheet;
            string fileExtension = Path.GetExtension(fileName);
            using (FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                if (fileExtension.Equals(".xlsx", StringComparison.OrdinalIgnoreCase))
                {
                    XSSFWorkbook excelFile = new XSSFWorkbook(file);
                    file.Close();
                    int totalSheet = excelFile.Count;
                    for (int i = 0; i < totalSheet; i++)
                    {
                        string sheetName = excelFile.GetSheetName(i);
                        sheet = excelFile.GetSheet(sheetName);
                        numberofLoc = numberofLoc + Access(sheet, rowHandler);
                    }
                }
                else
                {
                    HSSFWorkbook excelFile = new HSSFWorkbook(file);
                    file.Close();
                    int totalSheet = excelFile.Workbook.NumSheets;
                    for (int i = 0; i < totalSheet; i++)
                    {
                        string sheetName = excelFile.GetSheetName(i);
                        sheet = excelFile.GetSheet(sheetName);
                        numberofLoc = numberofLoc + Access(sheet, rowHandler);
                    }

                }
                return numberofLoc;

            }
        }
        public class SimpleRowAccess : RowAccess
        {

            public List<string> cells;
           
            public SimpleRowAccess(List<string> cells)
            {
                this.cells = cells;
            }

            /**To convert list into array */
            public String[] Get()
            {
                String[] colValues = cells.ToArray();
                return colValues;
            }

            public void Set(String[] values)
            {
                if (values != null)
                {
                    for (int i = 0; i < cells.Count(); ++i)
                    {
                        if (i >= values.Length)
                        {
                            break;
                        }
                    }
                }
            }

        }

       public static int Access(ISheet sheet, RowHandler rowHandler)
        {
            if(sheet==null)
            {
                throw new ArgumentNullException();
            }
            int count = 1;
            int rowcountnew = 0;
            bool rc = false;
            int rowcount = 0;
            
            List<String> cellList = new List<String>();
            int lastCellNumber = sheet.GetRow(0).LastCellNum;
            if (sheet != null)
            {
                rowcount = sheet.LastRowNum;
            }
            if (rowcount == 0 && sheet.GetRow(0) == null)
            {
                return 0;
            }

            for (int row1 = 0; row1 <= rowcount; row1++)
            {
                for (int col1 = 0; col1 < lastCellNumber; col1++)
                {
                    if (sheet.GetRow(row1).GetCell(col1) != null)
                    {
                        cellList.Add(sheet.GetRow(row1).GetCell(col1).ToString());
                    }
                    else
                    {
                        cellList.Add("");
                    }
                }
                if (rowHandler != null)
                {
                    rc = rowHandler.HandleRow(new SimpleRowAccess(cellList), rowcountnew);
                }
                if (!rc)
                {
                    break;
                }
                ++count;
                cellList.Clear();
                rowcountnew++;
            }
            return count;
        }

        /**
         * Checks existence of excel sheet
         * 
         * @param sheetName
         *            name of excel sheet
         * @param fileName
         * @return excel sheet exists or not
         */
        public static bool IsSheetExists(String fileName, String sheetName)
        {
            string fileExtension = Path.GetExtension(fileName);
            bool result = false;
            dynamic excelFile;
            try
            {
                if (fileExtension.Equals(".xlsx", StringComparison.OrdinalIgnoreCase))
                {
                    using (FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        excelFile = new XSSFWorkbook(file);
                        file.Close();
                    }
                }
                else
                {
                    using (FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        excelFile = new HSSFWorkbook(file);
                        file.Close();
                    }
                }
                ISheet activeSheet = excelFile.GetSheet(sheetName);
                if (activeSheet != null)
                {
                    result = true;
                }
            }
            catch (IOException ex)
            {
                logger.Trace(ex.Message);
                logger.HandleError("Error" + ex.Message);
            }
            return result;
        }

        /**
         * 
         * Sets and gets the value of rows
         * 
         */
        public interface NamedRowAccess
        {
            /**
             * Gets Row and returns it
             * 
             * @return object of DataRow
             */
            Dictionary<string, dynamic> Get();

            /**
             * Sets value of row
             * 
             * @param namedValues
             *            object of DataRow
             */
            void Set(Dictionary<string, string> NamedValues);

            /**
             * Overloaded method that sets value of particular column
             * 
             * @param colName
             *            name of field
             * @param value
             *            value of that field
             */
            void Set(String colName, String value);
        }

        /**
         * 
         * Implementing RowHandler interface and handles row data
         * 
         */
        public abstract class NamedRowHandler : RowHandler
        {
            private static String[] colNames;
            private static Dictionary<string, int> colName2IxMap = new Dictionary<string, int>();

            private class Access : NamedRowAccess
            {
                private readonly RowAccess rowAccess;

                public Access(RowAccess rowAccess)
                {
                    this.rowAccess = rowAccess;
                }

                public Dictionary<string, dynamic> Get()
                {
                    String[] values = rowAccess.Get();
                    Dictionary<string, dynamic> namedValues = new Dictionary<string, dynamic>();
                    for (int ix = 0; ix < values.Length; ix++)
                    {
                        if (ix >= colNames.Length)
                        {
                            break;
                        }
                        String colName = colNames[ix];
                        namedValues.Add(colName, values[ix]);
                    }

                    return new Dictionary<string, dynamic>(namedValues);
                }

                public void Set(Dictionary<string, string> namedValues)
                {
                    logger.Trace("namedValues logged");
                }

                public void Set(String colName, String value)
                {

                    logger.Trace(colName + " " + value);

                }
            }

            /**
             * Abstract method of RowHandler
             * 
             * @param namedRowAccess
             *            object of NamedRowAccess
             * @param rowIx
             *            index of row
             * @return boolean result
             */
            public abstract bool HandleRow(NamedRowAccess namedRowAccess, int rowIx);

            /**
             * Handles row and returns boolean result
             * 
             * @param rowAccess
             *            object of RowAccess
             * @param rowIx
             *            index of row
             * @return boolean result
             */
            public bool HandleRow(RowAccess rowAccess, int rowIx)
            {

                String[] colValues = null;

                if (rowAccess != null)
                {
                    colValues = rowAccess.Get();
                }
                if (rowIx == 0)
                { // header row
                    colValues = colValues.Where(val => val != null).ToArray();
                    SetColNames(colValues);
                    return true;
                }
                return HandleRow(new Access(rowAccess), rowIx - 1); //To add row from excel file
            }

            private static void SetColNames(String[] colNamess)
            {
                if (colNamess != null)
                {
                    colNames = colNamess;
                }
                colName2IxMap.Clear();
                for (int ix = 0; ix < colNames.Length; ix++)
                {
                    colName2IxMap.Add(colNames[ix], ix);
                }
            }
        }

        /**
         * 
         * Extending NamedRowHandler class and handles row data
         * 
         */
        public abstract class MapReader : NamedRowHandler
        {
            /**
             * Abstract method of NamedRowHandler class
             * 
             * @param row
             *            object of DataRow
             * @param rowIx
             *            index of row
             * @return boolean result
             */
            public abstract bool HandleRow(Dictionary<string, dynamic> row, int rowIx);

            /**
             * Overloaded method of NamedRowHandler class
             * 
             * @param namedRowAccess
             *            object of NamedRowAccess
             * @param rowIx
             *            index of row
             * @return boolean result
             */
            public override bool HandleRow(NamedRowAccess namedRowAccess, int rowIx)
            {
                if(namedRowAccess==null)
                {
                    throw new ArgumentNullException();
                }
                return HandleRow(namedRowAccess.Get(), rowIx);
            }
        }

        /**
         * 
         * Extends MapHeader class and defines its abstract method
         * 
         */
        public class RowArrayBuilder : MapReader
        {
            readonly private List<Dictionary<string, dynamic>> rows;

            /**
             * Constructor to initialize List of rows
             * 
             * @param rows
             *            List containing row data in Map
             */
            public RowArrayBuilder(List<Dictionary<string, dynamic>> rows)
            {
                this.rows = rows;
            }

            /**
             * Abstract method of MapHeader class that is handling row data
             * 
             * @param row
             *            object of DataRow
             * @param rowIx
             *            index of row
             * @return boolean result
             */
            public override bool HandleRow(Dictionary<string, dynamic> row, int rowIx)
            {
                rows.Add(row);
                return true;
            }
        }


    }

}
