/**Copyright � 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.Data;
using System.Collections.Generic;
using System.Management;
namespace src.cbf.utils
{
    public class ConstantVar
    {
       public static readonly string REPORTSELECTIONS = "DesignerTestCaseAccess";
       public static readonly string CONFIG = "CONFIG";
       public static readonly string UNIQUE = "UNIQUE";
       public static readonly string INPUT = "INPUT";
       public static readonly string OUTPUT = "OUTPUT";
       public static readonly string STEPID = "StepId";
       public static readonly string PARAMCODE = "ParamCode";
       public static readonly string EMAILTRACER = "In Email Alerter with parameters ";
       public static readonly string MAILTO = "MailTo";
       public static readonly string MAILSUBJECT = "Failure in Automated Execution of TestCase : ";
       public static readonly string MAILBODY = "Hello,<br><br> Please find more" +
            " details regarding the test case failure in below tables. <br>" +
            "<br> Also PFA failure screenshot for your reference. <br><br>";

        /**DESIGNER ACCESS UTIL */
       public static readonly string SERVICE = "Service";
       public static readonly string TESTCASE = "TestCase";
       public static readonly string GET = "get";
       public static readonly string DEISIGNERVER = "2.7.1";

       public static readonly string FOLDERPATH = "folderpath";
       public static readonly string TCNAME = "tcName";
       public static readonly string COMPONENT = "component";
       public static readonly string OBJECT = "object";
       public static readonly string METHOD = "method";
       public static readonly string VERSIOnNUMBER = "versionNumber";
       public static readonly string ARGUMENTS = "arguments";
       public static readonly string ARROW = "=>";
       public static readonly string DOT = ":";
       public static readonly string NIL = "nil";
       public static readonly string NULL = "null";

       public static readonly string CONTENTTYPE = "application/json; charset=utf-8";
       public static readonly string pMETHOD = "POST";
       public static readonly string AUTHORIZATION = "Authorization";
       public static readonly string BASIC = "Basic ";
       public static readonly string PARAMETERACCESS = "ParameterAccess";


    }
}
