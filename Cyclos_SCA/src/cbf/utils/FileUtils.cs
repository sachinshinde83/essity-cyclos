/**Copyright � 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;

namespace src.cbf.utils
{
    public class FileUtils
    {

        /**
         * Returns list of files in specified folder
         * 
         * @param folder
         *            name of folder containing files
         * @return list of files
         */
        /**public static List<FileInfo> fileList(string folder)
        {
            throw new FrameworkException("Method not implemented : fileList(", folder, ")");
        }*/

        /**
         * Checks file exist or not at specified path
         * 
         * @param sPath
         *            path of file
         * @return boolean result
         */
        public static bool MakePath(string sPath)
        {
            FileInfo file = new FileInfo(sPath);
            if (file.Exists)
            {
                return true;
            }
            if (!MakePath(file.Directory.ToString()))
            {
                return false;
            }
            try
            {
                file.Create();
            }
            catch (IOException e)
            {
                logger.HandleError("Exception caught in creating file :"+ e.Message); 
            }
            return file.Exists;
        }

       

        /**
         * Removes folder specified
         * 
         * @param sFolderPath
         *            path of folder to be removed
         */
        public static void RemoveFolder(string sFolderPath)
        {
            FileInfo file = new FileInfo(sFolderPath);
            file.Delete();
        }

        /**
         * Makes folder with specified path and returns boolean value
         * 
         * @param sFolderPath
         *            path of folder
         * @return boolean result
         */
        public static bool MakeFolder(string sFolderPath)
        {
            

            bool folderExists = Directory.Exists(sFolderPath);
            if (!folderExists)
            {
                Directory.CreateDirectory(sFolderPath);
                return true;
            }
            else
            {
                return true;// as directory already exists
            }
              
        }

        /**
         * Returns temporary path
         * 
         * @return temporary path string
         */
        /**public static string getTempPath()
        {
            throw new FrameworkException("Method not implemented : getTempPath()");
        }*/



        public static LogUtils logger = new LogUtils();
    }
}
