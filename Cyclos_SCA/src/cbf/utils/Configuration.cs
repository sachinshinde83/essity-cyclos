﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;


namespace src.cbf.utils
{
    public class Configuration
    {
        
	/**
	 * Constructor to load configuration file
	 * 
	 * @param fileName
	 *            name of Configuration file
	 */
	public Configuration(String fileName)  {
		this.fileName = fileName;
		configMap = Load(fileName);
	}

	/**
	 * Returns the value associated with the key
	 * 
	 * @param key
	 *            to retrieve value
	 * @return value as Object
	 */
	public Object Get(String key) {
		return configMap[key];

        

	}

    public void Set(Dictionary<String, Object> map)
    {
		configMap = map;
	}

	/**
	 * Returns all the properties of config file
	 * 
	 * @return map TODO: check if this can be removed
	 */
    public Dictionary<string, Object> FetchAllProp()
    {
		return configMap;
	}

	/*
	 * load the file and return map methods below are simply for parsing/loading
	 * the different kinds of nodes
	 */
    public Dictionary<String, Object> Load(String fileNameNew)
    {
        Dictionary<String, object> dctVar = new Dictionary<String, object>();

        /**logger.trace("Loading configuration from ", fileName); */
        if (fileNameNew != null && (File.Exists(fileNameNew)))
        {           

                try
                {
                    XmlDocument xml = new XmlDocument();
                    xml.Load(fileNameNew);
                    dctVar = fileNameNew.Contains("MasterConfig.xml")?GetVariables(xml.SelectNodes("/plugins/variable")):GetVariables(xml.SelectNodes("/config/variable"));
                    return dctVar;

                }
                catch (IOException ex)
                {
                    Console.WriteLine(ex.Message);
                }

            
        }
        return null;
    }

    public Dictionary<String, Object> GetVariables(XmlNodeList rootNode)
    {
        Dictionary<String, Object> map = new Dictionary<String, Object>();
        if (rootNode != null)
        {
            foreach (XmlNode subNode in rootNode) // Loop through List with foreach.
            {

                if (subNode.Name == "variable" && subNode.NodeType == XmlNodeType.Element)
                {
                    LoadVariable(subNode, map);
                }
            }
        }

		return map;
	}

	/*
	 * load variable defined in varNode into map
	 */
    private void LoadVariable(XmlNode varNode, Dictionary<String, Object> map)
    {
		String name = "";
		Object value = "";

        /**XmlNodeList varNodes = varNode.ChildNodes; */

        try
        {
            for (XmlNode subNode = varNode.FirstChild; subNode != null; subNode = subNode.NextSibling)
            {

                if (subNode.NodeType == XmlNodeType.Element)
                {
                    if (string.Equals(subNode.Name, "name"))
                    {
                        name = subNode.InnerText;
                    }
                    if (subNode.Name == "value")
                    {
                        value = GetValue(subNode);
                    }

                    if (name == "RunHome" && value.ToString() != "")
                    {
                        value = value + "\\Executed_on_" + UniqueUtils.UniqueDate();
                        /**value = value ; */
                    }
                }
            }

            map.Add(name, value);
        }
        catch (NullReferenceException ne)
        {
            Console.WriteLine(ne.Message);
        }
       
	}

	private dynamic GetValue(XmlNode valNode) 
    
    {

        Dictionary<String, Object> subNodesMap = new Dictionary<String, Object>();
        /**XmlNodeList valNodes = valNode.ChildNodes; */

        for (XmlNode subNode = valNode.FirstChild; subNode != null; subNode = subNode.NextSibling)             
        {

            if (string.Equals(subNode.Name, "array"))
            {
                return GetArray(subNode);
            }

            if (valNode.ChildNodes.Count == 1)
            {

                if (string.Equals(subNode.Name,"#text"))
                { return subNode.InnerText; }

                else
                {
                    Dictionary<String, Object> oneNodeMap = new Dictionary<String, Object>();
                    oneNodeMap.Add(subNode.Name, subNode.InnerText);
                    return oneNodeMap;
                }
                
            }
				

			LoadSubnode(subNode, subNodesMap);
		}

		return subNodesMap;
	}

    private void LoadSubnode(XmlNode mapNode, Dictionary<String, Object> map)   

    {
        /**XmlNodeList mapNodes = mapNode.ChildNodes; */
        for (XmlNode subNode = mapNode.FirstChild; subNode != null; subNode = subNode.NextSibling)             
        {
            if (!map.ContainsKey(mapNode.Name))
            {
                map.Add(mapNode.Name, GetValue(mapNode));
            }
		}
	}

    private List<Dictionary<String, Object>> GetArray(XmlNode arrayNode)
    
    {
        List<Dictionary<String, Object>> array = new List<Dictionary<String, Object>>();

        for (XmlNode subNode = arrayNode.FirstChild; subNode != null; subNode = subNode.NextSibling)          
        {
            if (subNode.Name == "value") 
            {                
                array.Add(GetValue(subNode));
			}
		}

        
		return array;
	}

	/**
	 * Returns Configuration format string
	 */
/**	public String toString() { */
/**		return StringUtils.mapString(this, fileName); */
/**	} */

	private readonly String fileName;
    private Dictionary<String, Object> configMap;

    }
}
