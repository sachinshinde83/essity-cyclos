﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Threading;

namespace src.cbf.utils
{
    public class UniqueUtils
    {


        /**
         * Retrieves unique string from the given one
         * 
         * @param str
         *            of which unique string to be found
         * @return unique sring value
         */
        public String UniqueString(String str)
        {

            String[] parsed;
            
            /**String[] parsed = new String[2] { "", "" }; */

            parsed = Parse(str);
            if (parsed.Equals(string.Empty)) {
                logger.HandleError("Invalid input : " + str);
                return null;
            }
            parsed[0] = str;
          /**  List<Char> domain; */
            List<String> domain;

            domain =  GetDomain("0")??GetDomain(parsed[1]);

            int domLen = domain.Count();
            /**int outLen = int.Parse(parsed[0]); */

 
            int outLen;

            int.TryParse(parsed[0], out outLen);
            
          
            int nSecs = (int) ((DateTime.Now - beginTm).TotalMilliseconds);
        

            String uniqueString = "";

            while (uniqueString.Count() < outLen) 
            {
                int n = nSecs % domLen;
                uniqueString = domain[n] + uniqueString;
                nSecs = nSecs / domLen;
            }
            /**String uniqueString = "";

            uniqueString = DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss", CultureInfo.InvariantCulture);
            */
            return uniqueString;
        }

        private String[] Parse(String str)
        {
            String[] result = new String[2];        
            /**String[] result = new String[2] { "", "" }; */


            Match match = Regex.Match(str, @"^\s*(\d+)(\w*)\\s*$", RegexOptions.IgnoreCase);
          /**  Match match = Regex.Match(str, @"\d{1}", RegexOptions.IgnoreCase); */
            while (match.Success)
            {
                result[0] = match.Groups[1].Value;
                result[1] = match.Groups[2].Value;
            }
            return result;
        }

        /**public List<Char> getDomain(String str)
        {
            List<Char> domain = new List<Char>();
            String[] variants = { "A", "a", "0" };
            foreach (String variant in variants)
            {

                if (str.Contains(variant))
                {
                    switch (variant[0])
                    {
                        case 'A':
                            domain = append('A', 26);
                            break;
                        case 'a':
                            domain = append('a', 26);
                            break;
                        case '0':
                            domain = append("0", 10);
                            break;
                    }
                }
            }
            return domain;
        }*/
        public List<String> GetDomain(String str)
        {
            List<String> domain = new List<String>();
            String[] variants = { "A", "a", "0" };
            foreach (String variant in variants)
            {
                if (str != null && str.Contains(variant))
                {                  
                        switch (variant[0])
                        {
                            case 'A':
                                domain = Append("A", 26);
                                break;
                            case 'a':
                                domain = Append("a", 26);
                                break;
                            case '0':
                                domain = Append("0", 10);
                                break;
                            default:
                                logger.Trace("Default switch selection");
                                break;
                        }                    
                }
            }
            return domain;
        }
        private List<String> Append(String startChar, int numChars)
        {
            List<String> domain = new List<String>();
            int temp;
            if (!int.TryParse(startChar,out temp))
            {
                int asciiCode = temp;
                for (int ix = asciiCode; ix <= asciiCode + numChars - 1; ix++)
                {
                    domain.Add(ix.ToString(CultureInfo.InvariantCulture));
                }
            }
            else
            {
                for (int ix = int.Parse(startChar, CultureInfo.InvariantCulture); ix <= numChars - 1; ix++)
                {
                    domain.Add(ix.ToString(CultureInfo.InvariantCulture));
                }
            }
            return domain;
        }
        

        private readonly LogUtils logger;
        private readonly DateTime beginTm;
        /**private static UniqueUtils uniqueUtils; */

        /**
         * Returns instance of UniqueUtils class
         * 
         * @return object of UniqueUtils
         */
        public UniqueUtils()
        {

            /**uniqueUtils.beginTm = DateTime.Now; */
            this.beginTm = DateTime.Now;
            logger = new LogUtils(this);

        }
      
        public static String UniqueDate()
        {

            return DateTime.Now.ToString("dd-MM-yyyy", CultureInfo.InvariantCulture) + "_Time_" + DateTime.Now.ToString("HH-mm-ss", CultureInfo.InvariantCulture);
        }

    }

}
