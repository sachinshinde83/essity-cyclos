﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using src.cbf.utils;
using System.Xml;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;


namespace src.cbf.utils
{
    public class DesignerAccessUtil
    {
        /**
 * Makes connection to Designer and returns its response
 * 
 * @param param
 *            Map containing parameters for Designer request
 */
        public readonly LogUtils logger;
        public DesignerAccessUtil(Dictionary<String, Object> param)
        {
            if (param != null)
            {
                testName = (String)param["testName"];
                folderPath = (String)param["folderPath"];

                serverURL = (String)param["url"];
                String projectName = (String)param["projectname"];
                request = serverURL + "/" + projectName;

                String userName = (String)param["username"];
                String password = (String)param["password"];
                userCredentials = userName + ":" + password;
            }
        }

	public object AccessDesigner()
    {

        Dictionary<String, String> args = new Dictionary<String, String>();
		args.Add(ConstantVar.FOLDERPATH, folderPath);
        args.Add(ConstantVar.TCNAME, testName);
        
        Dictionary<String, String>[] argArray = { args };
        
        Dictionary<String, Object> param = new Dictionary<String, Object>();
        param.Add(ConstantVar.COMPONENT, ConstantVar.SERVICE);
        param.Add(ConstantVar.OBJECT, ConstantVar.TESTCASE);
        param.Add(ConstantVar.METHOD, ConstantVar.GET);
        param.Add(ConstantVar.VERSIOnNUMBER, ConstantVar.DEISIGNERVER);
        param.Add(ConstantVar.ARGUMENTS, argArray);

		String response = SendRequest(param);

		if (response != null) {
			response = response.Replace(ConstantVar.ARROW, ConstantVar.DOT);
			response = response.Replace(ConstantVar.NIL, ConstantVar.NULL);

            var serializer = new JavaScriptSerializer();
            return serializer.Deserialize(response, typeof(object));
			
		}
		return null;

	}

    public String SendRequest(Dictionary<String, Object> param)
    {
            
        
        String line=null;

        try
        {
            var strJsonData = new JavaScriptSerializer().Serialize(param);

            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(request);
            httpWebRequest.ContentType = ConstantVar.CONTENTTYPE;
            httpWebRequest.Method = ConstantVar.pMETHOD;
            httpWebRequest.Proxy = new WebProxy() { UseDefaultCredentials = true };

            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(userCredentials);
            var Credentials = System.Convert.ToBase64String(plainTextBytes);

            httpWebRequest.Headers.Add(ConstantVar.AUTHORIZATION, ConstantVar.BASIC + Credentials);

            string postData = strJsonData;
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] byte1 = encoding.GetBytes(postData);

            /** Set the content length of the string being posted. */
            httpWebRequest.ContentLength = byte1.Length;
            httpWebRequest.KeepAlive = false;
            Stream newStream = httpWebRequest.GetRequestStream();
            newStream.Write(byte1, 0, byte1.Length);
            newStream.Flush();
            newStream.Close();

            var response = httpWebRequest.GetResponse();

            var stream = response.GetResponseStream();
            var sr = new StreamReader(stream);
            var content = sr.ReadToEnd();

            line = content;
        }
        catch (IOException ioex)
        {
                logger.HandleError("Error" + ioex.Message);
        }
       
        return line;
    }
           

	/**
	 * Returns DesignerAccess format string
	 */
    public String ToString()
    {
        return StringUtils.MapString(this, testName+ folderPath +  userCredentials);
    }

	
	private readonly String folderPath;
	private readonly String testName;
	private readonly String request;
	private readonly String userCredentials;
	private readonly String serverURL;

    }
}
