﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using src.cbf.utils;

namespace CBF_CSharp.src.cbf.utils
{
    public class RegExUtils    
    {
        public RegExUtils()
        {
            logger = new LogUtils(this);
        }
    /**
     * 
     * Matches String
     * 
     */
       
    public interface Handler 
    {
        /**
         * Matches string and returns boolean result
         * 
         * @param m
         *            object of Matcher
         * @return boolean result
         */
        bool Handle(Match m);
    };

    /**
     * 
     * Performs match and replaces string
     * 
     */
    public interface Replacements 
    {
        /**
         * Replaces old string with new one
         * 
         * @param m
         *            object of matcher
         * @return new string
         */
        String Replacement(Match m); // return null if it cant be
        /** replaced */
    }

    /**
     * Replaces string if matched with Pattern
     * 
     * @param src
     *            string to be replaced
     * @param p
     *            Pattern with which string need to be matched
     * @param r
     *            with which source will be replaced
     * @return new replaced string
     */
    public static String Replace(String src, String p, Replacements r)
    {
        StringBuilder sb = new StringBuilder();
        Match m = Regex.Match(src,p,RegexOptions.IgnoreCase);
        String s = string.Empty;

        while (m.Success)
        {
            if (r != null)
            {
                s = r.Replacement(m);
                sb.Append(s);
            }

            if (s == null) // retain old, if no replacement is found
            {
                s = m.Groups[0].Value;
                sb.Append(s);
            }
            /** To escape the special characters like ($) */
            /** TODO:Need to remove ASAP */
            if (s.Contains("$"))
            {
                sb.Append("\\" + s);
            }
            
            /**m.appendReplacement(sb, s);             */
        }
        /**m.appendTail(sb); */
        return sb.ToString();
    }

    /** where replacements are directly got by keying the entire match from a map */
    /**
     * Converts map to object of Replacements
     * 
     * @param map
     *            to be converted
     * @return object of Replacements
     */
    public static Replacements Map2Replacements(Dictionary<String, String> map)
    {
        Replacemnts replacemnts = new Replacemnts(map);
        return replacemnts;

        /**return new Replacements() */
        /**{ */
        /**    public String replacement(Matcher m) */
        /**    { */
        /**        return map.get(m.group()); */
        /**    } */
        /**}; */
    }

    /**
     * Converts string to Pattern
     * 
     * @param s
     *            to be converted
     * @return Pattern generated
     */
    public static Regex String2Pattern(String s)
    {
        Regex patt=null;
        try
        {
            patt = new Regex(s);
        }
        catch (ArithmeticException ae)
        {
            logger.Trace(ae.Message);
        }
       
        return patt;
    }
    private static  LogUtils logger; 
}

    public class Replacemnts:RegExUtils.Replacements
    {
        readonly Dictionary<String, String> map;
        public Replacemnts(Dictionary<String, String> map)

        {
            this.map = map;
            
        }
        public String Replacement(Match m)
        {
            if( m == null)
            {
                throw new ArgumentNullException();
            }
            if (m != null)
            {
                return map[m.Groups[0].Value];
            }
            return map[m.Groups[0].Value];
        }
    };

}
