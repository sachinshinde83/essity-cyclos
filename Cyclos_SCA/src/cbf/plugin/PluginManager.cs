﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using src.cbf.harness;
using src.cbf.utils;
using src.cbf.testAccess;
using System.Windows.Forms;

namespace src.cbf.plugin
{
    public class PluginManager
    {

       /**
         * Set by Harness directly prior to using it TODO: refine bootstrapping
         */

        public static String masterConfigFileName;

        /**
         * Returns instance of PluginManager
         * @return PluginManager instance
         */
        public static PluginManager Instance()
        {
            if (single == null)
            {
                masterConfigFileName = ResourcePaths.GetInstance().GetFrameworkResource("Resources", "MasterConfig.xml");
                single = new PluginManager(masterConfigFileName);
            }
            return single;
        }

        /**
         * Returns instance of specified Plugin
         * 
         * @param usageMap
         *            parameters of plugin
         * @return instance of Plugin
         */

        public static Object GetPlugin(Dictionary<String, Object> usageMap)
        {
            if(usageMap==null)
            {
                throw new ArgumentNullException();
            }
            object Plugin = null;
            if (usageMap != null)
            {
                Plugin = usageMap.ContainsKey("parameters") ?
                GetPlugin((String)usageMap["plugin"], (Dictionary<String, Object>)usageMap["parameters"]) :
                GetPlugin((String)usageMap["plugin"], null);
            }
            
            return Plugin;
        }

        /**
         * Returns instance of specified Plugin
         * 
         * @param pluginName
         *            of Plugin
         * @param usageParams
         *            parameters of plugin
         * @return instance of Plugin
         */
        public static Object GetPlugin(String pluginName, Dictionary<String, Object> usageParams)
        {

            return Instance().ParsePlugin(pluginName, usageParams);
        }

        public PluginManager()
        {
        }

        public PluginManager(String fileName)
        {
            try
            {
                masterConfig = new Configuration(fileName);
            }
            catch (ArgumentNullException e)
            {
                logger.HandleError("Error"+e.Message);
            }

        }

        public Object ParsePlugin(String pluginName, Dictionary<String, Object> usageParams)
        {
            String className = null;
            Dictionary<String, Object> masterParamsMap = new Dictionary<String, Object>();

            try
            {
                Dictionary<String, Object> masterPluginMap = (Dictionary<String, Object>)masterConfig.Get(pluginName);
                className = (String)masterPluginMap["classname"];
                masterParamsMap = masterPluginMap.ContainsKey("parameters") ? (masterPluginMap["parameters"].Equals("") ? null : (Dictionary<String, Object>)masterPluginMap["parameters"]) : null;
            }
            catch (ArgumentNullException e)
            {
                logger.HandleError( e.Message);
            }

            Dictionary<String, Object> finalMap = MergeMaps(masterParamsMap, usageParams);

            foreach (KeyValuePair<String, Object> entry in finalMap)
            {
                if (entry.Value.Equals("TBD"))
                {
                    logger.HandleError(entry.Key + " value must be specified in user config for ", pluginName);
                }
            }

            Dictionary<String, dynamic> finalMap1 = new Dictionary<String, dynamic>();
            foreach (KeyValuePair<String, Object> entry in finalMap)
            {
                finalMap1.Add(entry.Key, entry.Value);
            }

            
                return InitializePlugin(className, finalMap1);
           

        }


        public static Object InitializePlugin(String className, Dictionary<String, String> finalMap)
        {
            try
            {
                Type t = Type.GetType(className);
                return Activator.CreateInstance(t, finalMap);
            }

            catch (ArgumentNullException e)
            {
                logger.HandleError("Initialize plugin is failed", e.Message);
            }

            return finalMap;
        }

        public static Object InitializePlugin(String className, Dictionary<String, dynamic> finalMap)
        {
            try
            {
                Type t = Type.GetType(className);
                return Activator.CreateInstance(t, finalMap);

            }
            catch (ArgumentNullException e)
            {
                logger.HandleError("Error" + e.Message);
            }
           

            return finalMap;
        }

        private Dictionary<String, Object> MergeMaps(Dictionary<String, Object> masterParamsMap, Dictionary<String, Object> usageParams)
        {
            Dictionary<String, Object> finalMap;
            if (masterParamsMap == null)
            {
                finalMap = usageParams;
            }

            else
            {
                finalMap = masterParamsMap;
                if (usageParams != null)
                {
                    AddRange(finalMap, usageParams);
                }


            }
            return finalMap;
        }

        public void AddRange(Dictionary<String, Object> source, Dictionary<String, Object> collection)
        {
            if (collection == null)
            {
                throw new ArgumentNullException("Collection is null");
            }

            if (source == null)
            {
                throw new ArgumentNullException("Source is null");
            }

            foreach (var item in collection)
            {
                if (!source.ContainsKey(item.Key))
                {
                    source.Add(item.Key, item.Value);
                }
                else
                {
                    source.Remove(item.Key);
                    source.Add(item.Key, item.Value);
                }
            }
        }




        /**
         * Returns PluginManager format string
         */

        public String TooString()
        {
            return StringUtils.MapString(this, masterConfig);
        }

        private static PluginManager single;
        private static LogUtils logger = new LogUtils();
        readonly private Configuration masterConfig;
    }
}
