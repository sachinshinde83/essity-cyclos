﻿/*Copyright © 2017 Capgemini Group of companies. All rights reserved
(Subject to Limited Distribution and Restricted Disclosure Only.)
THIS SOURCE FILE MAY CONTAIN INFORMATION WHICH IS THE PROPRIETARY
INFORMATION OF CAPGEMINI GROUP OF COMPANIES AND IS INTENDED FOR USE
ONLY BY THE ENTITY WHO IS ENTITLED TO AND MAY CONTAIN
INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, OR EXEMPT FROM
DISCLOSURE UNDER APPLICABLE LAW.
YOUR ACCESS TO THIS SOURCE FILE IS GOVERNED BY THE TERMS AND
CONDITIONS OF AN AGREEMENT BETWEEN YOU AND CAPGEMINI GROUP OF COMPANIES.
The USE, DISCLOSURE REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
RESTRICTED AS SET FORTH THEREIN. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using src.cbf.model;
using src.cbf.engine;
using src.cbf.utils;

namespace src.cbf.plugin
{

    /**
     * A reporter, which manages other reports Understands the dependency across
     * reports
     **/
    public class ReportingManager : IResultReporter
    {
        readonly private Configuration CONFIG;
        readonly private LogUtils logger;


        public ReportingManager(Object reporter, Configuration CONFIG)
        {
            logger = new LogUtils(this);
            this.CONFIG = CONFIG;
            SelectReporters(reporter);
        }

        /**
         * Opens the report file and updates the headers as required
         * 
         * @param headers
         *            run details to be updated in report
         * 
         */
        public void Open(Dictionary<String, object> headers)
        {
           /** 'Start all reporters */
            foreach (TestResultTracker.Observer reporter in reporters)
            {

                try
                {
                    ((IResultReporter)reporter).Open(headers);

                    logger.Trace("opened reporter: ", reporter);
                }
                catch (ObjectDisposedException e)
                {
                    logger.HandleError("Error opening reporter", e, reporter);
                }
            }
        }

        /**
         * Closes the report file
         */
        public void Close()
        {
           /** Stop all reporters */
            foreach (TestResultTracker.Observer reporter in reporters)
            {

                try
                {
                    if (reporter.GetType().Name != ConstantVar.PARAMETERACCESS)
                    {
                        ((IResultReporter)reporter).Close();
                    }
                }
                catch (IndexOutOfRangeException e)
                {
                    logger.Warning("Error closing reporter - ", reporter, e.Message);
                }
            }
        }

        /**
         * Starts the specific reporter
         * 
         * @param result
         *            object of TestResult
         */
        public void Start(TestResult result)
        {
            foreach (TestResultTracker.Observer reporter in reporters)
            {
                try
                {
                    reporter.Start(result);
                }
                catch (IndexOutOfRangeException e)
                {
                    logger.Warning("Error in start - ", reporter, result, e);
                }
            }
        }

        /**
         * Logs specified reporter
         * 
         * @param result
         *            entity details
         * @param rsType
         *            result type of the current executed entity
         * @param details
         *            execution details of the current executed entity
         */
        public void Log(TestResult result, TestResult.ResultType rsType, Object details)
        {
            foreach (TestResultTracker.Observer reporter in reporters)
            {
                try
                {
                    reporter.Log(result, rsType, details);
                }
                catch (IndexOutOfRangeException e)
                {
                    logger.Warning("Error in reporting - ", reporter + " - "
                            + result + " - " + rsType + " - " + details, e);
                }
            }
        }

        /**
         * Reporter finish method
         * 
         * @param result
         *            execution details
         * @param rsType
         *            result type of the current executed entity
         * @param details
         *            execution details of the current executed entity
         */
        public void Finish(TestResult result, TestResult.ResultType rsType, Object details)
        {
            foreach (TestResultTracker.Observer reporter in reporters)
            {
                try
                {
                    reporter.Finish(result, rsType, details);
                }
                catch (IndexOutOfRangeException e)
                {
                    logger.Warning("Error in finish - ", reporter + " - " + result
                            + " - " + rsType + " - " + details, e);
                }
            }
        }

        public List<TestResultTracker.Observer> Reporters()
        {
            return reporters;
        }

        private void SelectReporters(Object reporterObj)
        {

           /**
             * ' Adds reporter in the correct order of dependency ' Caveat:Any
             * invalid selection is silently skipped without warning
             */

            String reporterSelection = "";
            List<Dictionary<String, Object>> reporterList = (List<Dictionary<String, Object>>)reporterObj;
            if (!reporterObj.ToString().Equals("{}"))
            {
                int i = 0;
                foreach (Dictionary<String, Object> map in reporterList)
                {
                    if (i == 0)
                    {
                        reporterSelection = (String)map["plugin"];
                        i++;
                    }
                    else
                    {
                        reporterSelection = reporterSelection + ","
                                + (String)map["plugin"];
                    }
                }
            }            
            dynamic reportsSelected = null;
            if (reporterSelection != null && !reporterSelection.Equals(""))
            {
                reporterSelection = reporterSelection.Trim();                
                reportsSelected = reporterSelection.Split(',');
            }

            InitializeReporters(reportsSelected, reporterList);
        }

        private void InitializeReporters(dynamic reportsSelected, List<Dictionary<String, Object>> reporterList) //String []reportsSelected(earlier argument)
        {

            foreach (String reporterName in supportedReports)
            {
                Boolean isSelected = false;
                if (reportsSelected == null)
                {
                    isSelected = true;    /* ' Default: all */
                }
                else
                {
                    foreach (String selReport in reportsSelected)
                    {                        
                        if (selReport.Equals(reporterName))
                        {
                            isSelected = true;                            
                        }
                    }
                }
                if (isSelected)
                {
                    Dictionary<String, Object> reporterMap = TraverseArray(reporterList, reporterName);
                    TestResultTracker.Observer reporter = null;
                    try
                    {

                        reporter = (TestResultTracker.Observer)PluginManager.GetPlugin(reporterMap);
                    }
                    catch (IndexOutOfRangeException c)
                    {
                        logger.HandleError(reporterName
                                + " plugin does not match a valid reporter", c,
                                reporterMap);

                    }
                    reporters.Add(reporter);
                    logger.Trace("Reporter selected: ", reporter);
                }
            }

            Open(CONFIG.FetchAllProp());
        }

        private Dictionary<String, Object> TraverseArray(List<Dictionary<String, Object>> reporterList, String key)
        {

            foreach (Dictionary<String, Object> innerMap in reporterList)
            {
                foreach (String str in innerMap.Keys)
                {

                    if (str.Equals("plugin") && innerMap[str].Equals(key))
                    {                        
                            return innerMap;                        
                    }
                }
            }

            return null;
        }

        /**
         * Returns PluginManager format string
         */

        public String TooString()
        {
            return StringUtils.MapString(this, CONFIG);
        }

        /** new reports are added */
        readonly String[] supportedReports = { "ScreenDump", "HtmlEvent",
            "ExcelReport", "ResultEventLogger", "EmailAlert",
            "JenkinsScreenDump", "JenkinsHtmlEvent", "JenkinsExcelReport",
            "TestLink", "JenkinsResultEventLogger", "AlmReporter" };  /**
																	 * "ExcelSummary"
																	 * old style
																	 * is
																	 * disabled
																	 * as a
																	 * default
																	 */

        public List<TestResultTracker.Observer> reporters = new List<TestResultTracker.Observer>();  /**
																 * Holds the
																 * managed
																 * reporters
																 * collection
																 */

    }

}
